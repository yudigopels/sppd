<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_spt extends CI_Model {

	public $table = 'spt';

	public function selectAll()
	{
		$this->db->select("tb_sppd.*,tb_pegawai.id as idpeg,tb_pegawai.nip")
				 ->from($this->table)
				 ->order_by('tb_sppd.id');
		$this->db->join('tb_pegawai', 'tb_sppd.idpeg = tb_pegawai.id');
		
		$query = $this->db->get();

		return $query->result_array();
	}

	public function giat($idkeg)
	{
		$this->db->select("spt.*,tb_pegawai.id as idpeg,tb_pegawai.nip, tb_pegawai.nama as nmpeg, giat.id as idgiat, giat.nama as nmgiat, giat.akun, giat.tahun, giat.lokasi as lok, tbl_provinsi.nama_provinsi as nmlok")
				 ->from($this->table)
				 ->order_by('spt.id');
		$this->db->join('tb_pegawai', 'spt.idpeg = tb_pegawai.id');
		$this->db->join('giat', 'spt.idkeg = giat.id');
		$this->db->join('tbl_provinsi', 'giat.lokasi = tbl_provinsi.id');
		$this->db->where('spt.idkeg', $idkeg);
		$query = $this->db->get();

		return $query->result_array();
	}

	public function ambil_peg() {
	$sql_kat=$this->db->get('tb_pegawai');	
	if($sql_kat->num_rows()>0){
		foreach ($sql_kat->result_array() as $row)
			{
				$result['-']= '- Pilih Pegawai -';
				$result[$row['gol']]= ucwords(strtolower($row['nama']));
			}
			return $result;
		}
	}

	public function ambil_kabupaten($kode_prop){
	//$this->db->select('pangkat.*, tingkat_jabatan.id as idtgkt, tingkat_jabatan.tingkat_jbt as tgkt');
		$this->db->select('pangkat.*, tingkat_jabatan.tingkat_jbt as tgkt')
				 ->from('pangkat')
				 ->order_by('pangkat.singkat', 'ASC');
	$this->db->join('tingkat_jabatan', 'pangkat.idbiaya=tingkat_jabatan.id');
	$this->db->where('pangkat.id',$kode_prop);
	$this->db->order_by('pangkat.id','asc');
	$sql_kabupaten=$this->db->get();
	if($sql_kabupaten->num_rows()>0){

		foreach ($sql_kabupaten->result_array() as $row)
        {
            $result[$row['idbiaya']]= ucwords(strtolower($row['tgkt']));
 
        }
		} else {
		   $result['-']= '- Belum Ada Tingkat -';
		}
        return $result;
	}




	public function peg()
	{
		$this->db->select("*")
				 ->from('tb_pegawai')
				 ->order_by('id');
		$query = $this->db->get();

		return $query->result_array();
	}

	public function lok()
	{
		$this->db->select("*")
				 ->from('tbl_provinsi')
				 ->order_by('id');
		$query = $this->db->get();

		return $query->result_array();
	}


	public function ambil_lok() {
	$sql_kat=$this->db->get('tbl_provinsi');	
	if($sql_kat->num_rows()>0){
		foreach ($sql_kat->result_array() as $row)
			{
				$result['-']= '- Pilih Lokasi -';
				$result[$row['id']]= ucwords(strtolower($row['nama_provinsi']));
			}
			return $result;
		}
	}

	public function ambil_loka($kode_lok){
	$this->db->where('id',$kode_lok);
	$this->db->order_by('id','asc');
	$sql_lok=$this->db->get('tbl_provinsi');
	if($sql_lok->num_rows()>0){

		foreach ($sql_lok->result_array() as $row)
        {
            $result[$row['id']]= ucwords(strtolower($row['ut']));
        }
		} else {
		   $result['-']= '- Uang Transport -';
		}
        return $result;
	}

	public function ambil_um($kode_lok){
	$this->db->where('id',$kode_lok);
	$this->db->order_by('id','asc');
	$sql_lok=$this->db->get('tbl_provinsi');
	if($sql_lok->num_rows()>0){

		foreach ($sql_lok->result_array() as $row)
        {
            $result[$row['id']]= ucwords(strtolower($row['ut']));
        }
		} else {
		   $result['-']= '- Uang Transport -';
		}
        return $result;
	}




	public function tambah_data($data)
	{
		$tambah_data = $this->db->insert($this->table, $data);
		return $tambah_data;
	}
	public function nips()
	{
		$this->db->select("*")
				 ->from('tb_pegawai')
				 ->order_by('id');
		$query = $this->db->get();

		return $query->result_array();
	}
	public function get_id($id)
	{
		$this->db->select("spt.*,tb_pegawai.id as idpeg,tb_pegawai.nip, tb_pegawai.nama as nmpeg, giat.id as idgiat, giat.nama as nmgiat, giat.akun, giat.tahun, giat.lokasi as lok, tbl_provinsi.nama_provinsi as nmlok, pejabat.namap, pejabat.nip")
				 ->from($this->table)
				 ->order_by('spt.id');
		$this->db->join('tb_pegawai', 'spt.idpeg = tb_pegawai.id');
		$this->db->join('giat', 'spt.idkeg = giat.id');
		$this->db->join('tbl_provinsi', 'giat.lokasi = tbl_provinsi.id');
		$this->db->join('pejabat', 'spt.idjabat = pejabat.id');
		$this->db->where('spt.id', $id);
		$query = $this->db->get();
		return $query->result_array();
	}
	public function update_data($id, $data)
	{
		$this->db->where('id', $id);
		$update = $this->db->update($this->table, $data);
		return $update;
	}
	public function hapus_data($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->delete($this->table);
		return $query;
	}

	public function cari_peg($key)
	{
		$this->db->select("*")
				 ->from('tb_pegawai')
				 ->order_by('id');
		$this->db->like('tb_pegawai.nama', $key);
		$query = $this->db->get();

		return $query->result_array();
	}

	public function cari_tanda($key)
	{
		$this->db->select("*")
				 ->from('pejabat')
				 ->order_by('id');
		$this->db->like('pejabat.namap', $key);
		$query = $this->db->get();

		return $query->result_array();
	}

	public function cetak($id)
	{
		$this->db->select("spt.*, tb_pegawai.nip, tb_pegawai.nama as nmpeg, pangkat.content, jabatan.content as jaba, giat.lokasi, giat.nama as nmkeg, tbl_provinsi.nama_provinsi as nmlok, pejabat.namap, pejabat.nip as nipj, pejabat.jabat as jbt")
				 ->from($this->table)
				 ->order_by('spt.id');
		$this->db->join('tb_pegawai', 'spt.idpeg = tb_pegawai.id');
		$this->db->join('pangkat', 'tb_pegawai.gol = pangkat.id');
		$this->db->join('jabatan', 'tb_pegawai.jab = jabatan.id');
		$this->db->join('pejabat', 'spt.idjabat = pejabat.id');
		$this->db->join('giat', 'spt.idkeg = giat.id');
		$this->db->join('tbl_provinsi', 'giat.lokasi = tbl_provinsi.id');
		$this->db->where('spt.id', $id);
		$query = $this->db->get();

		return $query->result_array();
	}

}

/* End of file M_skpd.php */
/* Location: ./application/models/M_kompetensi_bidang.php */
