<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_giat extends CI_Model {

	public $table = 'giat';

	public function selectAll($number,$offset)
	{
		$this->db->select("giat.*,tbl_provinsi.id as idprov,tbl_provinsi.nama_provinsi as lok")
				 //->from($this->table)
				 ->order_by('giat.id');
		$this->db->join('tbl_provinsi', 'giat.lokasi = tbl_provinsi.id');
		//$this->db->join('jabatan', 'tb_pegawai.jab = jabatan.id');
		//$this->db->where('id_skpd', $this->session->userdata('skpd'));
		$query = $this->db->get($this->table,$number,$offset);

		return $query->result_array();
	}

	function jumlah_data(){
		return $this->db->get($this->table)->num_rows();
		
	}

	public function lok()
	{
		$this->db->select("*")
				 ->from('tbl_provinsi')
				 ->order_by('id');
		$query = $this->db->get();

		return $query->result_array();
	}
	
	public function tambah_data($data)
	{
		$tambah_data = $this->db->insert($this->table, $data);
		return $tambah_data;
	}
	public function get_id($id)
	{
		$this->db->select("giat.*,tbl_provinsi.id as idprov,tbl_provinsi.nama_provinsi")
				 ->from($this->table)
				 ->order_by('giat.id');
		$this->db->join('tbl_provinsi', 'giat.lokasi = tbl_provinsi.id');
		$this->db->where('giat.id', $id);
		$query = $this->db->get();

		return $query->result_array();
	}
	public function update_data($id, $data)
	{
		$this->db->where('id', $id);
		$update = $this->db->update($this->table, $data);
		return $update;
	}
	public function hapus_data($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->delete($this->table);
		return $query;
	}

	public function carilok($key)
	{
		$this->db->select("*")
				 ->from('tbl_provinsi')
				 ->order_by('id');
		$this->db->like('tbl_provinsi.nama_provinsi', $key);
		$query = $this->db->get();

		return $query->result_array();
	}

}

/* End of file M_skpd.php */
/* Location: ./application/models/M_kompetensi_bidang.php */
