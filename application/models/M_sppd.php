<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_sppd extends CI_Model {

	public $table = 'sppd';
	public $table1 = 'sppd_ikut';
	public $table2 = 'sppd_berkas';
	public $table3 = 'spt';
	public $table4 = 'sppd_biaya';
	public $table5 = 'tingkat_jabatan';
	public $table6 = 'sppd_kwt';

	public function selectAll()
	{
		$this->db->select("sppd.*,tb_pegawai.id as idpeg,tb_pegawai.nip")
				 ->from($this->table)
				 ->order_by('tb_sppd.id');
		$this->db->join('tb_pegawai', 'tb_sppd.idpeg = tb_pegawai.id');
		
		$query = $this->db->get();

		return $query->result_array();
	}

	function jumlah_data(){
		return $this->db->get($this->table)->num_rows();
		
	}

	function jumlah_kwt(){
		return $this->db->get($this->table)->num_rows();
		
	}

	function jumlah_datav($thn){
		//return $this->db->get($this->table)->num_rows();
		$this->db->select("sppd.*,tb_pegawai.id as idpeg,tb_pegawai.nip, tb_pegawai.nama as nmpeg, tb_pegawai.jab, jabatan.content as jabat, giat.id as idgiat, giat.nama as nmgiat, giat.akun, giat.tahun, giat.lokasi,spt.id as idspt, spt.no_spt,data_tingkat.id as idtgkt")
				 ->from($this->table)
				 ->order_by('sppd.id');
		$this->db->join('spt', 'sppd.idspt = spt.id');
		$this->db->join('tb_pegawai', 'spt.idpeg = tb_pegawai.id');
		$this->db->join('pangkat', 'tb_pegawai.gol = pangkat.id');
		$this->db->join('data_tingkat', 'pangkat.idbiaya = data_tingkat.id');
		$this->db->join('giat', 'spt.idkeg = giat.id');
		$this->db->join('jabatan', 'tb_pegawai.jab = jabatan.id');
		$this->db->where('giat.tahun', $thn);
		//$this->db->where('sppd.idpeg', $idpeg);
		$query = $this->db->get();

		return $query->num_rows();	
	
	}

	public function v_sppd($thn,$number,$offset)
	{
		$this->db->select("sppd.*,tb_pegawai.id as idpeg,tb_pegawai.nip, tb_pegawai.nama as nmpeg, tb_pegawai.jab, jabatan.content as jabat, giat.id as idgiat, giat.nama as nmgiat, giat.akun, giat.tahun, giat.lokasi,spt.id as idspt, spt.no_spt,data_tingkat.id as idtgkt")
				 //->from($this->table)
				 ->order_by('sppd.id');
		$this->db->join('spt', 'sppd.idspt = spt.id');
		$this->db->join('tb_pegawai', 'spt.idpeg = tb_pegawai.id');
		$this->db->join('pangkat', 'tb_pegawai.gol = pangkat.id');
		$this->db->join('data_tingkat', 'pangkat.idbiaya = data_tingkat.id');
		$this->db->join('giat', 'spt.idkeg = giat.id');
		$this->db->join('jabatan', 'tb_pegawai.jab = jabatan.id');
		$this->db->where('giat.tahun', $thn);
		//$this->db->where('sppd.idpeg', $idpeg);
		$query = $this->db->get($this->table,$number,$offset);

		return $query->result_array();
	}

	public function tambah_data($data)
	{
		$tambah_data = $this->db->insert($this->table, $data);
		return $tambah_data;
	}

	public function get_id($id)
	{
		$this->db->select("sppd.*,spt.no_spt, spt.tgl_spt,spt.tgl_berangkat, spt.tgl_kembali, tb_pegawai.id as idpeg,tb_pegawai.nip, tb_pegawai.nama as nmpeg, tb_pegawai.jab, jabatan.content as jabat, pangkat.content as golpang, pangkat.idbiaya, data_tingkat.tingkat_jbt, pejabat.namap, pejabat.nip as nipj")
				 ->from($this->table)
				 ->order_by('sppd.id');
		$this->db->join('spt', 'sppd.idspt = spt.id');
		$this->db->join('tb_pegawai', 'spt.idpeg = tb_pegawai.id');
		$this->db->join('jabatan', 'tb_pegawai.jab = jabatan.id');
		$this->db->join('pangkat', 'tb_pegawai.gol = pangkat.id');
		$this->db->join('data_tingkat', 'pangkat.idbiaya = data_tingkat.id');
		$this->db->join('pejabat', 'sppd.idkom = pejabat.id');
		$this->db->where('sppd.id', $id);
		$query = $this->db->get();
		return $query->result_array();
	}

	public function get_idb($id)
	{
		$this->db->select("sppd.*,pejabat.namap as namapb, pejabat.nip as nipbe")
				 ->from($this->table)
				 ->order_by('sppd.id');
		//$this->db->join('spt', 'sppd.idspt = spt.id');
		//$this->db->join('tb_pegawai', 'spt.idpeg = tb_pegawai.id');
		//$this->db->join('jabatan', 'tb_pegawai.jab = jabatan.id');
		//$this->db->join('pangkat', 'tb_pegawai.gol = pangkat.id');
		//$this->db->join('data_tingkat', 'pangkat.idbiaya = data_tingkat.id');
		$this->db->join('pejabat', 'sppd.idb = pejabat.id');
		$this->db->where('sppd.id', $id);
		$query = $this->db->get();
		return $query->result_array();
	}

	public function update_data($id, $data)
	{
		$this->db->where('id', $id);
		$update = $this->db->update($this->table, $data);
		return $update;
	}
	public function hapus_data($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->delete($this->table);
		return $query;
	}

	public function v_ikut($idsppd)
	{
		$this->db->select("*")
				 ->from($this->table1)
				 ->order_by('id');
		$this->db->where('idsppd', $idsppd);
		$query = $this->db->get();

		return $query->result_array();
	}

	public function tambah_dataikut($data)
	{
		$tambah_datai = $this->db->insert($this->table1, $data);
		return $tambah_datai;
	}

	public function getikut_id($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->get($this->table1);
		return $query->result_array();
	}

	public function update_dataikut($id, $data)
	{
		$this->db->where('id', $id);
		$updatei = $this->db->update($this->table1, $data);
		return $updatei;
	}

	public function hapus_dataikut($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->delete($this->table1);
		return $query;
	}

	public function cari_spt($key)
	{
		$this->db->select("spt.*,tb_pegawai.nama, tb_pegawai.nip, tb_pegawai.gol, tb_pegawai.jab, jabatan.content, pangkat.content as golpang, pangkat.idbiaya, data_tingkat.tingkat_jbt")
				 ->from('spt')
				 ->order_by('spt.id');
		$this->db->join('tb_pegawai', 'spt.idpeg = tb_pegawai.id');
		$this->db->join('jabatan', 'tb_pegawai.jab = jabatan.id');
		$this->db->join('pangkat', 'tb_pegawai.gol = pangkat.id');
		$this->db->join('data_tingkat', 'pangkat.idbiaya = data_tingkat.id');
		$this->db->like('spt.no_spt', $key);
		$query = $this->db->get();

		return $query->result_array();
	}

	public function cari_pegs($key)
	{
		$this->db->select("tb_pegawai.*, jabatan.content as jabat, data_tingkat.tingkat_jbt,pangkat.idbiaya, pangkat.singkat")
				 ->from('tb_pegawai')
				 ->order_by('id');
		$this->db->join('jabatan', 'tb_pegawai.jab = jabatan.id');
		$this->db->join('pangkat', 'tb_pegawai.gol = pangkat.id');
		$this->db->join('data_tingkat', 'pangkat.idbiaya = data_tingkat.id');
		$this->db->like('tb_pegawai.nama', $key);
		$query = $this->db->get();

		return $query->result_array();
	}

	public function v_berkas($idsppd)
	{
		$this->db->select("sppd_berkas.*")//,tb_pegawai.id as idpeg,tb_pegawai.nip, tb_pegawai.nama as nmpeg, tb_pegawai.jab, jabatan.content as jabat")
				 ->from($this->table2)
				 ->order_by('sppd_berkas.id');
		//$this->db->join('spt', 'sppd.idspt = spt.id');
		//$this->db->join('tb_pegawai', 'spt.idpeg = tb_pegawai.id');
		//$this->db->join('giat', 'spt.idkeg = giat.id');
		//$this->db->join('jabatan', 'tb_pegawai.jab = jabatan.id');
		$this->db->where('sppd_berkas.idsppd', $idsppd);
		//$this->db->where('sppd.idpeg', $idpeg);
		$query = $this->db->get();

		return $query->result_array();
	}

	public function data_spt($idspt)
	{
		$this->db->select("spt.*,tb_pegawai.nama,tb_pegawai.nip, pangkat.content as pgkt, jabatan.content as jabat, giat.nama as kegiatan")
				 ->from($this->table3)
				 ->order_by('spt.id');
		$this->db->join('tb_pegawai', 'spt.idpeg = tb_pegawai.id');
		$this->db->join('pangkat', 'tb_pegawai.gol = pangkat.id');
		$this->db->join('jabatan', 'tb_pegawai.jab = jabatan.id');
		$this->db->join('giat', 'spt.idkeg = giat.id');
		$this->db->where('spt.id', $idspt);
		$query = $this->db->get();

		return $query->result_array();
	}

	public function data_berkas()
	{
		$this->db->select("data_berkas.*")
				 ->from('data_berkas')
				 ->order_by('id');
		$query = $this->db->get();

		return $query->result_array();
	}

	public function tambah_datab($data)
	{
		$tambah_datab = $this->db->insert($this->table2, $data);
		return $tambah_datab;
	}

	public function update_datab($idsppd, $data)
	{
		$this->db->where('idsppd', $idsppd);
		$updateb = $this->db->update($this->table2, $data);
		return $updateb;
	}

	public function v_biaya($idsppd)
	{
		$this->db->select("sppd_biaya.*")//,tb_pegawai.id as idpeg,tb_pegawai.nip, tb_pegawai.nama as nmpeg, tb_pegawai.jab, jabatan.content as jabat")
				 ->from($this->table4)
				 ->order_by('sppd_biaya.id');
		//$this->db->join('spt', 'sppd.idspt = spt.id');
		//$this->db->join('tb_pegawai', 'spt.idpeg = tb_pegawai.id');
		//$this->db->join('giat', 'spt.idkeg = giat.id');
		//$this->db->join('jabatan', 'tb_pegawai.jab = jabatan.id');
		$this->db->where('sppd_biaya.idsppd', $idsppd);
		//$this->db->where('sppd.idpeg', $idpeg);
		$query = $this->db->get();

		return $query->result_array();
	}

	public function data_giatlok($idspt)
	{
		$this->db->select("spt.*,giat.nama as kegiatan, giat.lokasi,tbl_provinsi.ut, tbl_provinsi.id as idprov, data_tingkat.tingkat_jbt")
				 ->from($this->table3)
				 ->order_by('spt.id');
		//$this->db->join('tb_pegawai', 'spt.idpeg = tb_pegawai.id');
		//$this->db->join('pangkat', 'tb_pegawai.gol = pangkat.id');
		//$this->db->join('jabatan', 'tb_pegawai.jab = jabatan.id');
		$this->db->join('giat', 'spt.idkeg = giat.id');
		$this->db->join('tbl_provinsi', 'giat.lokasi = tbl_provinsi.id');
		$this->db->join('tb_pegawai', 'spt.idpeg = tb_pegawai.id');
		$this->db->join('pangkat', 'tb_pegawai.gol = pangkat.id');
		$this->db->join('data_tingkat', 'pangkat.idbiaya = data_tingkat.id');
		$this->db->where('spt.id', $idspt);
		$query = $this->db->get();

		return $query->result_array();
	}

	public function data_gb($idlok,$idtgkt)
	{
		$this->db->select("*")
				 ->from($this->table5)
				 ->order_by('id');
		$this->db->where('tingkat_jbt', $idtgkt);
		$this->db->where('idlok', $idlok);
		$query = $this->db->get();

		return $query->result_array();
	}

	public function tambah_databy($data)
	{
		$tambah_databy = $this->db->insert($this->table4, $data);
		return $tambah_databy;
	}

	public function update_databy($idsppd, $data)
	{
		$this->db->where('idsppd', $idsppd);
		$updateby = $this->db->update($this->table4, $data);
		return $updateby;
	}

	public function v_kwt($idsppd)
	{
		$this->db->select("sppd_kwt.*")
				 ->from($this->table6)
				 ->order_by('id');
		//$this->db->join('spt', 'sppd.idspt = spt.id');
		//$this->db->join('tb_pegawai', 'spt.idpeg = tb_pegawai.id');
		//$this->db->join('giat', 'spt.idkeg = giat.id');
		//$this->db->join('jabatan', 'tb_pegawai.jab = jabatan.id');
		$this->db->where('idsppd', $idsppd);
		//$this->db->where('sppd.idpeg', $idpeg);
		$query = $this->db->get();

		return $query->result_array();
	}

	public function data_kwt($idsppd)
	{
		$this->db->select("sppd_biaya.*, tbl_provinsi.ut, tingkat_jabatan.um, tingkat_jabatan.us")
				 ->from($this->table4)
				 ->order_by('sppd_biaya.id');
		$this->db->join('tbl_provinsi', 'sppd_biaya.idprov = tbl_provinsi.id');
		$this->db->join('tingkat_jabatan', 'sppd_biaya.idtgkt = tingkat_jabatan.id');
		$this->db->where('sppd_biaya.idsppd', $idsppd);
		
		$query = $this->db->get();

		return $query->result_array();
	}

	public function tambah_datak($data)
	{
		$tambah_datak = $this->db->insert($this->table6, $data);
		return $tambah_datak;
	}

	public function update_datak($idsppd, $data)
	{
		$this->db->where('idsppd', $idsppd);
		$updateby = $this->db->update($this->table6, $data);
		return $updateby;
	}

	public function cari_tanda1($key)
	{
		$this->db->select("*")
				 ->from('pejabat')
				 ->order_by('id');
		$this->db->like('pejabat.namap', $key);
		$query = $this->db->get();

		return $query->result_array();
	}

	public function data_ttd($idsppd)
	{
		$this->db->select("sppd.*, pejabat.namap, pejabat.nip")
				 ->from($this->table)
				 ->order_by('sppd.id');
		$this->db->join('pejabat', 'sppd.idkom = pejabat.id');
		//$this->db->join('tb_pegawai', 'spt.idpeg = tb_pegawai.id');
		//$this->db->join('giat', 'spt.idkeg = giat.id');
		//$this->db->join('jabatan', 'tb_pegawai.jab = jabatan.id');
		$this->db->where('sppd.id', $idsppd);
		//$this->db->where('sppd.idpeg', $idpeg);
		$query = $this->db->get();

		return $query->result_array();
	}

	public function data_ttdb($idsppd)
	{
		$this->db->select("sppd.*, pejabat.namap as namapb, pejabat.nip as nipb")
				 ->from($this->table)
				 ->order_by('sppd.id');
		$this->db->join('pejabat', 'sppd.idb = pejabat.id');
		//$this->db->join('tb_pegawai', 'spt.idpeg = tb_pegawai.id');
		//$this->db->join('giat', 'spt.idkeg = giat.id');
		//$this->db->join('jabatan', 'tb_pegawai.jab = jabatan.id');
		$this->db->where('sppd.id', $idsppd);
		//$this->db->where('sppd.idpeg', $idpeg);
		$query = $this->db->get();

		return $query->result_array();
	}

	public function cetak($id)
	{
		$this->db->select("sppd.*, pejabat.namap,spt.idpeg, tb_pegawai.nip, tb_pegawai.nama as nmpeg, pangkat.singkat as golpang, 					jabatan.content as jabat, sppd_biaya.idtgkt,data_tingkat.tingkat_jbt as tgkt,giat.nama as nmgiat, 					spt.angkutan, spt.tempat_brgkt, spt.tujuan, sppd_biaya.lama, spt.tgl_berangkat, spt.tgl_kembali, giat.akun, 					spt.tgl_spt, pejabat.jabat as jbt, pejabat.nip as nipj")
				 ->from($this->table)
				 ->order_by('sppd.id');
		$this->db->join('pejabat', 'sppd.idkom = pejabat.id');
		$this->db->join('spt', 'sppd.idspt = spt.id');
		$this->db->join('tb_pegawai', 'spt.idpeg = tb_pegawai.id');
		$this->db->join('pangkat', 'tb_pegawai.gol = pangkat.id');
		$this->db->join('sppd_biaya', 'sppd.id = sppd_biaya.idsppd');
		$this->db->join('data_tingkat', 'sppd_biaya.idtgkt = data_tingkat.id');
		$this->db->join('giat', 'spt.idkeg = giat.id');
		$this->db->join('jabatan', 'tb_pegawai.jab = jabatan.id');
		$this->db->join('sppd_ikut', 'sppd.id = sppd_ikut.idsppd');
		$this->db->where('sppd.id', $id);
		$query = $this->db->get();

		return $query->result_array();
	}


	public function get_cikut($id)
	{
		$this->db->select("sppd_ikut.namai, sppd_ikut.ket")
				 ->from($this->table1)
				 ->order_by('sppd_ikut.id');
		//$this->db->join('sppd_ikut', 'sppd.id = sppd_ikut.idsppd');
		$this->db->where('sppd_ikut.idsppd', $id);
		$query = $this->db->get();
		return $query->result_array();
	}

	public function cetak_kwt($idsppd)
	{
		$this->db->select("sppd_kwt.*, sppd_biaya.tglbyr, sppd_biaya.lama, tbl_provinsi.ut, spt.idkeg, giat.nama as nmgiat, tingkat_jabatan.um,tingkat_jabatan.us, tb_pegawai.nama as nmpeg, spt.tgl_berangkat, spt.tgl_kembali")
				 ->from($this->table6)
				 ->order_by('sppd_kwt.id');
		$this->db->join('sppd_biaya', 'sppd_kwt.idsppd = sppd_biaya.idsppd');
		$this->db->join('tbl_provinsi', 'sppd_biaya.idprov = tbl_provinsi.id');
		$this->db->join('sppd', 'sppd_kwt.idsppd = sppd.id');
		$this->db->join('spt', 'sppd.idspt = spt.id');
		$this->db->join('tingkat_jabatan', 'sppd_biaya.idtgkt = tingkat_jabatan.id');
		$this->db->join('tb_pegawai', 'spt.idpeg = tb_pegawai.id');
		$this->db->join('giat', 'spt.idkeg = giat.id');
		$this->db->where('sppd_kwt.idsppd', $idsppd);
		$query = $this->db->get();

		return $query->result_array();
	}


}

/* End of file M_skpd.php */
/* Location: ./application/models/M_kompetensi_bidang.php */
