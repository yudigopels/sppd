<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_pegawai extends CI_Model {

	public $table = 'tb_pegawai';
	public $table1 = 'jabatan';
	public $table2 = 'pangkat';

	public function selectAll($number,$offset)
	{
		$this->db->select("tb_pegawai.*,pangkat.id as idpang,pangkat.singkat,jabatan.id as idjab,jabatan.content")
				 //->from($this->table)
				 ->order_by('tb_pegawai.id');
		$this->db->join('pangkat', 'tb_pegawai.gol = pangkat.id');
		$this->db->join('jabatan', 'tb_pegawai.jab = jabatan.id');
		//$this->db->where('id_skpd', $this->session->userdata('skpd'));
		$query = $this->db->get($this->table,$number,$offset);

		return $query->result_array();
	}

	function jumlah_data(){
		return $this->db->get($this->table)->num_rows();
		
	}

	public function jab()
	{
		$this->db->select("*")
				 ->from('jabatan')
				 ->order_by('id');
		$query = $this->db->get();

		return $query->result_array();
	}
	public function gol()
	{
		$this->db->select("*")
				 ->from('pangkat')
				 ->order_by('id');		
		$query = $this->db->get();

		return $query->result_array();
	}
	public function tambah_data($data)
	{
		$tambah_data = $this->db->insert($this->table, $data);
		return $tambah_data;
	}
	
	public function get_id($id)
	{
		$this->db->select("tb_pegawai.*,pangkat.id as idpang,pangkat.singkat,jabatan.id as idjab,jabatan.content")
				 ->from($this->table)
				 ->order_by('tb_pegawai.id');
		$this->db->join('pangkat', 'tb_pegawai.gol = pangkat.id');
		$this->db->join('jabatan', 'tb_pegawai.jab = jabatan.id');
		$this->db->where('tb_pegawai.id', $id);
		$query = $this->db->get();

		return $query->result_array();
	}
	public function update_data($id, $data)
	{
		$this->db->where('id', $id);
		$update = $this->db->update($this->table, $data);
		return $update;
	}
	public function hapus_data($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->delete($this->table);
		return $query;
	}

	public function cari_j($key)
	{
		$this->db->select("*")
				 ->from($this->table1)
				 ->order_by('id');
		$this->db->like('jabatan.content', $key);
		$query = $this->db->get();

		return $query->result_array();
	}

	public function cari_g($key)
	{
		$this->db->select("*")
				 ->from($this->table2)
				 ->order_by('id');
		$this->db->like('pangkat.singkat', $key);
		$query = $this->db->get();

		return $query->result_array();
	}
}

/* End of file M_skpd.php */
/* Location: ./application/models/M_kompetensi_bidang.php */
