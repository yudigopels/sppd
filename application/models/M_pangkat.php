<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_pangkat extends CI_Model {

	public $table = 'pangkat';

	public function selectAll($number,$offset)
	{
		$this->db->select('pangkat.*, data_tingkat.tingkat_jbt as tgkt')
				 //->from($this->table)
				 ->order_by('pangkat.singkat', 'ASC');
		$this->db->join('data_tingkat', 'pangkat.idbiaya=data_tingkat.id');
		$query = $this->db->get($this->table,$number,$offset);
		return $query->result_array();
	}	

	function jumlah_data(){
		return $this->db->get($this->table)->num_rows();
		
	}

	public function by()
	{
		$this->db->select("*")
				 ->from('data_tingkat')
				 ->order_by('tingkat_jbt');
		$query = $this->db->get();

		return $query->result_array();
	}

	public function tambah_data($data)
	{
		$tambah_data = $this->db->insert($this->table, $data);
		return $tambah_data;
	}
	public function get_id($id)
	{
	$this->db->select('pangkat.*, tingkat_jabatan.tingkat_jbt as tgkt')
				 ->from($this->table)
				 ->order_by('pangkat.singkat', 'ASC');
		$this->db->join('tingkat_jabatan', 'pangkat.idbiaya=tingkat_jabatan.id');
		$this->db->where('pangkat.id', $id);
		$query = $this->db->get();
		return $query->result_array();
	}


	public function update_data($id, $data)
	{
		$this->db->where('id', $id);
		$update = $this->db->update($this->table, $data);
		return $update;
	}
	public function hapus_data($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->delete($this->table);
		return $query;
	}
	
	
}

/* End of file M_skpd.php */
/* Location: ./application/models/M_kompetensi_bidang.php */
