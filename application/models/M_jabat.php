<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_jabat extends CI_Model {

	public $table = 'pejabat';

	public function selectAll()
	{
		$this->db->select('*')
				 ->from($this->table)
				 ->order_by('id', 'ASC');
		$query = $this->db->get();
		return $query->result_array();
	}	
	public function tambah_data($data)
	{
		$tambah_data = $this->db->insert($this->table, $data);
		return $tambah_data;
	}
	public function get_id($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->get($this->table);
		return $query->result_array();
	}
	public function update_data($id, $data)
	{
		$this->db->where('id', $id);
		$update = $this->db->update($this->table, $data);
		return $update;
	}
	public function hapus_data($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->delete($this->table);
		return $query;
	}

}

/* End of file M_skpd.php */
/* Location: ./application/models/M_kompetensi_bidang.php */
