<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_kwitansi extends CI_Model {

	public $table = 'tb_kwitansi';

	public function selectAll()
	{
				
		$this->db->select("tb_kwitansi.*,tb_sppd.id as idsp,tb_sppd.no_sppd")
				 ->from($this->table)
				 ->order_by('tb_kwitansi.id');
		$this->db->join('tb_sppd', 'tb_kwitansi.no_sppd = tb_sppd.id');
		//$this->db->join('jabatan', 'tb_pegawai.jab = jabatan.id');
		//$this->db->where('id_skpd', $this->session->userdata('skpd'));
		$query = $this->db->get();

		return $query->result_array();
	}
	
	public function nosppd()
	{
		$this->db->select("*")
				 ->from('tb_sppd')
				 ->order_by('id');
		$query = $this->db->get();

		return $query->result_array();
	}
public function tambah_data($data)
	{
		$tambah_data = $this->db->insert($this->table, $data);
		return $tambah_data;
	}
public function get_id($id)
	{
		$this->db->select("tb_kwitansi.*,tb_sppd.id as idsp,tb_sppd.no_sppd")
				 ->from($this->table)
				 ->order_by('tb_kwitansi.id');
		$this->db->join('tb_sppd', 'tb_kwitansi.no_sppd = tb_sppd.id');				
		$this->db->where('tb_kwitansi.id', $id);
		$query = $this->db->get();
		return $query->result_array();
	}
	public function update_data($id, $data)
	{
		$this->db->where('id', $id);
		$update = $this->db->update($this->table, $data);
		return $update;
	}
	public function hapus_data($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->delete($this->table);
		return $query;
	}
}

/* End of file M_skpd.php */
/* Location: ./application/models/M_kompetensi_bidang.php */
