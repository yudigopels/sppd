<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_tingkat_jabatan extends CI_Model {

	public $table = 'tingkat_jabatan';
	public $table1 = 'data_tingkat';

	public function selectAll($number,$offset)
	{
		$this->db->select('tingkat_jabatan.*, tbl_provinsi.nama_provinsi as lok, data_tingkat.tingkat_jbt as tgkt')
				 //->from($this->table)
				 ->order_by('tingkat_jabatan.tingkat_jbt', 'ASC');
		$this->db->join('tbl_provinsi', 'tingkat_jabatan.idlok = tbl_provinsi.id');
		$this->db->join('data_tingkat', 'tingkat_jabatan.tingkat_jbt = data_tingkat.id');
		$query = $this->db->get($this->table,$number,$offset);
		return $query->result_array();
	}	

	function jumlah_data(){
		return $this->db->get($this->table)->num_rows();
		
	}


	public function lok()
	{
		$this->db->select("*")
				 ->from('tbl_provinsi')
				 ->order_by('id');
		$query = $this->db->get();

		return $query->result_array();
	}
	public function tambah_data($data)
	{
		$tambah_data = $this->db->insert($this->table, $data);
		return $tambah_data;
	}
	public function get_id($id)
	{
		$this->db->select('tingkat_jabatan.*, tbl_provinsi.nama_provinsi as lok, data_tingkat.tingkat_jbt as tgkt')
				 //->from($this->table)
				 ->order_by('tingkat_jabatan.tingkat_jbt', 'ASC');
		$this->db->join('tbl_provinsi', 'tingkat_jabatan.idlok = tbl_provinsi.id');
		$this->db->join('data_tingkat', 'tingkat_jabatan.tingkat_jbt = data_tingkat.id');
		$this->db->where('tingkat_jabatan.id', $id);
		$query = $this->db->get($this->table);
		return $query->result_array();

	}
	public function update_data($id, $data)
	{
		$this->db->where('id', $id);
		$update = $this->db->update($this->table, $data);
		return $update;
	}
	public function hapus_data($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->delete($this->table);
		return $query;
	}
	public function cari_t($key)
	{
		$this->db->select("*")
				 ->from($this->table1)
				 ->order_by('id');
		$this->db->like('data_tingkat.tingkat_jbt', $key);
		$query = $this->db->get();

		return $query->result_array();
	}

}

/* End of file M_skpd.php */
/* Location: ./application/models/M_kompetensi_bidang.php */
