<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tingkat_jabatan extends CI_Controller {

	public $konten = array('main_view' => '', );

	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_tingkat_jabatan', 'tingkat_jabatan', TRUE);
	}

	public function index()
	{

		$jumlah_data = $this->tingkat_jabatan->jumlah_data();
		$this->load->library('pagination');
		$config['base_url'] = base_url().'index.php/tingkat_jabatan/index/';
		$config['total_rows'] = $jumlah_data;
		$config['per_page'] = 5;
		$from = $this->uri->segment(3);
		$this->pagination->initialize($config);

		$data = array('data' => $this->tingkat_jabatan->selectAll($config['per_page'],$from),
				'lok'=> $this->tingkat_jabatan->lok(),
		);
		$this->konten['main_view'] = $this->load->view('listtingkat_jabatan', $data, TRUE);
		$this->load->view('dashboard1', $this->konten);
	}

	public function tambah_data()
	{
		
		$data = array(
					'id' => '',
					'tingkat_jbt' => $this->input->post('idtk'),
					'idlok' => $this->input->post('idkeg'),
					'um' => $this->input->post('um'),
					//'ut' => $this->input->post('ut'),
					'us' => $this->input->post('us'),
										 );
		$this->tingkat_jabatan->tambah_data($data);
		redirect('tingkat_jabatan');
	}
	public function update()
	{
	$id = $this->input->post('id');
		$data = array(
		// ini nama table di db dan jab adalah name di update_jabatan
					'tingkat_jbt' => $this->input->post('idtk'),
					'idlok' => $this->input->post('idkeg'),
					'um' => $this->input->post('um'),
					//'ut' => $this->input->post('ut'),
					'us' => $this->input->post('us'),
										 );
		$this->tingkat_jabatan->update_data($id,$data);
		redirect('tingkat_jabatan');
	}
		public function get_id($id)
	{
		$data = array('data' => $this->tingkat_jabatan->get_id($id),
				'lok'=> $this->tingkat_jabatan->lok(),
		);

		
		$this->konten['main_view'] = $this->load->view('update_tingkat_jabatan', $data, TRUE);
		$this->load->view('dashboard1', $this->konten);
	}
	public function hapus($id)
	{
		$this->tingkat_jabatan->hapus_data($id);
		redirect('tingkat_jabatan');
	}

	public function search()
	{
		// tangkap variabel keyword dari URL
		$keyword = $this->uri->segment(3);

		// cari di database
		//$data = $this->db->select('kel.content as nmkel, kel.idkec, kec.content as nmkec ')->from('kel')->like('content',$keyword)->$this->db->join('kec', 'kel.idkec = kec.id')->get();	

		$data = array('data' => $this->tingkat_jabatan->cari_t($keyword), 
				
		);

		// format keluaran di dalam array
		foreach($data['data'] as $row)
		{
			$arr['query'] = $keyword;
			$arr['suggestions'][] = array(
				'value'	=>$row['tingkat_jbt'],
				'idt'	=>$row['id'],

			);
		}
		// minimal PHP 5.2
		//$this->session->set_flashdata('message',"<div style='color:green;'>NIK Sudah Ada!<div>");
		echo json_encode($arr);
		
	}

}

/* End of file eselon.php */
/* Location: ./application/controllers/eselon.php */
