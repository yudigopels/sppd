<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pangkat extends CI_Controller {

	public $konten = array('main_view' => '', );

	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_pangkat', 'pangkat', TRUE);
	}

	public function index()
	{
		$jumlah_data = $this->pangkat->jumlah_data();
		$this->load->library('pagination');
		$config['base_url'] = base_url().'index.php/pangkat/index/';
		$config['total_rows'] = $jumlah_data;
		$config['per_page'] = 5;
		$from = $this->uri->segment(3);
		$this->pagination->initialize($config);

		$data = array('data' => $this->pangkat->selectAll($config['per_page'],$from),
			      'by' => $this->pangkat->by(),						
		);
		$this->konten['main_view'] = $this->load->view('listpangkat', $data, TRUE);
		$this->load->view('dashboard1', $this->konten);
	}

	public function tambah_data()
	{
		// $isi =  preg_split('/\r\n|[\r\n]/', $this->input->post('isi'));
		// print_r($isi);
		$data = array(
					'id' => '',
					'singkat' => $this->input->post('singkat'),
					'content' => $this->input->post('content'),
					'idbiaya' => $this->input->post('tg'),
										 );
		$this->pangkat->tambah_data($data);
		redirect('pangkat');
	}
	public function update()
	{
		$id = $this->input->post('id');
		$data = array(					
					'singkat' => $this->input->post('singkat'),
					'content' => $this->input->post('content'),
					'idbiaya' => $this->input->post('by'),
										 );
		$this->pangkat->update_data($id,$data);
		redirect('pangkat');
	}
	public function get_id($id)
	{
		$data = array('data' => $this->pangkat->get_id($id),
			      'by' => $this->pangkat->by(),						
		);

		
		$this->konten['main_view'] = $this->load->view('update_pangkat', $data, TRUE);
		$this->load->view('dashboard1', $this->konten);
	}
	public function hapus($id)
	{
		$this->pangkat->hapus_data($id);
		redirect('pangkat');
	}
}

/* End of file eselon.php */
/* Location: ./application/controllers/eselon.php */
