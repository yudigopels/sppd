<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sppd extends CI_Controller {

	public $konten = array('main_view' => '', );

	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_sppd', 'sppd', TRUE);
	}

	public function index(){
	$data = array('data' => $this->sppd->selectAll(), 
			'nips' => $this->sppd->nips(),						
						);
		$this->konten['main_view'] = $this->load->view('listsppd', $data, TRUE);
		$this->load->view('dashboard1', $this->konten);
	}
	public function v_sppd()
	{
	$thn=date('Y');

		$jumlah_data = $this->sppd->jumlah_datav($thn);
		$this->load->library('pagination');
		$config['base_url'] = base_url().'index.php/sppd/v_sppd/';
		$config['total_rows'] = $jumlah_data;
		$config['per_page'] = 5;
		$from = $this->uri->segment(3);
		$this->pagination->initialize($config);

		$data = array('data' => $this->sppd->v_sppd($thn,$config['per_page'],$from), 
				//'thn' => $thn
				//'idpeg' => $idpeg,
				//'idpst' => $idspt
				);
		$this->konten['main_view'] = $this->load->view('v_sppd', $data, TRUE);
		$this->load->view('dashboard1', $this->konten);
	}
	
	public function tambah()
	{

		$data = array(
					'id' => '',
					'idspt' => $this->input->post('ids'),
					'nospd' => $this->input->post('no_spd'),
					'idkom' => $this->input->post('idj'),
					'kode' => $this->input->post('kode_spd'),
					'ket' => $this->input->post('ket')
				 );
		$this->sppd->tambah_data($data);
		redirect('sppd/v_sppd/');
	}
	public function update()
	{
		$id = $this->input->post('id');
		$data = array(
					'idspt' => $this->input->post('ids'),
					'idb' => $this->input->post('idjb'),
					'idkom' => $this->input->post('idj'),
					'kode' => $this->input->post('kode_spd'),
					'nospd' => $this->input->post('no_spd'),
					'ket' => $this->input->post('ket')
					);
		$this->sppd->update_data($id,$data);
		redirect('sppd/v_sppd/');
		}

	public function get_id($id)
	{
		$data = array(
		'data' => $this->sppd->get_id($id),
		'datab' => $this->sppd->get_idb($id),
		//'nips' => $this->sppd->nips(),
		);		
		$this->konten['main_view'] = $this->load->view('update_sppd', $data, TRUE);
		$this->load->view('dashboard1', $this->konten);

	}
	public function hapus($id)
	{
		$this->sppd->hapus_data($id);
		redirect('sppd/v_sppd/');
	}


	public function v_ikut($idsppd)
	{
	//$thn=date('Y');
		$data = array('data' => $this->sppd->v_ikut($idsppd), 
				'idspd' => $idsppd,
				//'idpeg' => $idpeg,
				//'idpst' => $idspt
				);
		$this->konten['main_view'] = $this->load->view('v_ikut', $data, TRUE);
		$this->load->view('dashboard1', $this->konten);
	}

	public function tambah_ikut()
	{

		$idsppd=$this->input->post('idsppd');
		$data = array(
					'id' => '',
					'idsppd' => $this->input->post('idsppd'),
					'namai' => $this->input->post('namai'),
					'ket' => $this->input->post('ket')
				 );
		$this->sppd->tambah_dataikut($data);
		redirect('sppd/v_ikut/'.$idsppd);
	}

	public function getikut_id($id,$idsppd)
	{
		$data = array(
				'data' => $this->sppd->getikut_id($id),
				'idspd' => $idsppd
		);		
		$this->konten['main_view'] = $this->load->view('update_spdikut', $data, TRUE);
		$this->load->view('dashboard1', $this->konten);

	}

	public function update_ikut()
	{
		$id = $this->input->post('id');
		$idsppd = $this->input->post('idsppd');
		$data = array(
					'idsppd' => $this->input->post('idsppd'),
					'namai' => $this->input->post('namai'),
					'ket' => $this->input->post('ket')
					);
		$this->sppd->update_dataikut($id,$data);
		redirect('sppd/v_ikut/'.$idsppd);
		}

	public function hapus_ikut($id,$idsppd)
	{
		$this->sppd->hapus_dataikut($id);
		redirect('sppd/v_ikut/'.$idsppd);
	}


	public function cari_spt()
	{
		// tangkap variabel keyword dari URL
		$keyword = $this->uri->segment(3);

		// cari di database
		//$data = $this->db->select('kel.content as nmkel, kel.idkec, kec.content as nmkec ')->from('kel')->like('content',$keyword)->$this->db->join('kec', 'kel.idkec = kec.id')->get();	

		$data = array('data' => $this->sppd->cari_spt($keyword), 
				
		);

		// format keluaran di dalam array
		foreach($data['data'] as $row)
		{

$tgspt=$row['tgl_spt'];
$pisah = explode('-',$tgspt);

switch($pisah[1]){       
        case 1 : {
                    $bln='Januari';
                }break;
        case 2 : {
                    $bln='Februari';
                }break;
        case 3 : {
                    $bln='Maret';
                }break;
        case 4 : {
                    $bln='April';
                }break;
        case 5 : {
                    $bln='Mei';
                }break;
        case 6 : {
                    $bln="Juni";
                }break;
        case 7 : {
                    $bln='Juli';
                }break;
        case 8 : {
                    $bln='Agustus';
                }break;
        case 9 : {
                    $bln='September';
                }break;
        case 10 : {
                    $bln='Oktober';
                }break;     
        case 11 : {
                    $bln='November';
                }break;
        case 12 : {
                    $bln='Desember';
                }break;
        default: {
                    $bln='UnKnown';
                }break;
    }


$array = array($pisah[2],$bln,$pisah[0]);
$tglspt = implode(' ',$array);

$tgb=$row['tgl_berangkat'];
$pisah1 = explode('-',$tgb);

switch($pisah1[1]){       
        case 1 : {
                    $bln='Januari';
                }break;
        case 2 : {
                    $bln='Februari';
                }break;
        case 3 : {
                    $bln='Maret';
                }break;
        case 4 : {
                    $bln='April';
                }break;
        case 5 : {
                    $bln='Mei';
                }break;
        case 6 : {
                    $bln="Juni";
                }break;
        case 7 : {
                    $bln='Juli';
                }break;
        case 8 : {
                    $bln='Agustus';
                }break;
        case 9 : {
                    $bln='September';
                }break;
        case 10 : {
                    $bln='Oktober';
                }break;     
        case 11 : {
                    $bln='November';
                }break;
        case 12 : {
                    $bln='Desember';
                }break;
        default: {
                    $bln='UnKnown';
                }break;
    }


$arrayb = array($pisah1[2],$bln,$pisah1[0]);
$brgkt = implode(' ',$arrayb);

$tgk=$row['tgl_kembali'];
$pisah2 = explode('-',$tgk);

switch($pisah2[1]){       
        case 1 : {
                    $bln='Januari';
                }break;
        case 2 : {
                    $bln='Februari';
                }break;
        case 3 : {
                    $bln='Maret';
                }break;
        case 4 : {
                    $bln='April';
                }break;
        case 5 : {
                    $bln='Mei';
                }break;
        case 6 : {
                    $bln="Juni";
                }break;
        case 7 : {
                    $bln='Juli';
                }break;
        case 8 : {
                    $bln='Agustus';
                }break;
        case 9 : {
                    $bln='September';
                }break;
        case 10 : {
                    $bln='Oktober';
                }break;     
        case 11 : {
                    $bln='November';
                }break;
        case 12 : {
                    $bln='Desember';
                }break;
        default: {
                    $bln='UnKnown';
                }break;
    }


$arrayk = array($pisah2[2],$bln,$pisah2[0]);
$kem = implode(' ',$arrayk);

			$arr['query'] = $keyword;
			$arr['suggestions'][] = array(
				'value'	=>$row['no_spt'],
				'ids'	=>$row['id'],
				'tgspt'	=>$tglspt,
				'brgkt'	=>$brgkt,
				'kembali'=>$kem,
				'peg'=>$row['nama'],
				'nip'=>$row['nip'],
				'jaba'=>$row['content'],
				'gol'=>$row['golpang'],
				'tgkt'=>$row['tingkat_jbt']
			);
		}
		// minimal PHP 5.2
		//$this->session->set_flashdata('message',"<div style='color:green;'>NIK Sudah Ada!<div>");
		echo json_encode($arr);
		
	}

	public function cari_pegs()
	{
		// tangkap variabel keyword dari URL
		$keyword = $this->uri->segment(3);

		// cari di database
		//$data = $this->db->select('kel.content as nmkel, kel.idkec, kec.content as nmkec ')->from('kel')->like('content',$keyword)->$this->db->join('kec', 'kel.idkec = kec.id')->get();	

		$data = array('data' => $this->sppd->cari_pegs($keyword), 
				
		);

		// format keluaran di dalam array
		foreach($data['data'] as $row)
		{
			$arr['query'] = $keyword;
			$arr['suggestions'][] = array(
				'value'	=>$row['nama'],
				'idp'	=>$row['id'],
				'nip'	=>$row['nip'],
				'jabat'	=>$row['jabat'],
				'gol'	=>$row['singkat'],
				'tgkt'	=>$row['tingkat_jbt'],

			);
		}
		// minimal PHP 5.2
		//$this->session->set_flashdata('message',"<div style='color:green;'>NIK Sudah Ada!<div>");
		echo json_encode($arr);
		
	}

	public function v_berkas($idsppd,$idspt)
	{
	$thn=date('Y');
		$data = array('data' => $this->sppd->v_berkas($idsppd), 
				'dspt' => $this->sppd->data_spt($idspt),
				'berkas' => $this->sppd->data_berkas(),
				'idsppd' => $idsppd
				);
		//$this->konten['main_view'] = $this->load->view('v_sppd', $data, TRUE);
		if(($data['data'])==null){
		$this->konten['main_view'] = $this->load->view('v_berkas', $data, TRUE);
		}else{
		$this->konten['main_view'] = $this->load->view('v_aberkas', $data, TRUE);
		}

		$this->load->view('dashboard1', $this->konten);
	}

	public function tambah_berkas()
	{

		$idsppd=$this->input->post('idsppd');
		$idspt=$this->input->post('idspt');

		$sourceb='';
		$pilb=$this->input->post('checkon');
		if(!$pilb) {
		$bks='';
		}else{
		foreach($pilb as $one_pilb) {
		    $sourceb.=$one_pilb.",";
		}
		$bks=substr($sourceb,0,-1);
		}

		$data = array(
					'id' => '',
					'idsppd' => $this->input->post('idsppd'),
					'idberkas' => $bks,
					//'ket' => $this->input->post('ket')
				 );
		$this->sppd->tambah_datab($data);
		redirect('sppd/v_berkas/'.$idsppd.'/'.$idspt);
	}

	public function update_berkas()
	{
		$idsppd=$this->input->post('idsppd');
		$idspt=$this->input->post('idspt');
		$sourceb='';
		$pilb=$this->input->post('checkon');
		if(!$pilb) {
		$bks='';
		}else{
		foreach($pilb as $one_pilb) {
		    $sourceb.=$one_pilb.",";
		}
		$bks=substr($sourceb,0,-1);
		}

		$data = array(
					'idsppd' => $this->input->post('idsppd'),
					'idberkas' => $bks,
					);
		$this->sppd->update_datab($idsppd,$data);
		redirect('sppd/v_berkas/'.$idsppd.'/'.$idspt);
	}

	public function v_biaya($idsppd,$idspt,$idlok,$idtingkat)
	{
		$data = array('data' => $this->sppd->v_biaya($idsppd), 
				'dspt' => $this->sppd->data_spt($idspt),
				'glok' => $this->sppd->data_giatlok($idspt),
				'biaya' => $this->sppd->data_gb($idlok,$idtingkat),
				'idsppd' => $idsppd,
				'idtingkat'=>$idtingkat
				);
		//$this->konten['main_view'] = $this->load->view('v_sppd', $data, TRUE);
		if(($data['data'])==null){
		$this->konten['main_view'] = $this->load->view('v_biaya', $data, TRUE);
		}else{
		$this->konten['main_view'] = $this->load->view('v_abiaya', $data, TRUE);
		}

		$this->load->view('dashboard1', $this->konten);
	}

	public function tambah_biaya()
	{

		$idsppd=$this->input->post('idsppd');
		$idspt=$this->input->post('idspt');
		$idtingkat=$this->input->post('idtingkat');
		$idlok=$this->input->post('idp');

		$data = array(
					'id' => '',
					'idsppd' => $idsppd,
					'tglbyr' => $this->input->post('datepicker'),
					'lama' => $this->input->post('lama'),
					'idprov' => $this->input->post('idp'),
					'idtgkt' => $this->input->post('idt'),
				 );
		$this->sppd->tambah_databy($data);
		redirect('sppd/v_biaya/'.$idsppd.'/'.$idspt.'/'.$idlok.'/'.$idtingkat);
	}

	public function update_biaya()
	{
		$idsppd=$this->input->post('idsppd');
		$idspt=$this->input->post('idspt');
		$idtingkat=$this->input->post('idtingkat');
		$idlok=$this->input->post('idp');

		$data = array(
					'idsppd' => $idsppd,
					'tglbyr' => $this->input->post('datepicker'),
					'lama' => $this->input->post('lama'),
					'idprov' => $this->input->post('idp'),
					'idtgkt' => $this->input->post('idt'),
					);
		$this->sppd->update_databy($idsppd,$data);
		redirect('sppd/v_biaya/'.$idsppd.'/'.$idspt.'/'.$idlok.'/'.$idtingkat);
	}

	public function v_kwt($idsppd,$idspt)
	{

		$data = array('data' => $this->sppd->v_kwt($idsppd), 
				'dspt' => $this->sppd->data_spt($idspt),
				//'glok' => $this->sppd->data_giatlok($idspt),
				'biaya' => $this->sppd->data_kwt($idsppd),
				'ttd' => $this->sppd->data_ttd($idsppd),
				'ttdb' => $this->sppd->data_ttdb($idsppd),
				'idsppd' => $idsppd,
				//'idtingkat'=>$idtingkat
				);
		//$this->konten['main_view'] = $this->load->view('v_sppd', $data, TRUE);
		if(($data['data'])==null){
		$this->konten['main_view'] = $this->load->view('v_kwt', $data, TRUE);
		}else{
		$this->konten['main_view'] = $this->load->view('v_akwt', $data, TRUE);
		}

		$this->load->view('dashboard1', $this->konten);
	}

	public function tambah_kwt()
	{

		$idsppd=$this->input->post('idsppd');
		$idspt=$this->input->post('idspt');

		$data = array(
					'id' => '',
					'idsppd' => $idsppd,
					'tgl' => $this->input->post('datepicker'),
					'nokwt' => $this->input->post('nokwt'),
				 );
		$this->sppd->tambah_datak($data);
		redirect('sppd/v_kwt/'.$idsppd.'/'.$idspt);
	}

	public function update_kwt()
	{
		$idsppd=$this->input->post('idsppd');
		$idspt=$this->input->post('idspt');

		$data = array(
					'idsppd' => $idsppd,
					'idsppd' => $idsppd,
					'tgl' => $this->input->post('datepicker'),
					'nokwt' => $this->input->post('nokwt'),
					);
		$this->sppd->update_datak($idsppd,$data);
		redirect('sppd/v_kwt/'.$idsppd.'/'.$idspt);
	}

	public function cari_tanda1()
	{
		// tangkap variabel keyword dari URL
		$keyword = $this->uri->segment(3);

		// cari di database
		//$data = $this->db->select('kel.content as nmkel, kel.idkec, kec.content as nmkec ')->from('kel')->like('content',$keyword)->$this->db->join('kec', 'kel.idkec = kec.id')->get();	

		$data = array('data' => $this->sppd->cari_tanda1($keyword), 
				
		);

		// format keluaran di dalam array
		foreach($data['data'] as $row)
		{
			$arr['query'] = $keyword;
			$arr['suggestions'][] = array(
				'value'	=>$row['namap'],
				'nipbe'	=>$row['nip'],
				'idjb'	=>$row['id']

			);
		}
		// minimal PHP 5.2
		//$this->session->set_flashdata('message',"<div style='color:green;'>NIK Sudah Ada!<div>");
		echo json_encode($arr);
		
	}

	public function cetak($id)
	{

		$data = array('data' => $this->sppd->cetak($id),
				'cikut' => $this->sppd->get_cikut($id), 

		);

	//$this->load->view('client/layout/identitas', $data);//, true); //render the view into HTML

        $sumber = $this->load->view('layout/spdpdf', $data, TRUE);
        $html = $sumber;
 
 
        $pdfFilePath = "sppd.pdf";
        $pdf = $this->m_pdf->load();
 
        $pdf->AddPage('P','Folio');
        $pdf->WriteHTML($html);
        $pdf->Output($pdfFilePath, "I");
     	}

	public function cetak_kwt($idsppd)
	{

		$data = array('data' => $this->sppd->cetak_kwt($idsppd),
				

		);

	//$this->load->view('client/layout/identitas', $data);//, true); //render the view into HTML

        $sumber = $this->load->view('layout/kwtpdf', $data, TRUE);
        $html = $sumber;
 
 
        $pdfFilePath = "kwitansi.pdf";
        $pdf = $this->m_pdf->load();
 
        $pdf->AddPage('P','Folio');
        $pdf->WriteHTML($html);
        $pdf->Output($pdfFilePath, "I");
     	}


}

/* End of file eselon.php */
/* Location: ./application/controllers/eselon.php */
