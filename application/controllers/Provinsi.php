<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Provinsi extends CI_Controller {

	public $konten = array('main_view' => '', );

	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_provinsi', 'provinsi', TRUE);
	}

	public function index()
	{

		$jumlah_data = $this->provinsi->jumlah_data();
		$this->load->library('pagination');
		$config['base_url'] = base_url().'index.php/provinsi/index/';
		$config['total_rows'] = $jumlah_data;
		$config['per_page'] = 5;
		$from = $this->uri->segment(3);
		$this->pagination->initialize($config);

		$data = array('data' => $this->provinsi->selectAll($config['per_page'],$from),
		);
		$this->konten['main_view'] = $this->load->view('listprovinsi', $data, TRUE);
		$this->load->view('dashboard1', $this->konten);
	}

	public function tambah_data()
	{
		$data = array(
					'id' => '',
					'nama_provinsi' => $this->input->post('np'),
					'ut' => $this->input->post('ut'),
										 );
		$this->provinsi->tambah_data($data);
		redirect('provinsi');
	}
	public function update()
	{
	$id = $this->input->post('id');
		$data = array(
		// ini nama table di db dan np adalah name di update_jabatan
					'nama_provinsi' => $this->input->post('np'),
					'ut' => $this->input->post('ut'),
										 );
		$this->provinsi->update_data($id,$data);
		redirect('provinsi');
	}
		public function get_id($id)
	{
		$data['data'] = $this->provinsi->get_id($id);
		$this->konten['main_view'] = $this->load->view('update_provinsi', $data, TRUE);
		$this->load->view('dashboard1', $this->konten);
	}
	public function hapus($id)
	{
		$this->provinsi->hapus_data($id);
		redirect('provinsi');
	}
}

/* End of file eselon.php */
/* Location: ./application/controllers/eselon.php */
