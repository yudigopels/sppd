<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jabatan extends CI_Controller {

	public $konten = array('main_view' => '', );

	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_jabatan', 'jabatan', TRUE);
	}

	public function index()
	{

		$jumlah_data = $this->jabatan->jumlah_data();
		$this->load->library('pagination');
		$config['base_url'] = base_url().'index.php/jabatan/index/';
		$config['total_rows'] = $jumlah_data;
		$config['per_page'] = 5;
		$from = $this->uri->segment(3);
		$this->pagination->initialize($config);

		$data = array('data' => $this->jabatan->selectAll($config['per_page'],$from),
		);
		$this->konten['main_view'] = $this->load->view('listjabatan', $data, TRUE);
		$this->load->view('dashboard1', $this->konten);
	}

	public function tmbh()
	{
		$data = array('data' => $this->jabatan->selectAll(), );
		$this->konten['main_view'] = $this->load->view('modal/addjabatan', $data, TRUE);
		$this->load->view('dashboard1', $this->konten);
	}
	public function tambah_data()
	{
		// $isi =  preg_split('/\r\n|[\r\n]/', $this->input->post('isi'));
		// print_r($isi);
		$data = array(
					'id' => '',
					'content' => $this->input->post('content'),
										 );
		$this->jabatan->tambah_data($data);
		redirect('jabatan');
	}
	public function update()
	{
	$id = $this->input->post('id');
		$data = array(
		// ini nama table di db dan jab adalah name di update_jabatan
					'content' => $this->input->post('jab'),
										 );
		$this->jabatan->update_data($id,$data);
		redirect('jabatan');
	}
		public function get_id($id)
	{
		$data['data'] = $this->jabatan->get_id($id);
		$this->konten['main_view'] = $this->load->view('update_jabatan', $data, TRUE);
		$this->load->view('dashboard1', $this->konten);
	}
	public function hapus($id)
	{
		$this->jabatan->hapus_data($id);
		redirect('jabatan');
	}
}

/* End of file eselon.php */
/* Location: ./application/controllers/eselon.php */
