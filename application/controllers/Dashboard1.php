<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard1 extends CI_Controller {

	public $konten = array('main_view' => '', );

	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_sppd', 'sppd', TRUE);
		$this->load->model('m_pegawai', 'peg', TRUE);
	}

	public function index()
	{
		$data = array(
		'sppd' => $this->sppd->jumlah_data(),
		'kwt' => $this->sppd->jumlah_kwt(),
		'peg' => $this->peg->jumlah_data(),
				);

		$this->konten['main_view'] = $this->load->view('konten', $data, TRUE);
		//$this->konten['main_view'] = $this->load->view('client/listpeg', $data, TRUE);
		$this->load->view('dashboard1', $this->konten);
	}
}
