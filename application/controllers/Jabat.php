<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jabat extends CI_Controller {

	public $konten = array('main_view' => '', );

	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_jabat', 'jabat', TRUE);
	}

	public function index()
	{
		$data = array('data' => $this->jabat->selectAll(),
		);
		$this->konten['main_view'] = $this->load->view('listjabat', $data, TRUE);
		$this->load->view('dashboard1', $this->konten);
	}

	
	public function tambah_data()
	{
		// $isi =  preg_split('/\r\n|[\r\n]/', $this->input->post('isi'));
		// print_r($isi);
		$data = array(
					'id' => '',
					'nip' => $this->input->post('nip'),
					'namap' => $this->input->post('namap'),
					'jabat' => $this->input->post('jabat'),
										 );
		$this->jabat->tambah_data($data);
		redirect('jabat');
	}
	public function update()
	{
	$id = $this->input->post('id');
		$data = array(
					'nip' => $this->input->post('nip'),
					'namap' => $this->input->post('namap'),
					'jabat' => $this->input->post('jabat'),
										 );
		$this->jabat->update_data($id,$data);
		redirect('jabat');
	}
	public function get_id($id)
	{
		$data['data'] = $this->jabat->get_id($id);
		$this->konten['main_view'] = $this->load->view('update_jabat', $data, TRUE);
		$this->load->view('dashboard1', $this->konten);
	}
	public function hapus($id)
	{
		$this->jabatan->hapus_data($id);
		redirect('jabatan');
	}
}

/* End of file eselon.php */
/* Location: ./application/controllers/eselon.php */
