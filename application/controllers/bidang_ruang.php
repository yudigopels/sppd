<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class bidang_ruang extends CI_Controller {

	public $konten = array('main_view' => '', );

	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_bidang_ruang', 'bidang_ruang', TRUE);
	}

	public function index()
	{
		$data = array('data' => $this->bidang_ruang->selectAll(),
		);
		$this->konten['main_view'] = $this->load->view('listbidang_ruang', $data, TRUE);
		$this->load->view('dashboard1', $this->konten);
	}

	public function tmbh()
	{
		$data = array('data' => $this->bidang_ruang->selectAll(), );
		$this->konten['main_view'] = $this->load->view('modal/addbidang_ruang', $data, TRUE);
		$this->load->view('dashboard1', $this->konten);
	}
	public function tambah_data()
	{
		
		$data = array(
					'id' => '',
					'bidang_ruang' => $this->input->post('bd'),
										 );
		$this->bidang_ruang->tambah_data($data);
		redirect('bidang_ruang');
	}
	public function update()
	{
	$id = $this->input->post('id');
		$data = array(
		// ini nama table di db dan jab adalah name di update_jabatan
					'bidang_ruang' => $this->input->post('bd'),
										 );
		$this->bidang_ruang->update_data($id,$data);
		redirect('bidang_ruang');
	}
		public function get_id($id)
	{
		$data['data'] = $this->bidang_ruang->get_id($id);
		$this->konten['main_view'] = $this->load->view('update_bidang_ruang', $data, TRUE);
		$this->load->view('dashboard1', $this->konten);
	}
	public function hapus($id)
	{
		$this->bidang_ruang->hapus_data($id);
		redirect('bidang_ruang');
	}
}

/* End of file eselon.php */
/* Location: ./application/controllers/eselon.php */
