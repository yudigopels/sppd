<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pegawai extends CI_Controller {

	public $konten = array('main_view' => '', );

	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_pegawai', 'pegawai', TRUE);
	}

	public function index()
	{

		$jumlah_data = $this->pegawai->jumlah_data();
		$this->load->library('pagination');
		$config['base_url'] = base_url().'index.php/pegawai/index/';
		$config['total_rows'] = $jumlah_data;
		$config['per_page'] = 5;
		$from = $this->uri->segment(3);
		$this->pagination->initialize($config);

		$data = array('data' => $this->pegawai->selectAll($config['per_page'],$from), 
						'gol' => $this->pegawai->gol(),
						'jab' => $this->pegawai->jab(),
						);
		//$this->konten['sidebar'] = $this->load->view('client/sidebar', array(), TRUE);
		$this->konten['main_view'] = $this->load->view('listpeg', $data, TRUE);
		$this->load->view('dashboard1', $this->konten);
	}
		
	public function get_id($id)
	{
		$data = array(
		'data' => $this->pegawai->get_id($id),
		'gol' => $this->pegawai->gol(),
		'jab' => $this->pegawai->jab(),
				);
		
		$this->konten['main_view'] = $this->load->view('update_pegawai', $data, TRUE);
		$this->load->view('dashboard1', $this->konten);

	}
		public function tmbh()
	{
		$data = array('data' => $this->pegawai->selectAll(), );
		$this->konten['main_view'] = $this->load->view('modal/addpegawai', $data, TRUE);
		$this->load->view('dashboard1', $this->konten);
	}
	public function tambah_data()
	{
		$data = array(
					'id' => '',
					'nip' => $this->input->post('nip'),
					'nama' => $this->input->post('nama'),
					'jab' => $this->input->post('idjb'),
					'gol' => $this->input->post('idg'),
										 );
		$this->pegawai->tambah_data($data);
		redirect('pegawai');
	}
		public function update()
	{
		$id = $this->input->post('id');
		$data = array(			
					'nip' => $this->input->post('nip'),
					'nama' => $this->input->post('nama'),
					'jab' => $this->input->post('idjb'),
					'gol' => $this->input->post('idg'),							
										 );
		$this->pegawai->update_data($id,$data);
		redirect('pegawai');
	}
	
	public function hapus($id)
	{
		$this->pegawai->hapus_data($id);
		redirect('pegawai');
	}

	public function searchjab()
	{
		// tangkap variabel keyword dari URL
		$keyword = $this->uri->segment(3);

		// cari di database
		//$data = $this->db->select('kel.content as nmkel, kel.idkec, kec.content as nmkec ')->from('kel')->like('content',$keyword)->$this->db->join('kec', 'kel.idkec = kec.id')->get();	

		$data = array('data' => $this->pegawai->cari_j($keyword), 
				
		);

		// format keluaran di dalam array
		foreach($data['data'] as $row)
		{
			$arr['query'] = $keyword;
			$arr['suggestions'][] = array(
				'value'	=>$row['content'],
				'idjb'	=>$row['id'],

			);
		}
		// minimal PHP 5.2
		//$this->session->set_flashdata('message',"<div style='color:green;'>NIK Sudah Ada!<div>");
		echo json_encode($arr);
		
	}

	public function searchgol()
	{
		// tangkap variabel keyword dari URL
		$keyword = $this->uri->segment(3);

		// cari di database
		//$data = $this->db->select('kel.content as nmkel, kel.idkec, kec.content as nmkec ')->from('kel')->like('content',$keyword)->$this->db->join('kec', 'kel.idkec = kec.id')->get();	

		$data = array('data' => $this->pegawai->cari_g($keyword), 
				
		);

		// format keluaran di dalam array
		foreach($data['data'] as $row)
		{
			$arr['query'] = $keyword;
			$arr['suggestions'][] = array(
				'value'	=>$row['singkat'],
				'idg'	=>$row['id'],

			);
		}
		// minimal PHP 5.2
		//$this->session->set_flashdata('message',"<div style='color:green;'>NIK Sudah Ada!<div>");
		echo json_encode($arr);
		
	}

}

/* End of file eselon.php */
/* Location: ./application/controllers/eselon.php */
