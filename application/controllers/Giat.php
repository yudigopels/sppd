<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Giat extends CI_Controller {

	public $konten = array('main_view' => '', );

	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_giat', 'giat', TRUE);
	}

	public function index()
	{

		$jumlah_data = $this->giat->jumlah_data();
		$this->load->library('pagination');
		$config['base_url'] = base_url().'index.php/giat/index/';
		$config['total_rows'] = $jumlah_data;
		$config['per_page'] = 5;
		$from = $this->uri->segment(3);
		$this->pagination->initialize($config);

		$data = array('data' => $this->giat->selectAll($config['per_page'],$from), 
						'lok' => $this->giat->lok(),
						//'jab' => $this->pegawai->jab(),
						);
		//$this->konten['sidebar'] = $this->load->view('client/sidebar', array(), TRUE);
		$this->konten['main_view'] = $this->load->view('listgiat', $data, TRUE);
		$this->load->view('dashboard1', $this->konten);
	}
		
	public function get_id($id)
	{
		$data = array(
		'data' => $this->giat->get_id($id),
		'loka' => $this->giat->lok(),
				);
		
		$this->konten['main_view'] = $this->load->view('update_giat', $data, TRUE);
		$this->load->view('dashboard1', $this->konten);

	}

	public function tambah_data()
	{
		$data = array(
					'id' => '',
					'nama' => $this->input->post('nama'),
					'tahun' => $this->input->post('tahun'),
					'akun' => $this->input->post('akun'),
					'lokasi' => $this->input->post('idkeg'),
					'skpd' => $this->input->post('skpd'),
					 );
		$this->giat->tambah_data($data);
		redirect('giat');
	}
		public function update()
	{
		$id = $this->input->post('id');
		$data = array(			
					'nama' => $this->input->post('nama'),
					'tahun' => $this->input->post('tahun'),
					'akun' => $this->input->post('akun'),
					'lokasi' => $this->input->post('idkeg'),
					'skpd' => $this->input->post('skpd'),							
										 );
		$this->giat->update_data($id,$data);
		redirect('giat');
	}
	
	public function hapus($id)
	{
		$this->giat->hapus_data($id);
		redirect('giat');
	}

	public function search()
	{
		// tangkap variabel keyword dari URL
		$keyword = $this->uri->segment(3);

		// cari di database
		//$data = $this->db->select('kel.content as nmkel, kel.idkec, kec.content as nmkec ')->from('kel')->like('content',$keyword)->$this->db->join('kec', 'kel.idkec = kec.id')->get();	

		$data = array('data' => $this->giat->carilok($keyword), 
				
		);

		// format keluaran di dalam array
		foreach($data['data'] as $row)
		{
			$arr['query'] = $keyword;
			$arr['suggestions'][] = array(
				'value'	=>$row['nama_provinsi'],
				'idk'	=>$row['id']

			);
		}
		// minimal PHP 5.2
		//$this->session->set_flashdata('message',"<div style='color:green;'>NIK Sudah Ada!<div>");
		echo json_encode($arr);
		
	}

}

/* End of file eselon.php */
/* Location: ./application/controllers/eselon.php */
