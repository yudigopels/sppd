<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kwitansi extends CI_Controller {

	public $konten = array('main_view' => '', );

	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_kwitansi', 'kwitansi', TRUE);
	}

	public function index()
	{
	$data = array('data' => $this->kwitansi->selectAll(), 
						'nosppd' => $this->kwitansi->nosppd(),
																		
						);
		$this->konten['main_view'] = $this->load->view('listkwitansi', $data, TRUE);
		$this->load->view('dashboard1', $this->konten);
	}
public function tmbh()
	{
		$data = array('data' => $this->kwitansi->selectAll(), );
		$this->konten['main_view'] = $this->load->view('modal/add_kwitansi', $data, TRUE);
		$this->load->view('dashboard1', $this->konten);
	}
public function get_id($id)
	{
		$data=array(
			'data' => $this->kwitansi->get_id($id),
			'nosppd' => $this->kwitansi->nosppd(),
			
			);
		$this->konten['main_view'] = $this->load->view('update_kwitansi', $data, TRUE);
		$this->load->view('dashboard1', $this->konten);
	}

	public function tambah()
	{
		$data = array(
			'id' => '',
			'no_kwitansi'=>$this->input->post('no_kwitansi'),
			'no_sppd' => $this->input->post('nosppd_add'),
			'transportasi' => $this->input->post('transportasi'),
			'konsumsi' => $this->input->post('konsumsi'),
			'penginapan' => $this->input->post('penginapan'),
			'kendaraan_lain' => $this->input->post('kendaraan_lain'),
						
			 );

		$this->kwitansi->tambah_data($data);
		redirect('kwitansi');
	}
public function update()
	{
        $id = $this->input->post('id');
		$data = array(
			'no_kwitansi'=>$this->input->post('no_kwitansi'),
			'no_sppd' => $this->input->post('nosppd_up'),
			'transportasi' => $this->input->post('transportasi'),
			'konsumsi' => $this->input->post('konsumsi'),
			'penginapan' => $this->input->post('penginapan'),
			'kendaraan_lain' => $this->input->post('kendaraan_lain'),
			//'sttb' => $this->input->post('sttb'),
			//'tsttb' => $tmt,
			//'tlls' => $tsk,
			
			 );

		$this->kwitansi->update_data($id,$data);
		redirect('kwitansi');
	}
public function hapus($id)
	{
		$this->kwitansi->hapus_data($id);
		redirect('kwitansi');
	}

}

/* End of file eselon.php */
/* Location: ./application/controllers/eselon.php */
