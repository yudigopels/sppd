<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Spt extends CI_Controller {

	public $konten = array('main_view' => '', );

	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_spt', 'spt', TRUE);
	}

	public function index(){
	$data = array('data' => $this->sppd->selectAll(), 
			'nips' => $this->sppd->nips(),						
						);
		$this->konten['main_view'] = $this->load->view('listsppd', $data, TRUE);
		$this->load->view('dashboard1', $this->konten);
	}
	public function giat($idkeg,$lok)
	{
		$data = array('data' => $this->spt->giat($idkeg), 
				//'peg' => $this->sppd->peg(), 
				//'lok' => $this->sppd->ambil_lok(), 
				'idkeg' => $idkeg,
				'lok' => $lok,
				//'provinsi'=>$this->sppd->ambil_peg(),
				);
		$this->konten['main_view'] = $this->load->view('spt', $data, TRUE);
		$this->load->view('dashboard1', $this->konten);
	}
	public function pilih_kabupaten(){
		$data['kabupaten']=$this->sppd->ambil_kabupaten($this->uri->segment(3));
		$this->load->view('v_drop_down_bid',$data);
	}

	public function pilih_lok(){
		$data['ut']=$this->sppd->ambil_loka($this->uri->segment(3));
		$this->load->view('v_drop_down_lok',$data);
	}

	public function pilih_um(){
		$data['um']=$this->sppd->ambil_um($this->uri->segment(3));
		$this->load->view('v_drop_down_um',$data);
	}

	public function tambah()
	{

	
		$data = array(
					'id' => '',
					'no_spt' => $this->input->post('no_spt'),
					'idkeg' => $this->input->post('idkeg'),
					'idpeg' => $this->input->post('idp'),
					'idjabat' => $this->input->post('idj'),
					'tgl_spt' => $this->input->post('datepicker'),
					'tgl_berangkat' => $this->input->post('datepicker1'),
					'tgl_kembali' => $this->input->post('datepicker2'),
					'angkutan' => $this->input->post('angkut'),
					'tempat_brgkt' => $this->input->post('brgkt'),
					'tujuan' => $this->input->post('tuju'),
					'ket' => $this->input->post('ket')
				 );
		$this->spt->tambah_data($data);
		redirect('spt/giat/'.$this->input->post('idkeg').'/'.$this->input->post('lokasi'));
	}
	public function update()
	{
		$id = $this->input->post('id');
		$idkeg = $this->input->post('idkeg');
		$lok = $this->input->post('lokasi');
		$data = array(
					'no_spt' => $this->input->post('no_spt'),
					'idkeg' => $this->input->post('idkeg'),
					'idpeg' => $this->input->post('idp'),
					'idjabat' => $this->input->post('idj'),
					'tgl_spt' => $this->input->post('datepicker'),
					'tgl_berangkat' => $this->input->post('datepicker1'),
					'tgl_kembali' => $this->input->post('datepicker2'),
					'angkutan' => $this->input->post('angkut'),
					'tempat_brgkt' => $this->input->post('brgkt'),
					'tujuan' => $this->input->post('tuju'),
					'ket' => $this->input->post('ket')
					);
		$this->spt->update_data($id,$data);
		redirect('spt/giat/'.$idkeg.'/'.$lok);
		}
	public function get_id($id,$idkeg,$lok)
	{
		$data = array(
			'data' => $this->spt->get_id($id),
			'idkeg' => $idkeg,
			'lok' => $lok,
		);		
		$this->konten['main_view'] = $this->load->view('update_spt', $data, TRUE);
		$this->load->view('dashboard1', $this->konten);

	}
	public function hapus($id,$idkeg,$lok)
	{
		$this->spt->hapus_data($id);
		redirect('spt/giat/'.$idkeg.'/'.$lok);
	}

	public function cari_peg()
	{
		// tangkap variabel keyword dari URL
		$keyword = $this->uri->segment(3);

		// cari di database
		//$data = $this->db->select('kel.content as nmkel, kel.idkec, kec.content as nmkec ')->from('kel')->like('content',$keyword)->$this->db->join('kec', 'kel.idkec = kec.id')->get();	

		$data = array('data' => $this->spt->cari_peg($keyword), 
				
		);

		// format keluaran di dalam array
		foreach($data['data'] as $row)
		{
			$arr['query'] = $keyword;
			$arr['suggestions'][] = array(
				'value'	=>$row['nama'],
				'idp'	=>$row['id']

			);
		}
		// minimal PHP 5.2
		//$this->session->set_flashdata('message',"<div style='color:green;'>NIK Sudah Ada!<div>");
		echo json_encode($arr);
		
	}

	public function cari_tanda()
	{
		// tangkap variabel keyword dari URL
		$keyword = $this->uri->segment(3);

		// cari di database
		//$data = $this->db->select('kel.content as nmkel, kel.idkec, kec.content as nmkec ')->from('kel')->like('content',$keyword)->$this->db->join('kec', 'kel.idkec = kec.id')->get();	

		$data = array('data' => $this->spt->cari_tanda($keyword), 
				
		);

		// format keluaran di dalam array
		foreach($data['data'] as $row)
		{
			$arr['query'] = $keyword;
			$arr['suggestions'][] = array(
				'value'	=>$row['namap'],
				'nip'	=>$row['nip'],
				'idj'	=>$row['id']

			);
		}
		// minimal PHP 5.2
		//$this->session->set_flashdata('message',"<div style='color:green;'>NIK Sudah Ada!<div>");
		echo json_encode($arr);
		
	}

	public function cetak($id)
	{

		$data = array('data' => $this->spt->cetak($id), 

		);

	//$this->load->view('client/layout/identitas', $data);//, true); //render the view into HTML

        $sumber = $this->load->view('layout/sptpdf', $data, TRUE);
        $html = $sumber;
 
 
        $pdfFilePath = "spt.pdf";
        $pdf = $this->m_pdf->load();
 
        $pdf->AddPage('P','Folio');
        $pdf->WriteHTML($html);
        $pdf->Output($pdfFilePath, "I");
     	}

}

/* End of file eselon.php */
/* Location: ./application/controllers/eselon.php */
