<div class="col-lg-10 col-xs-6">
<div class="col-lg-10 col-xs-6">

<a class="btn btn-success" data-toggle="modal" href='#modal-id' role="button"><i class="fa fa-plus">Tambah Data Golongan</i></a>
                  		<?php $this->load->view('modal/add_pangkat'); ?>
</div>


            <!-- small box -->
           <!-- <div class="table-responsive">-->
		    <div class="modal-info .modal-header">
			<!-- <div class="modal-header" style="background-color: #eef0f0;">-->
                <table class="table">
					    <thead>
						    <tr class="bg-orange-active">
						        <th>No</th>
							<th>Golongan</th>
						        <th>Nama</th>
							<th>Tingkat Biaya</th>
						        <th></th>
						    </tr>
					    </thead>
					    <tbody>
					    <?php $no = $this->uri->segment('3') + 1;//$no = 1; ?>
					    <?php foreach ($data as $d): ?>					    	
					    	<tr>
					    		<td><?php echo $no++; ?></td>
					    		<td><?php echo $d['singkat'] ?></td>
					    		<td><?php echo $d['content'] ?></td>
							<td><?php echo $d['tgkt'] ?></td>
					    		<td>
<a class="btn btn-primary btn-sm" href="<?php echo site_url('pangkat/get_id/').'/'.$d['id'] ?>" ><span style="color: #fff;" class="glyphicon glyphicon-pencil"></span> Edit</a>
<a class="btn btn-danger btn-sm" href="<?php echo site_url('pangkat/hapus/').'/'.$d['id'] ?>" ><span style="color: #fff;" class="glyphicon glyphicon-trash"></span> Hapus</a>
					    		</td>
					    	</tr>
					    <?php endforeach ?>
					    </tbody>
					</table>
            </div>
<div class="halaman">Halaman : <?php echo $this->pagination->create_links(); ?></div>
</div>
