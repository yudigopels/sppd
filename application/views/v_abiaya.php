<section class="content-header">
    <h1>
        <small>Verifikasi Biaya</small>
    </h1>

</section>

<div class="col-xs-12">
<div class="panel-body">

<?php echo form_open('sppd/update_biaya'); ?>
<?php foreach($dspt as $d){  ?>


<?php
$tgspt=$d['tgl_spt'];
$pisah = explode('-',$tgspt);

switch($pisah[1]){       
        case 1 : {
                    $bln='Januari';
                }break;
        case 2 : {
                    $bln='Februari';
                }break;
        case 3 : {
                    $bln='Maret';
                }break;
        case 4 : {
                    $bln='April';
                }break;
        case 5 : {
                    $bln='Mei';
                }break;
        case 6 : {
                    $bln="Juni";
                }break;
        case 7 : {
                    $bln='Juli';
                }break;
        case 8 : {
                    $bln='Agustus';
                }break;
        case 9 : {
                    $bln='September';
                }break;
        case 10 : {
                    $bln='Oktober';
                }break;     
        case 11 : {
                    $bln='November';
                }break;
        case 12 : {
                    $bln='Desember';
                }break;
        default: {
                    $bln='UnKnown';
                }break;
    }


$array = array($pisah[2],$bln,$pisah[0]);
$tglspt = implode(' ',$array);

$tgb=$d['tgl_berangkat'];
$pisah1 = explode('-',$tgb);

switch($pisah1[1]){       
        case 1 : {
                    $bln='Januari';
                }break;
        case 2 : {
                    $bln='Februari';
                }break;
        case 3 : {
                    $bln='Maret';
                }break;
        case 4 : {
                    $bln='April';
                }break;
        case 5 : {
                    $bln='Mei';
                }break;
        case 6 : {
                    $bln="Juni";
                }break;
        case 7 : {
                    $bln='Juli';
                }break;
        case 8 : {
                    $bln='Agustus';
                }break;
        case 9 : {
                    $bln='September';
                }break;
        case 10 : {
                    $bln='Oktober';
                }break;     
        case 11 : {
                    $bln='November';
                }break;
        case 12 : {
                    $bln='Desember';
                }break;
        default: {
                    $bln='UnKnown';
                }break;
    }


$arrayb = array($pisah1[2],$bln,$pisah1[0]);
$brgkt = implode(' ',$arrayb);

$tgk=$d['tgl_kembali'];
$pisah2 = explode('-',$tgk);

switch($pisah2[1]){       
        case 1 : {
                    $bln='Januari';
                }break;
        case 2 : {
                    $bln='Februari';
                }break;
        case 3 : {
                    $bln='Maret';
                }break;
        case 4 : {
                    $bln='April';
                }break;
        case 5 : {
                    $bln='Mei';
                }break;
        case 6 : {
                    $bln="Juni";
                }break;
        case 7 : {
                    $bln='Juli';
                }break;
        case 8 : {
                    $bln='Agustus';
                }break;
        case 9 : {
                    $bln='September';
                }break;
        case 10 : {
                    $bln='Oktober';
                }break;     
        case 11 : {
                    $bln='November';
                }break;
        case 12 : {
                    $bln='Desember';
                }break;
        default: {
                    $bln='UnKnown';
                }break;
    }


$arrayk = array($pisah2[2],$bln,$pisah2[0]);
$kem = implode(' ',$arrayk);
?>

<div class="form-group row">
	<div class="col-xs-5">

	<label style="font-size: 14px;">No SPT</label>
	<input type="text" name="kode_spd" id="inputKode" class="form-control input-sm" placeholder="Kode SPD....." required="required" value="<?php echo $d['no_spt'] ?>" readonly="yes">
	<input type="hidden" name="idspt" id="idspt" class="form-control" value="<?php echo $d['id']; ?>">
	<input type="hidden" name="idsppd" id="idsppd" class="form-control" value="<?php echo $idsppd; ?>">
	<input type="hidden" name="idtingkat" id="idtingkat" class="form-control" value="<?php echo $idtingkat; ?>">
	</div>

<div class="col-xs-3">
<label style="font-size: 14px;">Tgl SPT </label>
<input type="text" name="tgspt" id="tgspt" class="form-control" readonly="yes" value="<?php echo $tglspt ?>">
</div>
</div>




<div class="form-group row">

<div class="col-xs-3">
<label style="font-size: 14px;">Tujuan </label>
<input type="text" name="tuju" id="tuju" class="autospt form-control" readonly="yes" value="<?php echo $d['tujuan'] ?>">
</div>

<div class="col-xs-5">
<label style="font-size: 14px;">Waktu Pelaksanaan </label>
<input type="text" name="wkt" id="wkt" class="form-control" readonly="yes" value="<?php echo $brgkt.' s/d '.$kem ?>">
</div>

</div>

<div class="form-group row">
	<div class="col-xs-3">
	<label style="font-size: 14px;">Nama Pegawai </label>
	<input type="text" name="nmpeg" id="nmpeg" class="autospt form-control" readonly="yes" value="<?php echo $d['nama'] ?>">
	</div>

	<div class="col-xs-3">
	<label style="font-size: 14px;">NIP Pegawai </label>
	<input type="text" name="nipp" id="nipp" class="autospt form-control" readonly="yes" value="<?php echo $d['nip'] ?>">
	</div>

	<div class="col-xs-3">
	<label style="font-size: 14px;">Golongan/Ruang </label>
	<input type="text" name="gol" id="gol" class="autospt form-control" readonly="yes" value="<?php echo $d['pgkt'] ?>">
	</div>

	<div class="col-xs-3">
	<label style="font-size: 14px;">Jabatan </label>
	<input type="text" name="idjab" id="idjab" class="autospt form-control" readonly="yes" value="<?php echo $d['jabat'] ?>">
	</div>


</div>

<div class="form-group row">
	<div class="col-xs-8">
	<label style="font-size: 14px;">Kegiatan </label>
	<input type="text" name="tgkt" id="tgkt" class="autospt form-control" readonly="yes" value="<?php echo $d['kegiatan'] ?>">
	</div>


</div>


<div class="form-group row">
<div class="col-xs-2">
<label style="font-size: 14px;">Lama Perjalanan </label>
<?php
$tgl1 = $tgb;  // 1 Oktober 2009
$tgl2 = $tgk;  // 10 Oktober 2009

//$today=date('Y-m-d');
$day2=strtotime($tgl2);
$day1=strtotime($tgl1);
$selisih=$day2-$day1;
$hari=$selisih/(60*60*24);
$um=$hari;
//$uh=round($um,1);
?>

<input type="text" name="lama" id="lama" class="form-control" value="<?php echo $um.' Hari'; ?>" readonly="yes">
</div>

<?php foreach($glok as $g){  ?>
	<div class="col-xs-2">
	<label style="font-size: 14px;">Uang Transportasi </label>
	<input type="text" name="uhari" id="uhari" class="form-control" readonly="yes" value="<?php echo rupiah($g['ut']) ?>">
	<input type="hidden" name="idp" id="idp" class="form-control" readonly="yes" value="<?php echo $g['idprov'] ?>">
	</div>

	<div class="col-xs-2">
	<label style="font-size: 14px;">Tingkat Biaya </label>
	<input type="text" name="tgkt" id="tgkt" class="form-control" readonly="yes" value="<?php echo $g['tingkat_jbt']?>">
	</div>
<?php }?>

<?php foreach($biaya as $b){  ?>
	<div class="col-xs-2">
	<label style="font-size: 14px;">Uang Makan </label>
	<input type="text" name="um" id="um" class="form-control" readonly="yes" value="<?php echo rupiah($b['um']) ?>">
	</div>

<?php
$usaku=$b['us']*$um;
?>
	<div class="col-xs-2">
	<label style="font-size: 14px;">Uang Saku </label>
	<input type="text" name="saku" id="saku" class="form-control" readonly="yes" value="<?php echo rupiah($usaku)?>">
	<input type="hidden" name="idt" id="idt" class="form-control" readonly="yes" value="<?php echo $b['id']?>">
	</div>
<?php }?>

</div>

<div class="form-group row">
<?php foreach($data as $db){  ?>
<div class="col-xs-2">
	<label style="font-size: 14px;">Tgl Bayar</label>
	<div class="input-group date">
		<div class="input-group-addon">
		<i class="fa fa-calendar"></i>
		</div>
	<input type="text" name="datepicker" class="form-control pull-right" id="datepicker" value="<?php echo $db['tglbyr']?>">
	</div>
</div>
<?php }?>
</div>

<div class="form-group row">

	<div class="col-xs-4">
	<!--<button type="button" class="btn btn-default" data-dismiss="modal"><span style="color: #fff;" class="glyphicon glyphicon-remove-circle"></span> Tutup</button>-->
<a class="btn btn-info" href="<?php echo site_url('sppd/v_sppd') ?>" title=""><i class="ion ion-android-arrow-back"></i> Kembali</a>
	<button type="submit" class="btn btn-primary"><span style="color: #fff;" class="glyphicon glyphicon-floppy-saved"></span>Simpan</button>
	</div>
</div>

<?php }?>
<?php echo form_close(); ?>

</div><!--smallbox-->
</div>


