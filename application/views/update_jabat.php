<div id="page-wrapper">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <ol class="breadcrumb">
				<li>
					<a href="#"><i class="fa fa-dashboard"></i> Dashboard</a>
				</li>
				<li class="active"><i class="fa fa-wrench fa-fw"></i>Update Pejabat</li>
			</ol>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="panel panel-default">
                  <div class="panel-body" align="justify">	
				 <?php echo form_open('jabat/update'); ?>
<?php foreach($data as $row){  ?>

		<div class="form-group">					
			<label class="col-sm-2 control-label">NIP</label>
			<div class="col-sm-10">
<input type="text" name="nip" id="inputKode" class="form-control" value="<?php echo $row['nip']; ?>">		
<input type="hidden" name="id" id="inputKode" class="form-control" value="<?php echo $row['id']; ?>">	
			</div>
		</div>
<div class="form-group">					
			<label class="col-sm-2 control-label">Nama Pejabat</label>
			<div class="col-sm-10">
<input type="text" name="namap" id="inputKode" class="form-control" value="<?php echo $row['namap']; ?>">		

			</div>
		</div>	
		
<div class="form-group">					
			<label class="col-sm-2 control-label">Jabatan</label>
			<div class="col-sm-10">

<input type="text" name="jabat" id="inputKode" class="form-control" value="<?php echo $row['jabat']; ?>">	
<!--<select name="jabat" id="jabat" class="form-control">
<?php		
if (($d['status'])==1){
?>

				<option value="1" selected="selected">Pejabat Pembuat Komitmen</option>
				<option value="2">Bagian Keuangan</option>
				<option value="3">Bendahara Pengeluaran</option>

<?php
;}elseif (($d['status'])==2){ 
?>

				<option value="1">Pejabat Pembuat Komitmen</option>
				<option value="2" selected="selected">Bagian Keuangan</option>
				<option value="3">Bendahara Pengeluaran</option>

<?php
}else ;{ ?>

				<option value="1">Pejabat Pembuat Komitmen</option>
				<option value="2">Bagian Keuangan</option>
				<option value="3" selected="selected">Bendahara Pengeluaran</option>
 <?php } ?>
          			
		
           	</select>-->	

			</div>
		</div>	
		
<div class="form-group">
<label class="col-sm-2 control-label"></label>
<div class="col-sm-10">


<br><a href="<?php echo site_url('jabat') ?>" class="btn btn-danger"><span style="color: #fff;" class="glyphicon glyphicon-circle-arrow-left"></span> Kembali</a>
<button type="submit" class="btn btn-success"><span style="color: #fff;" class="glyphicon glyphicon-pencil"></span> Simpan Perubahan</button>
</div>
</div>
				<?php }?>
				<?php echo form_close(); ?>

		</div>
	</div>
</div>
</div>
