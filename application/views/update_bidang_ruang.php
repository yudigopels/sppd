<div id="page-wrapper">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <ol class="breadcrumb">
				<li>
					<a href="#"><i class="fa fa-dashboard"></i> Dashboard</a>
				</li>
				<li class="active"><i class="fa fa-wrench fa-fw"></i>Update Data Bidang Ruang</li>
			</ol>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="panel panel-default">
                  <div class="panel-body" align="justify">	
				 <?php echo form_open('bidang_ruang/update'); ?>
<?php foreach($data as $row){  ?>

		<div class="form-group">					
			<label class="col-sm-2 control-label">Bidang Ruang</label>
			<div class="col-sm-10">
<input type="text" name="bd" id="inputKode" class="form-control" value="<?php echo $row['bidang_ruang']; ?>">		
<input type="hidden" name="id" id="inputKode" class="form-control" value="<?php echo $row['id']; ?>">	
			</div>
		</div>
	
		
<div class="form-group">
<label class="col-sm-2 control-label"></label>
<div class="col-sm-10">


<br><a href="<?php echo site_url('bidang_ruang') ?>" class="btn btn-danger"><span style="color: #fff;" class="glyphicon glyphicon-circle-arrow-left"></span> Kembali</a>
<button type="submit" class="btn btn-success"><span style="color: #fff;" class="glyphicon glyphicon-pencil"></span> Simpan Perubahan</button>
</div>
</div>
				<?php }?>
				<?php echo form_close(); ?>

		</div>
	</div>
</div>
</div>
