<section class="content-header">
    <h1>
        <small>Bukti Penyerahan Berkas</small>
    </h1>

</section>

<div class="col-xs-12">
<div class="panel-body">

<?php echo form_open('sppd/update_berkas'); ?>
<?php foreach($dspt as $d){  ?>


<?php
$tgspt=$d['tgl_spt'];
$pisah = explode('-',$tgspt);

switch($pisah[1]){       
        case 1 : {
                    $bln='Januari';
                }break;
        case 2 : {
                    $bln='Februari';
                }break;
        case 3 : {
                    $bln='Maret';
                }break;
        case 4 : {
                    $bln='April';
                }break;
        case 5 : {
                    $bln='Mei';
                }break;
        case 6 : {
                    $bln="Juni";
                }break;
        case 7 : {
                    $bln='Juli';
                }break;
        case 8 : {
                    $bln='Agustus';
                }break;
        case 9 : {
                    $bln='September';
                }break;
        case 10 : {
                    $bln='Oktober';
                }break;     
        case 11 : {
                    $bln='November';
                }break;
        case 12 : {
                    $bln='Desember';
                }break;
        default: {
                    $bln='UnKnown';
                }break;
    }


$array = array($pisah[2],$bln,$pisah[0]);
$tglspt = implode(' ',$array);

$tgb=$d['tgl_berangkat'];
$pisah1 = explode('-',$tgb);

switch($pisah1[1]){       
        case 1 : {
                    $bln='Januari';
                }break;
        case 2 : {
                    $bln='Februari';
                }break;
        case 3 : {
                    $bln='Maret';
                }break;
        case 4 : {
                    $bln='April';
                }break;
        case 5 : {
                    $bln='Mei';
                }break;
        case 6 : {
                    $bln="Juni";
                }break;
        case 7 : {
                    $bln='Juli';
                }break;
        case 8 : {
                    $bln='Agustus';
                }break;
        case 9 : {
                    $bln='September';
                }break;
        case 10 : {
                    $bln='Oktober';
                }break;     
        case 11 : {
                    $bln='November';
                }break;
        case 12 : {
                    $bln='Desember';
                }break;
        default: {
                    $bln='UnKnown';
                }break;
    }


$arrayb = array($pisah1[2],$bln,$pisah1[0]);
$brgkt = implode(' ',$arrayb);

$tgk=$d['tgl_kembali'];
$pisah2 = explode('-',$tgk);

switch($pisah2[1]){       
        case 1 : {
                    $bln='Januari';
                }break;
        case 2 : {
                    $bln='Februari';
                }break;
        case 3 : {
                    $bln='Maret';
                }break;
        case 4 : {
                    $bln='April';
                }break;
        case 5 : {
                    $bln='Mei';
                }break;
        case 6 : {
                    $bln="Juni";
                }break;
        case 7 : {
                    $bln='Juli';
                }break;
        case 8 : {
                    $bln='Agustus';
                }break;
        case 9 : {
                    $bln='September';
                }break;
        case 10 : {
                    $bln='Oktober';
                }break;     
        case 11 : {
                    $bln='November';
                }break;
        case 12 : {
                    $bln='Desember';
                }break;
        default: {
                    $bln='UnKnown';
                }break;
    }


$arrayk = array($pisah2[2],$bln,$pisah2[0]);
$kem = implode(' ',$arrayk);
?>

<div class="form-group row">
	<div class="col-xs-5">

	<label style="font-size: 14px;">No SPT</label>
	<input type="text" name="kode_spd" id="inputKode" class="form-control input-sm" placeholder="Kode SPD....." required="required" value="<?php echo $d['no_spt'] ?>" readonly="yes">
	<input type="hidden" name="idspt" id="idspt" class="form-control" value="<?php echo $d['id']; ?>">
	<input type="hidden" name="idsppd" id="idsppd" class="form-control" value="<?php echo $idsppd; ?>">
	</div>

<div class="col-xs-3">
<label style="font-size: 14px;">Tgl SPT </label>
<input type="text" name="tgspt" id="tgspt" class="form-control" readonly="yes" value="<?php echo $tglspt ?>">
</div>
</div>




<div class="form-group row">

<div class="col-xs-3">
<label style="font-size: 14px;">Tujuan </label>
<input type="text" name="tuju" id="tuju" class="autospt form-control" readonly="yes" value="<?php echo $d['tujuan'] ?>">
</div>

<div class="col-xs-5">
<label style="font-size: 14px;">Waktu Pelaksanaan </label>
<input type="text" name="wkt" id="wkt" class="form-control" readonly="yes" value="<?php echo $brgkt.' s/d '.$kem ?>">
</div>

</div>

<div class="form-group row">
	<div class="col-xs-3">
	<label style="font-size: 14px;">Nama Pegawai </label>
	<input type="text" name="nmpeg" id="nmpeg" class="autospt form-control" readonly="yes" value="<?php echo $d['nama'] ?>">
	</div>

	<div class="col-xs-3">
	<label style="font-size: 14px;">NIP Pegawai </label>
	<input type="text" name="nipp" id="nipp" class="autospt form-control" readonly="yes" value="<?php echo $d['nip'] ?>">
	</div>

	<div class="col-xs-3">
	<label style="font-size: 14px;">Golongan/Ruang </label>
	<input type="text" name="gol" id="gol" class="autospt form-control" readonly="yes" value="<?php echo $d['pgkt'] ?>">
	</div>

	<div class="col-xs-3">
	<label style="font-size: 14px;">Jabatan </label>
	<input type="text" name="idjab" id="idjab" class="autospt form-control" readonly="yes" value="<?php echo $d['jabat'] ?>">
	</div>


</div>

<div class="form-group row">
	<div class="col-xs-8">
	<label style="font-size: 14px;">Kegiatan </label>
	<input type="text" name="tgkt" id="tgkt" class="autospt form-control" readonly="yes" value="<?php echo $d['kegiatan'] ?>">
	</div>
</div>

<?php
foreach($data as $ba){
$bks=$ba['idberkas'];
$arr=explode (",",$bks);
$jumbkt=count($arr);
$arc=0;
}

?>

<div class="form-group row">
<div class="col-xs-10">

	<table class="table">
					    <thead>
						    <tr>
						        <th>No</th>
						        <th>Berkas</th>
						        <th colspan="2">Status</th>
						    </tr>
					    </thead>
	<tbody>
	<?php $no = 1; ?>
	<?php foreach($berkas as $b){ ?> 
	<tr>
	<td><?php echo $no++; ?></td>
	<td><?php echo $b['berkas'] ?></td>
	<td colspan="2">
	<?php for ($i = 0; $i < $jumbkt; $i++) {
	if (($arr[$i])==($b['id'])){?>
	<input type="checkbox" checked="checked" name="checkon[]" value="<?=$b['id']; ?>"/>Ada/Tidak Ada
	<?php $arc=1; } ?>
	<?php } ?>

	<?php if($arc!=1){ ?>
	<input type="checkbox" name="checkon[]" value="<?=$b['id']; ?>"/>Ada/Tidak Ada
	<?php } ?>

	</td>
	</tr>
	<?php $arc=0; } ?>
	</tbody>
	</table>



</div>
</div>

<div class="form-group row">

	<div class="col-xs-4">
	<!--<button type="button" class="btn btn-default" data-dismiss="modal"><span style="color: #fff;" class="glyphicon glyphicon-remove-circle"></span> Tutup</button>-->
<a class="btn btn-info" href="<?php echo site_url('sppd/v_sppd') ?>" title=""><i class="ion ion-android-arrow-back"></i> Kembali</a>
	<button type="submit" class="btn btn-primary"><span style="color: #fff;" class="glyphicon glyphicon-floppy-saved"></span>Simpan</button>
	</div>
</div>

<?php }?>
<?php echo form_close(); ?>

</div><!--smallbox-->
</div>


