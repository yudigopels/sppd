<div id="page-wrapper">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <ol class="breadcrumb">
				<li>
					<a href="#"><i class="fa fa-dashboard"></i> Dashboard</a>
				</li>
				<li class="active"><i class="fa fa-wrench fa-fw"></i>Update Data Pegawai</li>
			</ol>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="panel panel-default">
                  <div class="panel-body" align="justify">	
				 <?php echo form_open('pegawai/update'); ?>
<?php foreach($data as $row){  ?>

			<div class="form-group">					
			<label class="col-sm-2 control-label">NIP</label>
			<div class="col-sm-10">
<input type="text" name="nip" id="inputKode" class="form-control" value="<?php echo $row['nip']; ?>">	
<input type="hidden" name="id" id="inputKode" class="form-control" value="<?php echo $row['id']; ?>">			
			</div>
		</div>
			
		<div class="form-group">					
			<label class="col-sm-2 control-label">Nama Pegawai</label>
			<div class="col-sm-10">
<input type="text" name="nama" id="inputKode" class="form-control" value="<?php echo $row['nama']; ?>">			
			</div>
		</div>

		<div class="form-group">					
			<label class="col-sm-2 control-label">Jabatan</label>
			<div class="col-sm-10">
<!--<?php $idjab=$row['idjab'];?>

			<select name="jabatan" id="jabatan" class="form-control">
			<?php foreach($jab as $rowg){?>
			<?php if (($rowg['id'])==($idjab)){ ?>
	<option value="<?=$rowg['id']?>" selected="selected"><?=$rowg['content']?></option>
<?php }else{ ?>
<option value="<?=$rowg['id']?>"><?=$rowg['content']?></option>
<?php }?>
<?php }?>
           		</select>-->

<input type="search" class="autoj form-control input-sm" id="autoj" name="jabatan" value="<?php echo $row['content']; ?>"/>
<input type="hidden" name="idjb" id="idjb" class="autoj form-control" readonly="yes" value="<?php echo $row['jab']; ?>">	
		
			</div>
		</div>
		
<div class="form-group">					
			<label class="col-sm-2 control-label">Golongan/Pangkat</label>
			<div class="col-sm-10">
<!--<?php $idpang=$row['idpang'];?>
			<select name="golongan" id="golongan" class="form-control">
			<?php foreach($gol as $rowp){?>
			<?php if (($rowp['id'])==($idpang)){ ?>
	<option value="<?=$rowp['id']?>" selected="selected"><?=$rowp['singkat']?></option>
<?php }else{ ?>
<option value="<?=$rowp['id']?>"><?=$rowp['singkat']?></option>
<?php }?>
<?php }?>
           		</select>-->

<input type="search" class="autog form-control input-sm" id="autog" name="golo" value="<?php echo $row['singkat']; ?>"/>
<input type="hidden" name="idg" id="idg" class="autog form-control" readonly="yes" value="<?php echo $row['gol']; ?>">		
			</div>
		</div>
			
<!--<div class="form-group">					
			<label class="col-sm-2 control-label">Jabatan1</label>
			<div class="col-sm-10">
	<select name="jabatan1" id="jabatan1" class="form-control">
	<?php foreach($jab as $rowg){
	if (($row['content'])==($rowg['id'])){
	?>
	<option value="<?=$rowg['id']?>" selected="selected"><?=$rowg['content']?></option>
	<?php }else{?>
	<option value="<?=$rowg['id']?>"><?=$rowg['content']?></option>
	<?php }}?>
	</select>		
</div>
</div>
<div class="form-group">					
			<label class="col-sm-2 control-label">Golongan/Pangkat</label>
			<div class="col-sm-10">
	<select name="gol1" id="gol1" class="form-control">
	<?php foreach($gol as $rowg){
	if (($row['singkat'])==($rowg['id'])){
	?>
	<option value="<?=$rowg['id']?>" selected="selected"><?=$rowg['singkat']?></option>
	<?php }else{?>
	<option value="<?=$rowg['id']?>"><?=$rowg['singkat']?></option>
	<?php }}?>
	</select>		
</div>
</div>-->
<!--<div class="form-group">					
			<label class="col-sm-2 control-label">Golongan/Pangkat</label>
			<div class="col-sm-10">              
<select name="gol" id="gol" class="form-control">
<?php $singkat=$row['singkat'];?>
		    <?php foreach($gol as $rowg){?>
		    <?php if (($rowg['singkat'])==($singkat)){?>
       			      <option value="<?=$rowg['singkat']?>" selected="selected">
       			      <?=$rowg['singkat']?>
   			      </option>
	              <?php }else{?>		
			            <option value="<?=$rowg['singkat']?>">
			            <?=$rowg['singkat']?>
			            </option>
	              <?php }?>
                  <?php }?>
         	      </select>	    
              </div>
		</div>-->

<div class="form-group">
<label class="col-sm-2 control-label"></label>
<div class="col-sm-10">


<br><a href="<?php echo site_url('pegawai') ?>" class="btn btn-danger"><span style="color: #fff;" class="glyphicon glyphicon-circle-arrow-left"></span> Kembali</a>
<button type="submit" class="btn btn-success"><span style="color: #fff;" class="glyphicon glyphicon-pencil"></span> Simpan Perubahan</button>
</div>
</div>
				<?php }?>
				<?php echo form_close(); ?>

		</div>
	</div>
</div>
</div>
