<section class="content-header">
    <h1>
        Dashboard
        <small>Kegiatan</small>
    </h1>

</section>

<section class="content-header">

<a class="btn btn-success" data-toggle="modal" href='#modal-id' role="button"><i class="fa fa-plus">Tambah Data</i></a>
                  		<?php $this->load->view('modal/add_giat'); ?>
</section>

<div class="col-lg-12 col-xs-6">
	<!-- small box -->
	<div class="table-responsive">
	<div class="modal-info .modal-header">
	<table class="table">
					    <thead>
						    <tr class="bg-orange-active">
						        <th>No</th>
						        <th>Nama Kegiatan</th>
						        <th>Tahun</th>
							<th>Lokasi</th>
						        <th></th>
						    </tr>
					    </thead>

					    <tbody>
					    <?php $no = $this->uri->segment('3') + 1;//$no = 1; ?>
					    <?php foreach ($data as $d): ?>					    	
					    	<tr>
					    		<td><?php echo $no++; ?></td>
					    		<td><?php echo $d['nama'] ?></td>
					    		<td><?php echo $d['tahun'] ?></td>
							<!--<td><?php echo $d['akun'] ?><td>-->
							<td><?php echo $d['lok'] ?></td>
					    		<td>
<a class="btn btn-primary btn-sm" href="<?php echo site_url('spt/giat').'/'.$d['id'].'/'.$d['lok'] ?>" ><span style="color: #fff;" class="glyphicon glyphicon-list"></span> SPT</a>
<a class="btn btn-success btn-sm" href="<?php echo site_url('giat/get_id/').'/'.$d['id'] ?>" ><span style="color: #fff;" class="glyphicon glyphicon-pencil"></span> Edit</a>
<a class="btn btn-danger btn-sm" href="<?php echo site_url('giat/hapus/').'/'.$d['id'] ?>" ><span style="color: #fff;" class="glyphicon glyphicon-trash"></span> Hapus</a>
<!--<a class="btn btn-info" href="<?php echo base_url().'pegawai/cetak/'.$d['id']; ?>" target="_blank" title=""><i class="fa fa-print"></i> Cetak</a>-->
					    		</td>
					    	</tr>
					    <?php endforeach ?>
					    </tbody>
	</table>
</div>
</div>
<div class="halaman">Halaman : <?php echo $this->pagination->create_links(); ?></div>

