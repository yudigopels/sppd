

<div class="col-lg-10 col-xs-6">
<a class="btn btn-success" data-toggle="modal" href='#modal-id' role="button"><i class="fa fa-plus">Tambah Data Pejabat</i></a>
                  		<?php $this->load->view('modal/add_jabat'); ?>
</div>

<div class="col-lg-10 col-xs-6">

            <!-- small box -->
           <!-- <div class="table-responsive">-->
			 <div class="modal-info .modal-header">
                <table class="table">
					    <thead>
						    <tr class="bg-orange-active">
						        <th>No</th>
							<th>Nama Pejabat</th>
						       	<th>Jabatan</th>					        
						        <th></th>
						    </tr>
					    </thead>
					    <tbody>
					    <?php $no = 1; ?>
					    <?php foreach ($data as $d): ?>					    	
					    	<tr>
					    		<td><?php echo $no++; ?></td>
					    		<td><?php echo $d['namap'] ?></td>
					    		<td><?php echo $d['jabat'] ?></td>
								<td>
					    			<a class="btn btn-primary" href="<?php echo site_url('jabat/get_id/').'/'.$d['id'] ?>" ><span style="color: #fff;" class="glyphicon glyphicon-pencil"></span> Edit</a>
					    			<a class="btn btn-danger" href="<?php echo site_url('jabat/hapus/').'/'.$d['id'] ?>" ><span style="color: #fff;" class="glyphicon glyphicon-trash"></span> Hapus</a>
<!--<a class="btn btn-info" href="<?php echo base_url().'pegawai/cetak/'.$d['id']; ?>" target="_blank" title=""><i class="fa fa-print"></i> Cetak</a>-->
					    		</td>
					    	</tr>
					    <?php endforeach ?>
					    </tbody>
					</table>
            </div>
</div>
