<div class="bd-example">
<div class="modal fade" id="modal-id">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header" style="background-color: #ec6e07;">
				<button type="button" class="close" data-dismiss="modal" style="color: #fff;" aria-hidden="true">&times;</button>
				<h4 class="modal-title" style="color: #fff;">Tambah Data Bidang Ruang </h4>
			</div>
			
<div class="modal-body">
			<?php echo form_open('bidang_ruang/tambah_data'); ?>
<div class="form-group">
	<label class="control-label col-sm-3">Bidang Ruang</label>
	<input type="text" name="bd" id="inputKode" class="form-control" placeholder="Bidang Ruang ....." required="required">
	</div>

</div>

<div class="modal-footer">
	<button type="button" class="btn btn-default" data-dismiss="modal">
	<span style="color: #fff;" class="glyphicon glyphicon-remove-circle"></span> Tutup</button>
<button type="submit" class="btn btn-primary"><span style="color: #fff;" class="glyphicon glyphicon-floppy-saved"></span>Simpan</button>
</div>
	<?php echo form_close(); ?>

	</div><!--row -->
	</div>
	</div>
</div>
