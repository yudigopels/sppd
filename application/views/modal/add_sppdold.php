<div class="bd-example">
<div class="modal fade" id="modal-id">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header" style="background-color: #ec6e07;">
				<button type="button" class="close" data-dismiss="modal" style="color: #fff;" aria-hidden="true">&times;</button>
				<h4 class="modal-title" style="color: #fff;">Tambah Data SPPD </h4>
			</div>
			
<div class="modal-body">
			<?php echo form_open('sppd/tambah'); ?>

<div class="form-group">
	<label for="no_sppd" class="control-label col-sm-3">NO SPPD</label>
	<input type="text" name="no_sppd" id="inputKode" class="form-control input-sm" placeholder="NO SPPD ....." required="required">
			</div>
			
<div class="form-group">
		<label style="font-size: 14px;">NIP</label>
		<select name="nips" id="nips" class="form-control">
		<option value="-">-</option>
		<?php foreach($nips as $np){?>
          			<option value="<?=$np['id']?>"><?=$np['nip']?></option>
		<?php }?>
           			</select>
				</div>
		
<div class="form-group">
	<label class="control-label col-sm-3">Tgl terbit SPPD</label>
    <input type="text" name="tgl_terbit_sppd" id="tanggal" class="form-control" placeholder="Tgl terbit SPPD ....." required="required">
				</div>
<div class="form-group">
	<label class="control-label col-sm-3">Tgl Berangkat</label>
    <input type="text" name="tgl_berangkat" id="inputKode" class="form-control" placeholder="Tgl Berangkat ....." required="required">
				</div>
<div class="form-group">
	<label class="control-label col-sm-3">Tgl Kembali</label>
    <input type="text" name="tgl_kembali" id="inputKode" class="form-control" placeholder="Tgl Kembali ....." required="required">
				</div>
<div class="form-group">
	<label class="control-label col-sm-3">Uang Saku</label>
	<input type="number" name="uang_saku" id="inputKode" onkeypress="return hanyaAngka(event, false)" class="form-control" placeholder="Uang Saku ....." required="required">
	</div>
<div class="form-group">
	<label class="control-label col-sm-3">Tujuan</label>
	<input type="text" name="tujuan" id="inputKode" class="form-control" placeholder="Tujuan ....." required="required">
	</div>
<div class="form-group">
	<label class="control-label col-sm-3">Kendaraan</label>
	<input type="text" name="kendaraan" id="inputKode" class="form-control" placeholder="Kendaraan ....." required="required">
	</div>
<div class="form-group">
	<label class="control-label col-sm-3">Keperluan</label>
	<input type="text" name="keperluan" id="inputKode" class="form-control" placeholder="Keperluan ....." required="required">
	</div>

<script type="text/javascript">
            $(document).ready(function () {
                $('#tanggal').datepicker({
                    format: "dd-mm-yyyy",
                    autoclose:true
                });
            });
        </script>
</div>

<div class="modal-footer">
	<button type="button" class="btn btn-default" data-dismiss="modal">
</label><span style="color: #fff;" class="glyphicon glyphicon-remove-circle"></span> Tutup</button>
<button type="submit" class="btn btn-primary"><span style="color: #fff;" class="glyphicon glyphicon-floppy-saved"></span>Simpan</button>
</div>
	<?php echo form_close(); ?>

	</div><!--row -->
	</div>
	</div>
</div>
