<div class="bd-example">
<div class="modal fade" id="modal-id">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header" style="background-color: #ec6e07;">
				<button type="button" class="close" data-dismiss="modal" style="color: #fff;" aria-hidden="true">&times;</button>
				<h4 class="modal-title" style="color: #fff;">Tambah Data Tingkat Jabatan </h4>
			</div>
			
<div class="modal-body">
			<?php echo form_open('tingkat_jabatan/tambah_data'); ?>
<div class="form-group">
	<label class="control-label col-sm-3">Tingkat</label>

<input type="search" class="autot form-control input-sm" id="autot" name="tkt" placeholder="Tingkat ....."/>
<input type="hidden" name="idtk" id="idtk" class="auto form-control" readonly="yes">
	

</div>

<div class="form-group">
		<label class="control-label col-sm-3">Lokasi</label>
		<!--<select name="lokasi" id="lokasi" class="form-control">
		<?php foreach($lok as $rowg){?>
          			<option value="<?=$rowg['id']?>"><?=$rowg['nama_provinsi']?></option>
		<?php }?>
           	</select>-->

<input type="search" class="autocomplete form-control input-sm" id="autocomplete" name="lokasi" placeholder="Lokasi Tujuan"/>
<input type="hidden" name="idkeg" id="idkeg" class="autocomplete form-control" readonly="yes">

</div>

<div class="form-group">
	<label class="control-label col-sm-3">Uang Makan</label>
	<input type="text" name="um" id="inputKode" class="form-control" placeholder="Uang Makan ....." required="required">

</div>

<!--<div class="form-group">
	<label class="control-label col-sm-3">Uang Transport Lokal</label>
	<input type="text" name="ut" id="inputKode" class="form-control" placeholder="Uang Transport Lokal ....." required="required">
</div>-->

<div class="form-group">
	<label class="control-label col-sm-3">Uang Saku</label>
	<input type="text" name="us" id="inputKode" class="form-control" placeholder="Uang Saku ....." required="required">
</div>

<div class="modal-footer">
	<button type="button" class="btn btn-default" data-dismiss="modal">
	<span style="color: #fff;" class="glyphicon glyphicon-remove-circle"></span> Tutup</button>
<button type="submit" class="btn btn-primary"><span style="color: #fff;" class="glyphicon glyphicon-floppy-saved"></span>Simpan</button>
</div>
	<?php echo form_close(); ?>
</div>

</div><!--row -->
</div>
</div>
</div>
