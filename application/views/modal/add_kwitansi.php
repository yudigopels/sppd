<div class="bd-example">
<div class="modal fade" id="modal-id">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header" style="background-color: #ec6e07;">
				<button type="button" class="close" data-dismiss="modal" style="color: #fff;" aria-hidden="true">&times;</button>
				<h4 class="modal-title" style="color: #fff;">Tambah Data Kwitansi </h4>
			</div>
			
<div class="modal-body">
			<?php echo form_open('kwitansi/tambah'); ?>

<div class="form-group">
	<label for="no_sppd" class="control-label col-sm-3">No Kwitansi</label>
	<input type="number" name="no_kwitansi" id="inputKode" onkeypress="return hanyaAngka(event, false)" class="form-control input-sm" placeholder="NO kwitansi ....." required="required">
			</div>
			
<div class="form-group">
		<label style="font-size: 14px;">No SPPD</label>
		<select name="nosppd" id="nosppd" class="form-control">
		<option value="-">-</option>
		<?php foreach($nosppd as $nos){?>
          			<option value="<?=$nos['id']?>"><?=$nos['no_sppd']?></option>
		<?php }?>
           			</select>
				</div>


<div class="form-group">
	<label class="control-label col-sm-3">Transportasi</label>
	<input type="number" name="transportasi" id="inputKode" onkeypress="return hanyaAngka(event, false)" type="text"class="form-control" placeholder="Transportasi ....." required="required">
	</div>
<div class="form-group">
	<label class="control-label col-sm-3">Konsumsi</label>
	<input type="number" name="konsumsi" id="inputKode" onkeypress="return hanyaAngka(event, false)" class="form-control" placeholder="Konsumsi ....." required="required">
	</div>
<div class="form-group">
	<label class="control-label col-sm-3">Penginapan</label>
	<input type="number" name="penginapan" id="inputKode" onkeypress="return hanyaAngka(event, false)" class="form-control" placeholder="Penginapan ....." required="required">
	</div>
<div class="form-group">
	<label class="control-label col-sm-3">Kendaraan Lain</label>
	<input type="number" name="kendaraan_lain" id="inputKode" onkeypress="return hanyaAngka(event, false)" class="form-control" placeholder="Kendaraan Lain ....." required="required">
	</div>
	
</div>

<div class="modal-footer">
	<button type="button" class="btn btn-default" data-dismiss="modal">
	<span style="color: #fff;" class="glyphicon glyphicon-remove-circle"></span> Tutup</button>
<button type="submit" class="btn btn-primary"><span style="color: #fff;" class="glyphicon glyphicon-floppy-saved"></span>Simpan</button>
</div>
	<?php echo form_close(); ?>

	</div><!--row -->
	</div>
	</div>
</div>
