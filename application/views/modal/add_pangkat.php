<div class="bd-example">
<div class="modal fade" id="modal-id">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header" style="background-color: #ec6e07;">
				<button type="button" class="close" data-dismiss="modal" style="color: #fff;" aria-hidden="true">&times;</button>
				<h4 class="modal-title" style="color: #fff;">Tambah Data Golongan </h4>
			</div>
			
<div class="modal-body">
			<?php echo form_open('pangkat/tambah_data'); ?>
			
<div class="form-group">
	<label class="control-label col-sm-3">Golongan</label>
	<input type="text" name="singkat" id="inputKode" class="form-control" placeholder="Golongan ....." required="required">

<div class="form-group">
	<label class="control-label col-sm-3">Nama</label>
	<input type="text" name="content" id="inputKode" class="form-control" placeholder="Nama ....." required="required">


</div>

<div class="form-group">
		<label class="control-label col-sm-3">Tingkat Biaya</label>
		<select name="tg" id="tg" class="form-control">
		<?php foreach($by as $rowg){?>
          			<option value="<?=$rowg['id']?>"><?=$rowg['tingkat_jbt']?></option>
		<?php }?>
           	</select>
</div>

<div class="modal-footer">
	<button type="button" class="btn btn-default" data-dismiss="modal">
	<span style="color: #fff;" class="glyphicon glyphicon-remove-circle"></span> Tutup</button>
<button type="submit" class="btn btn-primary"><span style="color: #fff;" class="glyphicon glyphicon-floppy-saved"></span>Simpan</button>
</div>
	<?php echo form_close(); ?>

</div><!--row -->
</div>
</div>
</div>
</div>
