<div class="bd-example">
<div class="modal fade" id="modal-id">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header" style="background-color: #ec6e07;">
				<button type="button" class="close" data-dismiss="modal" style="color: #fff;" aria-hidden="true">&times;</button>
				<h4 class="modal-title" style="color: #fff;">Tambah Data Kegiatan </h4>
			</div>
			
<div class="modal-body">
			<?php echo form_open('giat/tambah_data'); ?>

<div class="form-group">
	<label for="nip" class="control-label col-sm-3">Nama Kegiatan</label>
	<input type="text" name="nama" id="inputKode" class="form-control input-sm" placeholder="Nama Kegiatan ....." required="required">
			</div>
<div class="form-group">
	<label class="control-label col-sm-3">Tahun Anggaran</label>
	<input type="number" name="tahun" id="inputKode" onkeypress="return hanyaAngka(event, false)" class="form-control" placeholder="Tahun Anggaran ....." required="required">
	</div>

<div class="form-group">
	<label class="control-label col-sm-3">Akun Anggaran</label>
	<input type="text" name="akun" id="inputKode" class="form-control" placeholder="Akun Anggaran ....." required="required">
	</div>

<div class="form-group">
		<label class="control-label col-sm-3">Lokasi</label>
		<!--<select name="lokasi" id="lokasi" class="form-control">
		<?php foreach($lok as $rowg){?>
          			<option value="<?=$rowg['id']?>"><?=$rowg['nama_provinsi']?></option>
		<?php }?>
           	</select>-->

<input type="search" class="autocomplete form-control input-sm" id="autocomplete" name="lokasi" placeholder="Lokasi Tujuan"/>
<input type="hidden" name="idkeg" id="idkeg" class="autocomplete form-control" readonly="yes">

		</div>

<div class="form-group">
	<label class="control-label col-sm-3">SKPD tujuan</label>
	<input type="text" name="skpd" id="inputKode" class="form-control" placeholder="Satker Tujuan ....." required="required">
	</div>

<div class="modal-footer">
	<button type="button" class="btn btn-default" data-dismiss="modal">
	<span style="color: #fff;" class="glyphicon glyphicon-remove-circle"></span> Tutup</button>
<button type="submit" class="btn btn-primary"><span style="color: #fff;" class="glyphicon glyphicon-floppy-saved"></span>Simpan</button>
</div>
	<?php echo form_close(); ?>

	</div><!--row -->
	</div>
	</div>
</div>
