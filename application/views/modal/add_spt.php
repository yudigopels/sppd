<div class="bd-example">
<div class="modal fade" id="modal-id">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header" style="background-color: #ec6e07;">
				<button type="button" class="close" data-dismiss="modal" style="color: #fff;" aria-hidden="true">&times;</button>
				<h4 class="modal-title" style="color: #fff;">Tambah Data Surat  </h4>
			</div>
			
<div class="modal-body">
			<?php echo form_open('spt/tambah'); ?>

<div class="form-group">
	<label for="no_sppd" class="control-label col-sm-3">NO SPT</label>
	<input type="text" name="no_spt" id="inputKode" class="form-control input-sm" placeholder="NO SPT ....." required="required">
	<input type="hidden" name="idkeg" id="inputKode" class="form-control" value="<?php echo $idkeg; ?>">	
			</div>
			



<div class="form-group">
	<label for="peg" class="control-label col-sm-3">Pegawai</label>
	<!--<?php
	$style_provinsi='class="form-control input-sm" id="provinsi_id"  onChange="tampilKabupaten()"';
	echo form_dropdown('provinsi_id',$provinsi,'',$style_provinsi);
	?>-->

<input type="search" class="auto form-control input-sm" id="auto" name="peg" placeholder="Nama Pegawai"/>
<input type="hidden" name="idp" id="idp" class="auto form-control" readonly="yes">
</div>

<?php foreach($data as $row){  ?>
<div class="form-group">
	<label for="lok_id" class="control-label col-sm-3">Lokasi</label>
	<input type="text" name="lokasi" id="lokasi" class="form-control" readonly="yes" value="<?php echo $row['nmlok'] ?> ">
	
</div>

<?php }?>


	
<!--<div class="form-group">
	<label class="control-label col-sm-3">Tgl terbit SPPD</label>
    <input type="text" name="tglsppd" id="inputKode" class="form-control" placeholder="Tgl terbit SPPD ....." required="required">
				</div>
<div class="form-group">
	<label class="control-label col-sm-3">Tgl Berangkat</label>
    <input type="text" name="tglbrgkat" id="inputKode" class="form-control" placeholder="Tgl Berangkat ....." required="required">
				</div>
<div class="form-group">
	<label class="control-label col-sm-3">Tgl Kembali</label>
    <input type="text" name="tglkmbli" id="inputKode" class="form-control" placeholder="Tgl Kembali ....." required="required">
				</div>-->
				<label class="control-label col-sm-3">Tgl terbit SPPD</label>
	<br></br>
	<div class="form-group">
					<div class="input-group date">
					<div class="input-group-addon">
					<i class="fa fa-calendar"></i>
					</div>
	<input type="text" class="form-control pull-right" id="datepicker1">
	</div>
</div>
<label class="control-label col-sm-3">Tgl Berangkat</label>
	<br></br>
	<div class="form-group">
					<div class="input-group date">
					<div class="input-group-addon">
					<i class="fa fa-calendar"></i>
					</div>
	<input type="text" class="form-control pull-right" id="datepicker2">
	</div>
</div>
<label class="control-label col-sm-3">Tgl Kembali</label>
	<br></br>
	<div class="form-group">
					<div class="input-group date">
					<div class="input-group-addon">
					<i class="fa fa-calendar"></i>
					</div>
	<input type="text" class="form-control pull-right" id="datepicker3">
	</div>
</div>
<div class="form-group">
	<label class="control-label col-sm-3">Angkutan</label>
	<input type="text" name="angkut" id="inputKode" class="form-control" placeholder="Angkutan ....." required="required">
	</div>
<div class="form-group">
	<label class="control-label col-sm-3">Berangkat</label>
	<input type="text" name="brgkt" id="inputKode" class="form-control" placeholder="Berangkat ....." required="required">
	</div>
<div class="form-group">
	<label class="control-label col-sm-3">Tujuan</label>
	<input type="text" name="tujuan" id="inputKode" class="form-control" placeholder="Tujuan ....." required="required">
	</div>
<div class="form-group">
	<label class="control-label col-sm-3">Pengikut (orang)</label>
	<input type="text" name="ikut" id="inputKode" class="form-control" placeholder="Pengikut ....." required="required">
	</div>
<div class="form-group">
	<label class="control-label col-sm-3">Keterangan Lain</label>
	<input type="text" name="ket" id="inputKode" class="form-control" placeholder="Keperluan ....." required="required">
	</div>

</div>

<div class="modal-footer">
	<button type="button" class="btn btn-default" data-dismiss="modal">
</label><span style="color: #fff;" class="glyphicon glyphicon-remove-circle"></span> Tutup</button>
<button type="submit" class="btn btn-primary"><span style="color: #fff;" class="glyphicon glyphicon-floppy-saved"></span>Simpan</button>
</div>
	<?php echo form_close(); ?>

	</div><!--row -->
	</div>
	</div>
</div>
