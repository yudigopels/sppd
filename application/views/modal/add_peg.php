<div class="bd-example">
<div class="modal fade" id="modal-id">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header" style="background-color: #ec6e07;">
				<button type="button" class="close" data-dismiss="modal" style="color: #fff;" aria-hidden="true">&times;</button>
				<h4 class="modal-title" style="color: #fff;">Tambah Data Pegawai </h4>
			</div>
			
<div class="modal-body">
			<?php echo form_open('pegawai/tambah_data'); ?>

<div class="form-group">
	<label for="nip" class="control-label col-sm-3">NIP</label>
	<input type="number" name="nip" id="inputKode" onkeypress="return hanyaAngka(event, false)" class="form-control input-sm" placeholder="NIP ....." required="required">
			</div>
<div class="form-group">
	<label class="control-label col-sm-3">Nama Pegawai</label>
	<input type="text" name="nama" id="inputKode" class="form-control" placeholder="Nama Pegawai ....." required="required">
	</div>

<div class="form-group">
		<label class="control-label col-sm-3">Jabatan</label>
<!--		<select name="jabadd" id="jabadd" class="form-control">
		<?php foreach($jab as $rowg){?>
          			<option value="<?=$rowg['id']?>"><?=$rowg['content']?></option>
		<?php }?>
           	</select>-->
<input type="search" class="autoj form-control input-sm" id="autoj" name="jabatan" placeholder="Jabatan"/>
<input type="hidden" name="idjb" id="idjb" class="autoj form-control" readonly="yes">
		</div>

<div class="form-group">
		<label class="control-label col-sm-3">Gol/Pangkat</label>
		<!--<select name="goladd" id="goladd" class="form-control">
		<?php foreach($gol as $rowg){?>
          			<option value="<?=$rowg['id']?>"><?=$rowg['singkat']?></option>
		<?php }?>
           	</select>-->
<input type="search" class="autog form-control input-sm" id="autog" name="golo" placeholder="Golongan/Pangkat"/>
<input type="hidden" name="idg" id="idg" class="autog form-control" readonly="yes">	
		</div>

</div>

<div class="modal-footer">
	<button type="button" class="btn btn-default" data-dismiss="modal">
	<span style="color: #fff;" class="glyphicon glyphicon-remove-circle"></span> Tutup</button>
<button type="submit" class="btn btn-primary"><span style="color: #fff;" class="glyphicon glyphicon-floppy-saved"></span>Simpan</button>
</div>
	<?php echo form_close(); ?>

	</div><!--row -->
	</div>
	</div>
</div>
