<div class="col-md-12">	

	<div class="panel-heading">
	<h2>Update Data SPPD</h2>
	</div>
<div class="panel-body">

<?php echo form_open('sppd/update'); ?>
<?php foreach($data as $row){  ?>

<div class="form-group row">
	<div class="col-xs-4">

	<label style="font-size: 14px;">Kode SPPD</label>
	<input type="text" name="kode_spd" id="inputKode" class="form-control input-sm" placeholder="Kode SPD....." required="required" value="<?php echo $row['kode']?>">
	<input type="hidden" name="id" id="inputKode" class="form-control" value="<?php echo $row['id']; ?>">
	</div>

	<div class="col-xs-4">

	<label style="font-size: 14px;">No SPPD</label>
	<input type="text" name="no_spd" id="inputKode" class="form-control input-sm" placeholder="Nomor SPD....." required="required" value="<?php echo $row['nospd']?>">
	</div>

	<div class="col-xs-3">
	<label style="font-size: 14px;">No SPT </label>
<input type="search" class="autospt form-control input-sm" id="autospt" name="nospt" placeholder="No SPT" value="<?php echo $row['no_spt']?>"/>
<input type="hidden" name="ids" id="ids" class="autospt form-control" readonly="yes" value="<?php echo $row['idspt']?>">	
	</div>


</div>

<?php
$tgspt=$row['tgl_spt'];
$pisah = explode('-',$tgspt);

switch($pisah[1]){       
        case 1 : {
                    $bln='Januari';
                }break;
        case 2 : {
                    $bln='Februari';
                }break;
        case 3 : {
                    $bln='Maret';
                }break;
        case 4 : {
                    $bln='April';
                }break;
        case 5 : {
                    $bln='Mei';
                }break;
        case 6 : {
                    $bln="Juni";
                }break;
        case 7 : {
                    $bln='Juli';
                }break;
        case 8 : {
                    $bln='Agustus';
                }break;
        case 9 : {
                    $bln='September';
                }break;
        case 10 : {
                    $bln='Oktober';
                }break;     
        case 11 : {
                    $bln='November';
                }break;
        case 12 : {
                    $bln='Desember';
                }break;
        default: {
                    $bln='UnKnown';
                }break;
    }


$array = array($pisah[2],$bln,$pisah[0]);
$tglspt = implode(' ',$array);

$tgb=$row['tgl_berangkat'];
$pisah1 = explode('-',$tgb);

switch($pisah1[1]){       
        case 1 : {
                    $bln='Januari';
                }break;
        case 2 : {
                    $bln='Februari';
                }break;
        case 3 : {
                    $bln='Maret';
                }break;
        case 4 : {
                    $bln='April';
                }break;
        case 5 : {
                    $bln='Mei';
                }break;
        case 6 : {
                    $bln="Juni";
                }break;
        case 7 : {
                    $bln='Juli';
                }break;
        case 8 : {
                    $bln='Agustus';
                }break;
        case 9 : {
                    $bln='September';
                }break;
        case 10 : {
                    $bln='Oktober';
                }break;     
        case 11 : {
                    $bln='November';
                }break;
        case 12 : {
                    $bln='Desember';
                }break;
        default: {
                    $bln='UnKnown';
                }break;
    }


$arrayb = array($pisah1[2],$bln,$pisah1[0]);
$brgkt = implode(' ',$arrayb);

$tgk=$row['tgl_kembali'];
$pisah2 = explode('-',$tgk);

switch($pisah2[1]){       
        case 1 : {
                    $bln='Januari';
                }break;
        case 2 : {
                    $bln='Februari';
                }break;
        case 3 : {
                    $bln='Maret';
                }break;
        case 4 : {
                    $bln='April';
                }break;
        case 5 : {
                    $bln='Mei';
                }break;
        case 6 : {
                    $bln="Juni";
                }break;
        case 7 : {
                    $bln='Juli';
                }break;
        case 8 : {
                    $bln='Agustus';
                }break;
        case 9 : {
                    $bln='September';
                }break;
        case 10 : {
                    $bln='Oktober';
                }break;     
        case 11 : {
                    $bln='November';
                }break;
        case 12 : {
                    $bln='Desember';
                }break;
        default: {
                    $bln='UnKnown';
                }break;
    }


$arrayk = array($pisah2[2],$bln,$pisah2[0]);
$kem = implode(' ',$arrayk);

?>

<div class="form-group row">

<div class="col-xs-3">
<label style="font-size: 14px;">Tgl SPT </label>
<input type="text" name="tgspt" id="tgspt" class="autospt form-control" readonly="yes" value="<?php echo $tglspt ?>">
</div>

<div class="col-xs-3">
<label style="font-size: 14px;">Tgl Berangkat </label>
<input type="text" name="brgkt" id="brgkt" class="autospt form-control" readonly="yes" value="<?php echo $brgkt?>">
</div>

<div class="col-xs-3">
<label style="font-size: 14px;">Tgl Kembali </label>
<input type="text" name="kmbl" id="kmbl" class="autospt form-control" readonly="yes" value="<?php echo $kem?>">
</div>

</div>

<div class="form-group row">
	<div class="col-xs-3">
	<label style="font-size: 14px;">Nama Pegawai </label>
	<input type="text" name="nmpeg" id="nmpeg" class="autospt form-control" readonly="yes" value="<?php echo $row['nmpeg']?>">
	<!--<input type="search" class="autop form-control input-sm" id="autop" name="peg" placeholder="Nama Pegawai" value="<?php echo $row['nmpeg']?>"/>
	<input type="hidden" name="idp" id="idp" class="autop form-control" readonly="yes" value="<?php echo $row['idpeg']?>">-->
	</div>

	<div class="col-xs-3">
	<label style="font-size: 14px;">NIP Pegawai </label>
	<input type="text" name="nipp" id="nipp" class="autospt form-control" readonly="yes" value="<?php echo $row['nip']?>">
	</div>

	<div class="col-xs-3">
	<label style="font-size: 14px;">Jabatan </label>
	<input type="text" name="idjab" id="idjab" class="autospt form-control" readonly="yes" value="<?php echo $row['jabat']?>">
	</div>

	<div class="col-xs-3">
	<label style="font-size: 14px;">Golongan </label>
	<input type="text" name="gol" id="gol" class="autospt form-control" readonly="yes" value="<?php echo $row['golpang']?>">
	</div>

</div>

<div class="form-group row">
	<div class="col-xs-3">
	<label style="font-size: 14px;">Tingkat Biaya </label>
	<input type="text" name="tgkt" id="tgkt" class="autospt form-control" readonly="yes" value="<?php echo $row['tingkat_jbt']?>">
	</div>
</div>


<div class="form-group row">
<div class="col-xs-5">
<label style="font-size: 14px;">PPKom </label>
<input type="search" class="autotanda form-control input-sm" id="autotanda" name="pegtanda" placeholder="Nama Penandatangan" value="<?php echo $row['namap']?>"/>
<input type="text" name="nip" id="nip" class="autotanda form-control" readonly="yes" value="<?php echo $row['nipj']?>">
<input type="hidden" name="idj" id="idj" class="autotanda form-control" readonly="yes" value="<?php echo $row['idkom']?>">
</div>

<?php foreach($datab as $rowb){  ?>
<div class="col-xs-5">
<label style="font-size: 14px;">Bendahara Pengeluaran </label>
<input type="search" class="autob form-control input-sm" id="autob" name="pegb" placeholder="Nama Bendahara" value="<?php echo $rowb['namapb']?>"/>
<input type="text" name="nipbe" id="nipbe" class="autob form-control" readonly="yes" value="<?php echo $rowb['nipbe']?>">
<input type="hidden" name="idjb" id="idjb" class="autob form-control" readonly="yes" value="<?php echo $rowb['idb']?>">
</div>
<?php }?>

</div>

<div class="form-group row">
<div class="col-xs-6">
	<label style="font-size: 14px;">Keterangan Lain-lain</label>
	<textarea id="editor1" name="ket" rows="6" class="form-control input-sm"><?php echo $row['ket'] ?></textarea>
</div>
</div>

<div class="form-group row">

	<div class="col-xs-4">
	<!--<button type="button" class="btn btn-default" data-dismiss="modal"><span style="color: #fff;" class="glyphicon glyphicon-remove-circle"></span> Tutup</button>-->
<a class="btn btn-info" href="<?php echo site_url('sppd/v_sppd') ?>" title=""><i class="ion ion-android-arrow-back"></i> Kembali</a>
	<button type="submit" class="btn btn-primary"><span style="color: #fff;" class="glyphicon glyphicon-floppy-saved"></span>Simpan</button>
	</div>
</div>

<?php }?>
<?php echo form_close(); ?>

</div><!--smallbox-->
</div>

