<section class="content-header">
    <h1>
        Dashboard
        <small>SPPD</small>
    </h1>

</section>


<div class="col-lg-12 col-xs-6">

<a class="btn btn-primary" href="<?php echo site_url('giat') ?>" ><span style="color: #fff;" class="glyphicon glyphicon-add"></span> Pilih Kegiatan</a>
            <!-- small box -->

<div class="modal-info .modal-header">
                <table class="table">
					    <thead>
						    <tr class="bg-orange-active">
						        <th>No</th>
							<th>No SPPD</th>
						        <th>NIP</th>
							<th>Tgl terbit SPPD</th>
							<th>Tujuan</th>
							
						        						        
						        <th></th>
						    </tr>
					    </thead>
					    <tbody>
					    <?php $no = 1; ?>
					    <?php foreach ($data as $d): ?>					    	
					    	<tr>
					    		<td><?php echo $no++; ?></td>
					    		<td><?php echo $d['no_sppd'] ?></td>
					    		<td><?php echo $d['nip'] ?></td>
							<td><?php echo $d['tgl_terbit_sppd'] ?></td>
							<td><?php echo $d['tujuan'] ?></td>
														    		
					    		<td>
					    			<a class="btn btn-primary" href="<?php echo site_url('sppd/get_id/').'/'.$d['id'] ?>" ><span style="color: #fff;" class="glyphicon glyphicon-pencil"></span> Edit</a>
					    			<a class="btn btn-danger" href="<?php echo site_url('sppd/hapus/').'/'.$d['id'] ?>" ><span style="color: #fff;" class="glyphicon glyphicon-trash"></span> Hapus</a>
<!--<a class="btn btn-info" href="<?php echo base_url().'pegawai/cetak/'.$d['id']; ?>" target="_blank" title=""><i class="fa fa-print"></i> Cetak</a>-->
					    		</td>
					    	</tr>
					    <?php endforeach ?>
					    </tbody>
					</table>
            </div>
</div>
