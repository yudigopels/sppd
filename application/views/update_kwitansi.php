<div id="page-wrapper">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <ol class="breadcrumb">
				<li>
					<a href="#"><i class="fa fa-dashboard"></i> Dashboard</a>
				</li>
				<li class="active"><i class="fa fa-wrench fa-fw"></i>Update Data Kwitansi</li>
			</ol>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="panel panel-default">
                  <div class="panel-body" align="justify">	
				 <?php echo form_open('kwitansi/update'); ?>
<?php foreach($data as $row){  ?>
		
		
		<div class="form-group">					
			<label class="col-sm-2 control-label">No Kwitansi</label>
			<div class="col-sm-10">
<input type="number" name="id" id="inputKode" class="form-control" value="<?php echo $row['no_kwitansi']; ?>">	
			</div>
		</div>
	<div class="form-group">					
			<label class="col-sm-2 control-label">No SPPD</label>
			<div class="col-sm-10">
<?php $idsp=$row['idsp'];?>

			<select name="nosppd_up" id="nosppd_up" class="form-control">
			<?php foreach($nosppd as $rowg){?>
			<?php if (($rowg['id'])==($idsp)){ ?>
	<option value="<?=$rowg['id']?>" selected="selected"><?=$rowg['no_sppd']?></option>
<?php }else{ ?>
<option value="<?=$rowg['id']?>"><?=$rowg['no_sppd']?></option>
<?php }?>
<?php }?>
           		</select>		
			</div>
		</div>
		
		<div class="form-group">					
			<label class="col-sm-2 control-label">Transportasi</label>
			<div class="col-sm-10">
<input type="number" name="transportasi" id="inputKode" class="form-control" value="<?php echo $row['transportasi']; ?>">			
			</div>
		</div>
		<div class="form-group">					
			<label class="col-sm-2 control-label">Konsumsi</label>
			<div class="col-sm-10">
<input type="number" name="konsumsi" id="inputKode" class="form-control" value="<?php echo $row['konsumsi']; ?>">			
			</div>
		</div>
		<div class="form-group">					
			<label class="col-sm-2 control-label">Penginapan</label>
			<div class="col-sm-10">
<input type="number" name="penginapan" id="inputKode" class="form-control" value="<?php echo $row['penginapan']; ?>">			
			</div>
		</div>
		<div class="form-group">					
			<label class="col-sm-2 control-label">Kendaraan lain</label>
			<div class="col-sm-10">
<input type="number" name="kendaraan_lain" id="inputKode" class="form-control" value="<?php echo $row['kendaraan_lain']; ?>">	
<input type="hidden" name="id" id="inputKode" class="form-control" value="<?php echo $row['id']; ?>">		
			</div>
		</div>

		
<div class="form-group">
<label class="col-sm-2 control-label"></label>
<div class="col-sm-10">


<br><a href="<?php echo site_url('kwitansi') ?>" class="btn btn-danger"><span style="color: #fff;" class="glyphicon glyphicon-circle-arrow-left"></span> Kembali</a>
<button type="submit" class="btn btn-success"><span style="color: #fff;" class="glyphicon glyphicon-pencil"></span> Simpan Perubahan</button>
</div>
</div>
				<?php }?>
				<?php echo form_close(); ?>

		</div>
	</div>
</div>
</div>
