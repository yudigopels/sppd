<section class="content-header">
    <h1>
        Dashboard
        <small>Surat Perintah Tugas</small>
    </h1>

</section>


<div class="col-xs-12">


          <!-- Custom Tabs -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#home" data-toggle="tab" aria-expanded="true">View</a></li>
              <li class=""><a href="#add" data-toggle="tab" aria-expanded="false">Tambah</a></li>

             
            </ul>
           
	<div class="tab-content">
		

	<div class="tab-pane active" id="home">
	<!-- small box -->
	<div class="table-responsive">
	<div class="modal-info .modal-header">
	<table class="table">
					    <thead>
						    <tr class="bg-orange-active">
						        <th>No</th>
						        <th>Nama Kegiatan</th>
						        <th>Tahun</th>
						      	<th>Nomor SPT</th>
							<th>Pegawai</th>
						        <th></th>
						    </tr>
					    </thead>

					    <tbody>
					    <?php $no = 1; ?>
					    <?php foreach ($data as $d): ?>					    	
					    	<tr>
					    		<td><?php echo $no++; ?></td>
					    		<td><?php echo $d['nmgiat'] ?></td>
					    		<td><?php echo $d['tahun'] ?></td>
							<td><?php echo $d['no_spt'] ?></td>
							<td><?php echo $d['nmpeg'] ?><td>
					    		<td>
<a class="btn btn-success btn-sm" href="<?php echo site_url('spt/get_id/').'/'.$d['id'].'/'.$idkeg.'/'.$lok ?>" ><span style="color: #fff;" class="glyphicon glyphicon-pencil"></span> Edit</a>
<a class="btn btn-danger btn-sm" href="<?php echo site_url('spt/hapus/').'/'.$d['id'].'/'.$idkeg.'/'.$lok ?>" ><span style="color: #fff;" class="glyphicon glyphicon-trash"></span> Hapus</a>
<a class="btn btn-warning" href="<?php echo site_url('spt/cetak').'/'.$d['id'] ?>" title="<?php echo $d['no_spt'] ?>" target="_blank"><i class="ion ion-printer"></i> Cetak</a>
<!--<a class="btn btn-info btn-sm" href="<?php echo site_url('sppd/v_sppd/').'/'.$d['id'].'/'.$idkeg.'/'.$d['idpeg'] ?>" ><i class="ion ion-card"></i> SPD</a>-->
				    		</td>
					    	</tr>
					    <?php endforeach ?>
					    </tbody>
	</table>
</div>
</div>
</div>

              <div class="tab-pane" id="add">

                <?php $this->load->view('add/add_spt'); ?>
              </div>

</div>
</div>
<a class="btn btn-warning" href="<?php echo site_url('giat') ?>" title=""><i class="ion ion-android-arrow-back"></i> Kembali</a>
</div>


