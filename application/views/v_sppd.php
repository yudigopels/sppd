<section class="content-header">
    <h1>
        Dashboard
        <small>SPPD</small>
    </h1>

</section>


<div class="col-xs-12">


          <!-- Custom Tabs -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#home" data-toggle="tab" aria-expanded="true">View</a></li>
              <li class=""><a href="#add" data-toggle="tab" aria-expanded="false">Tambah</a></li>

             
            </ul>
           
	<div class="tab-content">
		

	<div class="tab-pane active" id="home">
	<!-- small box -->
	<div class="table-responsive">
	<div class="bg-green-active">
	<table class="table">
					    <thead>
						    <tr>
						        <th>No</th>
						        <th>Nama Kegiatan</th>
						        <th>Tahun</th>
						      	<th>Nomor SPT</th>
							<th>Pegawai/NIP/Jabatan</th>
						        <th></th>
						    </tr>
					    </thead>

					    <tbody>
					    <?php $no = $this->uri->segment('3') + 1;//$no = 1; ?>
					    <?php foreach ($data as $d): ?>					    	
					    	<tr class="bg-purple">
					    		<td><?php echo $no++; ?></td>
					    		<td><?php echo $d['nmgiat'] ?></td>
					    		<td><?php echo $d['tahun'] ?></td>
							<td><?php echo $d['no_spt'] ?></td>
							<td><?php echo $d['nmpeg'].'/'.$d['nip'].'/'.$d['jabat'] ?><td>
					    		<td>
<a class="btn btn-success btn-sm" href="<?php echo site_url('sppd/get_id/').'/'.$d['id'] ?>" ><span style="color: #fff;" class="glyphicon glyphicon-pencil"></span> Edit</a>
<a class="btn btn-danger btn-sm" href="<?php echo site_url('sppd/hapus/').'/'.$d['id'] ?>" ><span style="color: #fff;" class="glyphicon glyphicon-trash"></span> Hapus</a>
<a class="btn btn-danger btn-sm" href="<?php echo site_url('sppd/v_ikut/').'/'.$d['id'] ?>" ><i class="ion ion-person-add"></i> Pengikut</a>
<div class="btn-group">

<a class="btn btn-info btn-sm" href="#"><i class="icon-ok-circle"></i> Sub Menu</a>
<a class="btn btn-success dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret"></span></a>
	<ul class="dropdown-menu">
<li><a href="<?php echo site_url('sppd/v_berkas').'/'.$d['id'].'/'.$d['idspt'] ?>"><i class="ion ion-android-calendar" aria-hidden="true"></i> Penyerahan Berkas</a></li>
<!--<li><a href="<?php echo site_url('sppd/v_berkas').'/'.$d['id'].'/'.$d['idspt'] ?>"><i class="ion ion-android-calendar" aria-hidden="true"></i> Cetak Bukti Berkas</a></li>
<li><a href="<?php echo site_url('sppd/v_berkas').'/'.$d['id'].'/'.$d['idspt'] ?>"><i class="ion ion-android-calendar" aria-hidden="true"></i> Cetak Pernyataan</a></li>-->
<li><a href="<?php echo site_url('sppd/v_biaya').'/'.$d['id'].'/'.$d['idspt'].'/'.$d['lokasi'].'/'.$d['idtgkt'] ?>"><i class="ion ion-android-calendar" aria-hidden="true"></i> Verifikasi Biaya</a></li>
<li><a href="<?php echo site_url('sppd/v_kwt').'/'.$d['id'].'/'.$d['idspt']?>"><i class="ion ion-android-calendar" aria-hidden="true"></i> Kwitansi</a></li>
	</ul>
</div><!-- /btn-group -->



<a class="btn btn-default btn-sm" href="<?php echo site_url('sppd/cetak').'/'.$d['id'] ?>" title="<?php echo $d['no_spt'] ?>" target="_blank"><i class="ion ion-printer"></i> Cetak</a>
				    		</td>
					    	</tr>
					    <?php endforeach ?>
					    </tbody>
	</table>
</div>
</div>
</div>

              <div class="tab-pane" id="add">

                <?php $this->load->view('add/add_spd'); ?>
              </div>

</div>
</div>
<div class="halaman">Halaman : <?php echo $this->pagination->create_links(); ?></div>
</div>


