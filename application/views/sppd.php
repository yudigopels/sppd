<section class="content-header">
    <h1>
        Dashboard
        <small>SPPD</small>
    </h1>

</section>

<section class="content-header">

<a class="btn btn-success" data-toggle="modal" href='#modal-id' role="button"><i class="fa fa-plus">Tambah Data</i></a>
                  		<?php $this->load->view('modal/add_sppd'); ?>
</section>

<div class="col-lg-12 col-xs-6">
	<!-- small box -->
	<div class="table-responsive">
	<div class="modal-info .modal-header">
	<table class="table">
					    <thead>
						    <tr class="bg-orange-active">
						        <th>No</th>
						        <th>Nama Kegiatan</th>
						        <th>Tahun</th>
						      	<th>Nomor SPPD</th>
							<th>Pegawai</th>
						        <th></th>
						    </tr>
					    </thead>

					    <tbody>
					    <?php $no = 1; ?>
					    <?php foreach ($data as $d): ?>					    	
					    	<tr>
					    		<td><?php echo $no++; ?></td>
					    		<td><?php echo $d['nmgiat'] ?></td>
					    		<td><?php echo $d['tahun'] ?></td>
							<td><?php echo $d['no_sppd'] ?></td>
							<td><?php echo $d['nmpeg'] ?><td>
					    		<td>
<a class="btn btn-success btn-sm" href="<?php echo site_url('sppd/get_id/').'/'.$d['id'] ?>" ><span style="color: #fff;" class="glyphicon glyphicon-pencil"></span> Edit</a>
<a class="btn btn-danger btn-sm" href="<?php echo site_url('sppd/hapus/').'/'.$d['id'] ?>" ><span style="color: #fff;" class="glyphicon glyphicon-trash"></span> Hapus</a>
<a class="btn btn-info btn-sm" href="<?php echo site_url('sppd/hapus/').'/'.$d['id'] ?>" ><span style="color: #fff;" class="glyphicon glyphicon-book"></span> Kwitansi</a>
<!--<a class="btn btn-info" href="<?php echo base_url().'pegawai/cetak/'.$d['id']; ?>" target="_blank" title=""><i class="fa fa-print"></i> Cetak</a>-->
					    		</td>
					    	</tr>
					    <?php endforeach ?>
					    </tbody>
	</table>
</div>
</div>

