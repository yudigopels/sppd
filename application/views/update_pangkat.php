<div id="page-wrapper">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <ol class="breadcrumb">
				<li>
					<a href="#"><i class="fa fa-dashboard"></i> Dashboard</a>
				</li>
				<li class="active"><i class="fa fa-wrench fa-fw"></i>Update Data Golongan</li>
			</ol>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="panel panel-default">
                  <div class="panel-body" align="justify">	
				 <?php echo form_open('pangkat/update'); ?>
<?php foreach($data as $row){  ?>

			<div class="form-group">					
			<label class="col-sm-2 control-label">Golongan</label>
			<div class="col-sm-10">
<input type="text" name="singkat" id="inputKode" class="form-control" value="<?php echo $row['singkat']; ?>">	
<input type="hidden" name="id" id="inputKode" class="form-control" value="<?php echo $row['id']; ?>">		
			</div>
		</div>
		
		<div class="form-group">					
			<label class="col-sm-2 control-label">Nama</label>
			<div class="col-sm-10">
<input type="text" name="content" id="inputKode" class="form-control" value="<?php echo $row['content']; ?>">			
			</div>
		</div>
		
<div class="form-group">					
			<label class="col-sm-2 control-label">Tingkat Biaya</label>
			<div class="col-sm-10">
			<select name="by" id="by" class="form-control">
			<?php 
			$idb=$row['idbiaya'];
			foreach($by as $rowa){?>
			<?php if(($rowa['id'])==($idb)){?>
			<option value="<?=$rowa['id']?>" selected="selected"><?=$rowa['tingkat_jbt']?></option>
			<?php }else{ ?>
			<option value="<?=$rowa['id']?>"><?=$rowa['tingkat_jbt']?></option>
			<?php }?>
			<?php }?>

           		</select>

		
			</div>
		</div>

		
			
		
<div class="form-group">
<div class="col-sm-10">
<a href="<?php echo site_url('pangkat') ?>" class="btn btn-danger"><span style="color: #fff;" class="glyphicon glyphicon-circle-arrow-left"></span> Kembali</a>
<button type="submit" class="btn btn-success"><span style="color: #fff;" class="glyphicon glyphicon-pencil"></span> Simpan Perubahan</button>
</div>
</div>
				<?php }?>
				<?php echo form_close(); ?>

		</div>
	</div>
</div>
</div>
