<div class="col-md-12">	

	<div class="panel-heading">
	<h2>Update Data SPT</h2>
	</div>
<div class="panel-body">

<?php echo form_open('spt/update'); ?>
<?php foreach($data as $row){  ?>

<div class="form-group row">
	<div class="col-xs-5">

	<label for="no_sppd" class="control-label col-sm-3">NO SPT</label>
	<input type="text" name="no_spt" id="inputKode" class="form-control input-sm" value="<?php echo $row['no_spt']; ?>" required="required">
	<input type="hidden" name="idkeg" id="inputKode" class="form-control" value="<?php echo $idkeg; ?>">
	<input type="hidden" name="id" id="inputKode" class="form-control" value="<?php echo $row['id']; ?>">
	</div>

	<div class="col-xs-5">
	<label style="font-size: 14px;">Nama Pegawai </label>
	<input type="search" class="auto form-control input-sm" id="auto" name="peg" value="<?php echo $row['nmpeg']; ?>"/>
	<input type="hidden" name="idp" id="idp" class="auto form-control" readonly="yes" value="<?php echo $row['idpeg']; ?>">
	</div>
</div>


<div class="form-group row">

<div class="col-xs-2">
	<label style="font-size: 14px;">Tgl SPT</label>
	<div class="input-group date">
		<div class="input-group-addon">
		<i class="fa fa-calendar"></i>
		</div>
	<input type="text" name="datepicker" class="form-control pull-right" id="datepicker" value="<?php echo $row['tgl_spt']; ?>">
	</div>
</div>

	<div class="col-xs-2">
	<label style="font-size: 14px;">Lokasi Tujuan</label>
	<input type="text" name="lokasi" id="lokasi" class="form-control" readonly="yes" value="<?php echo $lok?> ">
	
	</div>

<div class="col-xs-2">
	<label style="font-size: 14px;">Tgl Berangkat</label>
	<div class="input-group date">
		<div class="input-group-addon">
		<i class="fa fa-calendar"></i>
		</div>
	<input type="text" name="datepicker1" class="form-control pull-right" id="datepicker1" value="<?php echo $row['tgl_berangkat']; ?>">
	</div>
</div>

<div class="col-xs-2">
	<label style="font-size: 14px;">Tgl Kembali</label>
	<div class="input-group date">
		<div class="input-group-addon">
		<i class="fa fa-calendar"></i>
		</div>
	<input type="text" name="datepicker2" class="form-control pull-right" id="datepicker2" value="<?php echo $row['tgl_kembali']; ?>">
	</div>
</div>

<div class="col-xs-2">
<label class="control-label col-sm-3">Angkutan</label>
<input type="text" name="angkut" id="inputKode" class="form-control" placeholder="Angkutan ....." required="required" value="<?php echo $row['angkutan']; ?>">
</div>



</div>

<div class="form-group row">

<div class="col-xs-3">
<label class="control-label col-sm-3">Tempat Berangkat</label>
<input type="text" name="brgkt" id="inputKode" class="form-control" placeholder="Tempat Berangkat ....." required="required" value="<?php echo $row['tempat_brgkt']; ?>">
</div>

<div class="col-xs-3">
<label class="control-label col-sm-3">Tempat Tujuan</label>
<input type="text" name="tuju" id="inputKode" class="form-control" placeholder="Tempat Tujuan ....." required="required" value="<?php echo $row['tujuan']; ?>">
</div>

<div class="col-xs-5">
<label style="font-size: 14px;">Nama Penandatangan </label>
<input type="search" class="autotanda form-control input-sm" id="autotanda" name="pegtanda" value="<?php echo $row['namap']; ?>"/>
<input type="text" name="nip" id="nip" class="autotanda form-control" readonly="yes" value="<?php echo $row['nip']; ?>">
<input type="hidden" name="idj" id="idj" class="autotanda form-control" readonly="yes" value="<?php echo $row['idjabat']; ?>">
</div>

</div>

<div class="form-group row">


<div class="col-xs-6">
	<label style="font-size: 14px;">Keterangan</label>
	<textarea id="editor1" name="ket" rows="4" class="form-control input-sm"><?php echo $row['ket']; ?></textarea>
</div>

</div>

<div class="form-group row">

	<div class="col-xs-4">
	<!--<button type="button" class="btn btn-default" data-dismiss="modal"><span style="color: #fff;" class="glyphicon glyphicon-remove-circle"></span> Tutup</button>-->
<a class="btn btn-info" href="<?php echo site_url('spt/giat').'/'.$idkeg.'/'.$lok ?>" title=""><i class="ion ion-android-arrow-back"></i> Kembali</a>
	<button type="submit" class="btn btn-primary"><span style="color: #fff;" class="glyphicon glyphicon-floppy-saved"></span>Simpan</button>
	</div>
</div>
				<?php }?>
				<?php echo form_close(); ?>

</div>
</div>
