<?php
$this->load->view('template/head');
?>

<!--tambahkan custom css disini-->
<!-- iCheck -->
<link href="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/iCheck/flat/blue.css') ?>" rel="stylesheet" type="text/css" />
<!-- Morris chart -->
<link href="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/morris/morris.css') ?>" rel="stylesheet" type="text/css" />
<!-- jvectormap -->
<link href="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/jvectormap/jquery-jvectormap-1.2.2.css') ?>" rel="stylesheet" type="text/css" />
<!-- Date Picker -->
<link href="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/datepicker/datepicker3.css') ?>" rel="stylesheet" type="text/css" />
<!-- Daterange picker -->
<link href="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/daterangepicker/daterangepicker-bs3.css') ?>" rel="stylesheet" type="text/css" />
<!-- bootstrap wysihtml5 - text editor -->
<link href="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') ?>" rel="stylesheet" type="text/css" />

<?php
$this->load->view('template/topbar');
$this->load->view('template/sidebar');
?>

<!-- Content Header (Page header) -->
<section class="content-header">
    
</section>

<!-- Main content -->
<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <?php echo $main_view; ?>   

       

    </div><!-- /.row -->
    <!-- Main row -->
   

</section><!-- /.content -->


<?php
$this->load->view('template/js');
?>

<!--tambahkan custom js disini-->
<!-- jQuery UI 1.11.2 
<script src="<?php echo base_url('assets/js/jquery-ui.min.js') ?>" type="text/javascript"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip 
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>-->
<script src="<?php echo base_url('assets/js/jquery-ui.min.js') ?>" type="text/javascript"></script>
<!-- Morris.js charts -->
<script src="<?php echo base_url('assets/js/raphael-min.js') ?>"></script>
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/morris/morris.min.js') ?>" type="text/javascript"></script>
<!-- Sparkline -->
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/sparkline/jquery.sparkline.min.js') ?>" type="text/javascript"></script>
<!-- jvectormap -->
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') ?>" type="text/javascript"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/knob/jquery.knob.js') ?>" type="text/javascript"></script>
<!-- daterangepicker -->
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/daterangepicker/daterangepicker.js') ?>" type="text/javascript"></script>
<!-- datepicker -->
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/datepicker/bootstrap-datepicker.js') ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/ckeditor/ckeditor.js') ?>"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') ?>" type="text/javascript"></script>
<!-- iCheck -->
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/iCheck/icheck.min.js') ?>" type="text/javascript"></script>

<script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/ckeditor/ckeditor.js') ?>"></script>

<!-- AdminLTE dashboard demo (This is only for demo purposes) 
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/dist/js/pages/dashboard.js') ?>" type="text/javascript"></script>

<!-- AdminLTE for demo purposes 
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/dist/js/demo.js') ?>" type="text/javascript"></script>-->

<script src="<?php echo base_url('assetsauto/js/jquery.autocomplete.js');?>" type="text/javascript" ></script>

<script>
function tampilKabupaten()
 {
	 kdprop = document.getElementById("provinsi_id").value;
	 $.ajax({
		 url:"<?php echo base_url();?>index.php/sppd/pilih_kabupaten/"+kdprop+"",
		 success: function(response){
		 $("#kabupaten_id").html(response);
		 },
		 dataType:"html"
	 });
	 return false;
 }
 
 

</script>

<script>
function tampilLok()
 {
	 kdlok = document.getElementById("lok_id").value;
	 $.ajax({
		 url:"<?php echo base_url();?>index.php/sppd/pilih_lok/"+kdlok+"",
		 success: function(response){
		 $("#ut_id").html(response);
		 },
		 dataType:"html"
	 });
	 return false;

	//$.ajax({
	//	 url:"<?php echo base_url();?>index.php/sppd/pilih_um/"+kdlok+"",
	//	 success: function(response){
	//	 $("#us_id").html(response);
	//	 },
	//	 dataType:"html"
	 //});
	 //return false;

 }
 
 

</script>

<script type='text/javascript'>
        var site = "<?php echo site_url('giat/search');?>";
	//var site2 = "<?php echo site_url('ibu/carikel');?>";

        $(function(){
            $('.autocomplete').autocomplete({
                // serviceUrl berisi URL ke controller/fungsi yang menangani request kita
                serviceUrl: site,//+'/autocomplete/search',
                // fungsi ini akan dijalankan ketika user memilih salah satu hasil request
                onSelect: function (suggestion) {
                    $('#idkeg').val(''+suggestion.idk); // membuat id 'v_nim' untuk ditampilkan
                    //$('#pesa').val(''+suggestion.pes); // membuat id 'v_jurusan' untuk ditampilkan
		//$('#pesa').val(''+message.pesan);
                }
            });
        });

</script>

</script>

   <script type='text/javascript'>
        var site2 = "<?php echo site_url('spt/cari_peg');?>";
	//var site2 = "<?php echo site_url('ibu/carikel');?>";

        $(function(){
            $('.auto').autocomplete({
                // serviceUrl berisi URL ke controller/fungsi yang menangani request kita
                serviceUrl: site2,//+'/autocomplete/search',
                // fungsi ini akan dijalankan ketika user memilih salah satu hasil request
                onSelect: function (suggestion) {
                    $('#idp').val(''+suggestion.idp); // membuat id 'v_nim' untuk ditampilkan
                    //$('#pesa').val(''+suggestion.pes); // membuat id 'v_jurusan' untuk ditampilkan
		//$('#pesa').val(''+message.pesan);
                }
            });
        });

</script>


<script type='text/javascript'>
        var site3 = "<?php echo site_url('spt/cari_tanda');?>";
	//var site2 = "<?php echo site_url('ibu/carikel');?>";

        $(function(){
            $('.autotanda').autocomplete({
                // serviceUrl berisi URL ke controller/fungsi yang menangani request kita
                serviceUrl: site3,//+'/autocomplete/search',
                // fungsi ini akan dijalankan ketika user memilih salah satu hasil request
                onSelect: function (suggestion) {
                    $('#nip').val(''+suggestion.nip); // membuat id 'v_nim' untuk ditampilkan
                    $('#idj').val(''+suggestion.idj); // membuat id 'v_jurusan' untuk ditampilkan
		//$('#pesa').val(''+message.pesan);
                }
            });
        });

</script>

<script type='text/javascript'>
        var site9 = "<?php echo site_url('sppd/cari_tanda1');?>";
	//var site2 = "<?php echo site_url('ibu/carikel');?>";

        $(function(){

            $('.autob').autocomplete({
                // serviceUrl berisi URL ke controller/fungsi yang menangani request kita
                serviceUrl: site9,//+'/autocomplete/search',
                // fungsi ini akan dijalankan ketika user memilih salah satu hasil request
                onSelect: function (suggestion) {
                    $('#nipbe').val(''+suggestion.nipbe); // membuat id 'v_nim' untuk ditampilkan
                    $('#idjb').val(''+suggestion.idjb); // membuat id 'v_jurusan' untuk ditampilkan
		//$('#pesa').val(''+message.pesan);
                }
            });
        });

</script>


<script type='text/javascript'>
        var site4 = "<?php echo site_url('sppd/cari_spt');?>";

        $(function(){
            $('.autospt').autocomplete({
                // serviceUrl berisi URL ke controller/fungsi yang menangani request kita
                serviceUrl: site4,//+'/autocomplete/search',
                // fungsi ini akan dijalankan ketika user memilih salah satu hasil request
                onSelect: function (suggestion) {
                    $('#ids').val(''+suggestion.ids); // membuat id 'v_nim' untuk ditampilkan
                    $('#tgspt').val(''+suggestion.tgspt); // membuat id 'v_nim' untuk ditampilkan
                    $('#brgkt').val(''+suggestion.brgkt); // membuat id 'v_nim' untuk ditampilkan
                    $('#kmbl').val(''+suggestion.kembali); // membuat id 'v_nim' untuk ditampilkan
                    $('#nmpeg').val(''+suggestion.peg); // membuat id 'v_nim' untuk ditampilkan
                    $('#nipp').val(''+suggestion.nip); // membuat id 'v_nim' untuk ditampilkan
                    $('#idjab').val(''+suggestion.jaba); // membuat id 'v_nim' untuk ditampilkan
                    $('#gol').val(''+suggestion.gol); // membuat id 'v_nim' untuk ditampilkan
                    $('#tgkt').val(''+suggestion.tgkt); // membuat id 'v_nim' untuk ditampilkan
                    
                }
            });
        });

</script>

</script>

   <script type='text/javascript'>
        var site5 = "<?php echo site_url('sppd/cari_pegs');?>";
	//var site2 = "<?php echo site_url('ibu/carikel');?>";

        $(function(){
            $('.autop').autocomplete({
                // serviceUrl berisi URL ke controller/fungsi yang menangani request kita
                serviceUrl: site5,//+'/autocomplete/search',
                // fungsi ini akan dijalankan ketika user memilih salah satu hasil request
                onSelect: function (suggestion) {
                    $('#idp').val(''+suggestion.idp); // membuat id 'v_nim' untuk ditampilkan
                    $('#nipp').val(''+suggestion.nip); // membuat id 'v_nim' untuk ditampilkan
                    $('#jab').val(''+suggestion.jabat); // membuat id 'v_nim' untuk ditampilkan
                   $('#gol').val(''+suggestion.gol); // membuat id 'v_nim' untuk ditampilkan
                  $('#tgkt').val(''+suggestion.tgkt); // membuat id 'v_nim' untuk ditampilkan
                   // $('#idb').val(''+suggestion.idb); // membuat id 'v_nim' untuk ditampilkan
		//$('#pesa').val(''+message.pesan);
                }
            });
        });

</script>

<script type='text/javascript'>
        var site6 = "<?php echo site_url('tingkat_jabatan/search');?>";
	//var site2 = "<?php echo site_url('ibu/carikel');?>";

        $(function(){
            $('.autot').autocomplete({
                // serviceUrl berisi URL ke controller/fungsi yang menangani request kita
                serviceUrl: site6,//+'/autocomplete/search',
                // fungsi ini akan dijalankan ketika user memilih salah satu hasil request
                onSelect: function (suggestion) {
                    $('#idtk').val(''+suggestion.idt); // membuat id 'v_nim' untuk ditampilkan
                    //$('#pesa').val(''+suggestion.pes); // membuat id 'v_jurusan' untuk ditampilkan
		//$('#pesa').val(''+message.pesan);
                }
            });
        });

</script>

<script type='text/javascript'>
        var site7 = "<?php echo site_url('pegawai/searchjab');?>";
	//var site2 = "<?php echo site_url('ibu/carikel');?>";

        $(function(){
            $('.autoj').autocomplete({
                // serviceUrl berisi URL ke controller/fungsi yang menangani request kita
                serviceUrl: site7,//+'/autocomplete/search',
                // fungsi ini akan dijalankan ketika user memilih salah satu hasil request
                onSelect: function (suggestion) {
                    $('#idjb').val(''+suggestion.idjb); // membuat id 'v_nim' untuk ditampilkan
                    //$('#pesa').val(''+suggestion.pes); // membuat id 'v_jurusan' untuk ditampilkan
		//$('#pesa').val(''+message.pesan);
                }
            });
        });

</script>

<script type='text/javascript'>
        var site8 = "<?php echo site_url('pegawai/searchgol');?>";
	//var site2 = "<?php echo site_url('ibu/carikel');?>";

        $(function(){
            $('.autog').autocomplete({
                // serviceUrl berisi URL ke controller/fungsi yang menangani request kita
                serviceUrl: site8,//+'/autocomplete/search',
                // fungsi ini akan dijalankan ketika user memilih salah satu hasil request
                onSelect: function (suggestion) {
                    $('#idg').val(''+suggestion.idg); // membuat id 'v_nim' untuk ditampilkan
                    //$('#pesa').val(''+suggestion.pes); // membuat id 'v_jurusan' untuk ditampilkan
		//$('#pesa').val(''+message.pesan);
                }
            });
        });

</script>


<script>
 $('#datepicker').datepicker({
 format: "yyyy-mm-dd",
      autoclose: true
    });
</script>

<script>
 $('#datepicker1').datepicker({
 format: "yyyy-mm-dd",
      autoclose: true
    });
</script>

<script>
 $('#datepicker2').datepicker({
 format: "yyyy-mm-dd",
      autoclose: true
    });
</script>

<script>
  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('editor1')
    //bootstrap WYSIHTML5 - text editor
    $('.textarea').wysihtml5()
  })
</script>


<?php
$this->load->view('template/foot');
?>
