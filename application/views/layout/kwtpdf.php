<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>SPPD</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link href="<?php echo base_url('assets/AdminLTE-2.0.5/dist/css/AdminLTE.min.css') ?>" rel="stylesheet" type="text/css" />

</head>
<body>

<?php foreach ($data as $d): ?>	

<div class="col-md 12">
<?php echo $d['nokwt']?>
</div>


<?php endforeach ?>

		<div align="center" class="page-header">
		Bendahara Pengeluaran BPPKAD Kota Probolinggo
		</div>

	
<div class="row">

<div class="col-md 12">

	<table class="table table-bordered" width="100%" border="0">
	<tbody>
<?php foreach ($data as $de): ?>

<?php

$um=$de['um']*$de['lama'];
$tt=$de['us']+$um+$de['ut'];
?>

<?php
$tgb=$de['tgl_berangkat'];
$pisah1 = explode('-',$tgb);

switch($pisah1[1]){       
        case 1 : {
                    $bln='Januari';
                }break;
        case 2 : {
                    $bln='Februari';
                }break;
        case 3 : {
                    $bln='Maret';
                }break;
        case 4 : {
                    $bln='April';
                }break;
        case 5 : {
                    $bln='Mei';
                }break;
        case 6 : {
                    $bln="Juni";
                }break;
        case 7 : {
                    $bln='Juli';
                }break;
        case 8 : {
                    $bln='Agustus';
                }break;
        case 9 : {
                    $bln='September';
                }break;
        case 10 : {
                    $bln='Oktober';
                }break;     
        case 11 : {
                    $bln='November';
                }break;
        case 12 : {
                    $bln='Desember';
                }break;
        default: {
                    $bln='UnKnown';
                }break;
    }


$arrayb = array($pisah1[2],$bln,$pisah1[0]);
$brgkt = implode(' ',$arrayb);

$tgk=$de['tgl_kembali'];
$pisah2 = explode('-',$tgk);

switch($pisah2[1]){       
        case 1 : {
                    $bln='Januari';
                }break;
        case 2 : {
                    $bln='Februari';
                }break;
        case 3 : {
                    $bln='Maret';
                }break;
        case 4 : {
                    $bln='April';
                }break;
        case 5 : {
                    $bln='Mei';
                }break;
        case 6 : {
                    $bln="Juni";
                }break;
        case 7 : {
                    $bln='Juli';
                }break;
        case 8 : {
                    $bln='Agustus';
                }break;
        case 9 : {
                    $bln='September';
                }break;
        case 10 : {
                    $bln='Oktober';
                }break;     
        case 11 : {
                    $bln='November';
                }break;
        case 12 : {
                    $bln='Desember';
                }break;
        default: {
                    $bln='UnKnown';
                }break;
    }


$arrayk = array($pisah2[2],$bln,$pisah2[0]);
$kem = implode(' ',$arrayk);

?>

	<tr>
	<td colspan="11" align="center"><strong>==<?php echo $this->fungsi->terbilang($tt).' Rupiah';?>==</strong></td>
	</tr>

	<tr>
	<td colspan="11" align="justify"><?php echo $de['nmgiat'].' pada tanggal '.$brgkt.' s/d '.$kem;?></td>
	</tr>

	<tr>
	<td width="2%" align="center" valign="top">&nbsp;</td>
	<td colspan="2">Uang Saku</td>
	<td colspan="4"><?php echo $this->fungsi->rupiah($de['us'])?></td>
	<td colspan="4">= <?php echo rupiah($de['us'])?></td>
	</tr>

<?php
$tgb=$de['tgl'];
$pisah1 = explode('-',$tgb);

switch($pisah1[1]){       
        case 1 : {
                    $bln='Januari';
                }break;
        case 2 : {
                    $bln='Februari';
                }break;
        case 3 : {
                    $bln='Maret';
                }break;
        case 4 : {
                    $bln='April';
                }break;
        case 5 : {
                    $bln='Mei';
                }break;
        case 6 : {
                    $bln="Juni";
                }break;
        case 7 : {
                    $bln='Juli';
                }break;
        case 8 : {
                    $bln='Agustus';
                }break;
        case 9 : {
                    $bln='September';
                }break;
        case 10 : {
                    $bln='Oktober';
                }break;     
        case 11 : {
                    $bln='November';
                }break;
        case 12 : {
                    $bln='Desember';
                }break;
        default: {
                    $bln='UnKnown';
                }break;
    }


$arrayb = array($pisah1[2],$bln,$pisah1[0]);
$tgc = implode(' ',$arrayb);
?>
	<tr>
	<td width="2%" align="center" valign="top">&nbsp;</td>
	<td colspan="2">Uang Makan</td>
	<td colspan="4"><?php echo $this->fungsi->rupiah($de['um'])?></td>
	<td colspan="4">= <?php echo rupiah($um)?></td>
	</tr>

	<tr>
	<td width="2%" align="center" valign="top">&nbsp;</td>
	<td colspan="2">Uang Transportasi</td>
	<td colspan="4"><?php echo $this->fungsi->rupiah($de['ut'])?></td>
	<td colspan="4"><u>= <?php echo rupiah($de['ut'])?></u></td>
	</tr>

	<tr>
	<td width="2%" align="center" valign="top">&nbsp;</td>
	<td colspan="6"><strong>Jumlah yang diterima</strong></td>
	<td colspan="4"><strong><?php echo rupiah($tt)?></strong></td>
	</tr>

	<tr>

	<td width="2%" align="center" valign="top">&nbsp;</td>
	<td colspan="6">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<td colspan="4" align="center">Probolinggo, <?php echo $tgc ?></td>
	<tr>

	<td width="2%" align="center" valign="top">&nbsp;</td>
	<td colspan="6">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<td colspan="4" align="center">Yang Menerima</td>
	<tr>

	<td width="2%" align="center" valign="top">&nbsp;</td>
	<td colspan="6">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<td colspan="4">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<tr>

	<td width="2%" align="center" valign="top">&nbsp;</td>
	<td colspan="6">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<td colspan="4">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<tr>

	<tr>
	<td width="2%" align="center" valign="top">&nbsp;</td>
	<td colspan="2" align="center"><strong>==<?php echo rupiah($tt);?>==</strong></td>
	<td colspan="4">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<td colspan="4" align="center"><strong><u><?php echo strtoupper($de['nmpeg']) ?></u></strong></td>
	</tr>

<?php endforeach ?>

	</tbody>
	</table>







</div>
	</div><!-- /.row -->



<!--<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 1.0
    </div>
    <strong>Copyright &copy; 2017 <a href="http://cvmiladiyyah.web.id" target="_blank">Miladiyyah</a>.</strong> All rights reserved.
</footer>-->

    </body>
</html>
