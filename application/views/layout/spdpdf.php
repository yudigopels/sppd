<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>SPPD</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link href="<?php echo base_url('assets/AdminLTE-2.0.5/dist/css/AdminLTE.min.css') ?>" rel="stylesheet" type="text/css" />

</head>
<body>

		<div align="center" class="page-header">
		<strong>BADAN PENGELOLAAN KEUANGAN DAN ANGGARAN DAERAH<br>
		<u>PEMERINTAH KOTA PROBOLINGGO</u></strong>
		</div>
<?php foreach ($data as $d): ?>	

<div class="col-md 12">
	<table class="table table-bordered" width="100%" border="0">
	<tbody>
	<tr>
	<td colspan="3"></td>
	<td colspan="3"></td>
	<td colspan="4" align="right" width="100px">Lembar Ke</td>
	<td width="1%">:</td>
	<td>1</td>
	</tr>

	<tr>
	<td colspan="3"></td>
	<td colspan="3"></td>
	<td colspan="4" align="right" width="100px">Kode</td>
	<td width="1%">:</td>
	<td><?php echo $d['kode']; ?></td>
	</tr>

	<tr>
	<td colspan="3"></td>
	<td colspan="3"></td>
	<td colspan="4" align="right" width="100px">Nomor</td>
	<td width="1%">:</td>
	<td><?php echo $d['nospd']; ?></td>
	</tr>

	</tbody>
	</table>
</div>


<?php endforeach ?>

		<div align="center" class="page-header">
		<u>SURAT PERINTAH PERJALANAN DINAS</u>
		</div>

	
<div class="row">

<div class="col-md 12">

	<table class="table table-bordered" width="100%" border="1">
	<tbody>
<?php foreach ($data as $de): ?>
	<tr>
	<td width="2%" align="center">1</td>
	<td colspan="6">Pejabat Pembuat Komitmen</td>
	<td colspan="4"><?php echo $de['namap']?></td>
	</tr>

	<tr>
	<td width="2%" align="center" valign="top">2</td>
	<td colspan="6" valign="top">Nama/NIP Pegawai yang melaksanakan perjalanan dinas</td>
	<td colspan="4"><?php echo $de['nmpeg'].'/'.$de['nip']?></td>
	</tr>

	<tr>
	<td rowspan="3" width="2%" align="center" valign="top">3</td>
	<td colspan="6">a. Golongan/Ruang</td>
	<td colspan="4"><?php echo $de['golpang']?></td>
	</tr>

	<tr>
	<td colspan="6">b. Jabatan/Instansi</td>
	<td colspan="4"><?php echo $de['jabat']?></td>
	</tr>

	<tr>
	<td colspan="6">c. Tingkat Biaya Perjalanan Dinas</td>
	<td colspan="4"><?php echo $de['tgkt']?></td>
	</tr>

	<tr>
	<td width="2%" align="center" valign="top">4</td>
	<td colspan="6" valign="top">Maksud perjalanan dinas</td>
	<td colspan="4"><?php echo $de['nmgiat']?></td>
	</tr>

	<tr>
	<td width="2%" align="center" valign="top">5</td>
	<td colspan="6" valign="top">Alat angkut yang dipergunakan</td>
	<td colspan="4"><?php echo $de['angkutan']?></td>
	</tr>

	<tr>
	<td rowspan="2" width="2%" align="center" valign="top">6</td>
	<td colspan="6">a. Tempat berangkat</td>
	<td colspan="4"><?php echo $de['tempat_brgkt']?></td>
	</tr>

	<tr>
	<td colspan="6">b. Tempat Tujuan</td>
	<td colspan="4"><?php echo $de['tujuan']?></td>
	</tr>

	<tr>
	<td rowspan="3" width="2%" align="center" valign="top">7</td>
	<td colspan="6">a. Lamanya perjalanan dinas</td>
	<td colspan="4"><?php echo $de['lama'].' Hari' ?></td>
	</tr>
<?php
$tgb=$de['tgl_berangkat'];
$pisah1 = explode('-',$tgb);

switch($pisah1[1]){       
        case 1 : {
                    $bln='Januari';
                }break;
        case 2 : {
                    $bln='Februari';
                }break;
        case 3 : {
                    $bln='Maret';
                }break;
        case 4 : {
                    $bln='April';
                }break;
        case 5 : {
                    $bln='Mei';
                }break;
        case 6 : {
                    $bln="Juni";
                }break;
        case 7 : {
                    $bln='Juli';
                }break;
        case 8 : {
                    $bln='Agustus';
                }break;
        case 9 : {
                    $bln='September';
                }break;
        case 10 : {
                    $bln='Oktober';
                }break;     
        case 11 : {
                    $bln='November';
                }break;
        case 12 : {
                    $bln='Desember';
                }break;
        default: {
                    $bln='UnKnown';
                }break;
    }


$arrayb = array($pisah1[2],$bln,$pisah1[0]);
$brgkt = implode(' ',$arrayb);

$tgk=$de['tgl_kembali'];
$pisah2 = explode('-',$tgk);

switch($pisah2[1]){       
        case 1 : {
                    $bln='Januari';
                }break;
        case 2 : {
                    $bln='Februari';
                }break;
        case 3 : {
                    $bln='Maret';
                }break;
        case 4 : {
                    $bln='April';
                }break;
        case 5 : {
                    $bln='Mei';
                }break;
        case 6 : {
                    $bln="Juni";
                }break;
        case 7 : {
                    $bln='Juli';
                }break;
        case 8 : {
                    $bln='Agustus';
                }break;
        case 9 : {
                    $bln='September';
                }break;
        case 10 : {
                    $bln='Oktober';
                }break;     
        case 11 : {
                    $bln='November';
                }break;
        case 12 : {
                    $bln='Desember';
                }break;
        default: {
                    $bln='UnKnown';
                }break;
    }


$arrayk = array($pisah2[2],$bln,$pisah2[0]);
$kem = implode(' ',$arrayk);

$tgspt=$d['tgl_spt'];
$pisah = explode('-',$tgspt);

switch($pisah[1]){       
        case 1 : {
                    $bln='Januari';
                }break;
        case 2 : {
                    $bln='Februari';
                }break;
        case 3 : {
                    $bln='Maret';
                }break;
        case 4 : {
                    $bln='April';
                }break;
        case 5 : {
                    $bln='Mei';
                }break;
        case 6 : {
                    $bln="Juni";
                }break;
        case 7 : {
                    $bln='Juli';
                }break;
        case 8 : {
                    $bln='Agustus';
                }break;
        case 9 : {
                    $bln='September';
                }break;
        case 10 : {
                    $bln='Oktober';
                }break;     
        case 11 : {
                    $bln='November';
                }break;
        case 12 : {
                    $bln='Desember';
                }break;
        default: {
                    $bln='UnKnown';
                }break;
    }


$array = array($pisah[2],$bln,$pisah[0]);
$tglspt = implode(' ',$array);

?>
	<tr>
	<td colspan="6">b. Tanggal berangkat</td>
	<td colspan="4"><?php echo $brgkt?></td>
	</tr>

	<tr>
	<td colspan="6">c. Tanggal harus kembali/tiba di tempat baru *)</td>
	<td colspan="4"><?php echo $kem?></td>
	</tr>

	<tr>
	<td width="2%" align="center" valign="top">8</td>
	<td colspan="6" valign="top">Pengikut</td>
	<td colspan="2" valign="top">Nama</td>
	<td colspan="2" valign="top">Keterangan</td>
	</tr>

<?php foreach ($cikut as $c): ?>
	<tr>
	<td width="2%" align="center" valign="top"></td>
	<td colspan="6" valign="top"></td>
	<td colspan="2" valign="top"><?php echo $c['namai']; ?></td>
	<td colspan="2" valign="top"><?php echo $c['ket']; ?></td>
	</tr>
<?php endforeach ?>

	<tr>
	<td rowspan="3" width="2%" align="center" valign="top">9</td>
	<td colspan="6" valign="top">Pembebanan Anggaran</td>
	<td colspan="4"></td>
	</tr>

	<tr>
	<td colspan="6" valign="top">a. Instansi</td>
	<td colspan="4">BPKAD Kota Probolinggo</td>
	</tr>

	<tr>
	<td colspan="6" valign="top">b. Akun</td>
	<td colspan="4"><?php echo $de['akun']?></td>
	</tr>


	<tr>
	<td width="2%" align="center" valign="top">10</td>
	<td colspan="6" valign="top">Keterangan lain-lain</td>
	<td colspan="4"><?php echo $de['ket']?></td>
	</tr>

<?php endforeach ?>

	</tbody>
	</table>


<br/>

	<table class="table table-bordered" width="100%" border="0">
	<tbody>
	<tr>

	<td colspan="3" align="center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<td colspan="4" align="center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<td colspan="2" align="center">Dikeluarkan di</td>
	<td align="center">:</td>
	<td colspan="2">Probolinggo</td>
	</tr>

	<tr>
	<td colspan="3" align="center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<td colspan="4" align="center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<td colspan="2" align="center">Pada Tanggal</td>
	<td align="center">:</td>
	<td colspan="2"><?php echo $tglspt?></td>
	</tr>

	</tbody>
	</table>

<br/>

	<table class="table table-bordered" width="100%" border="0">
	<tbody>
	<tr>
	<td colspan="4" align="center">&nbsp;&nbsp;&nbsp;</td>
	<td colspan="5" align="center"><strong><?php echo strtoupper($d['jbt'])?></strong></td>
	</tr>
	<tr>
	<td colspan="4" align="center">&nbsp;&nbsp;&nbsp;</td>
	<td colspan="5" align="center"><strong>BPKAD Kota Probolinggo</strong></td>
	</tr>

	<tr>
	<td colspan="4" align="center">&nbsp;&nbsp;&nbsp;</td>
	<td colspan="5" align="center">&nbsp;&nbsp;&nbsp;</td>
	</tr>

	<tr>
	<td colspan="4" align="center">&nbsp;&nbsp;&nbsp;</td>
	<td colspan="5" align="center">&nbsp;&nbsp;&nbsp;</td>
	</tr>

	<tr>
	<td colspan="4" align="center">&nbsp;&nbsp;&nbsp;</td>
	<td colspan="5" align="center">&nbsp;&nbsp;&nbsp;</td>
	</tr>

	<tr>
	<td colspan="4" align="center">&nbsp;&nbsp;&nbsp;</td>
	<td colspan="5" align="center">&nbsp;&nbsp;&nbsp;</td>
	</tr>



	<tr>
	<td colspan="4" align="center">&nbsp;&nbsp;&nbsp;</td>
	<td colspan="5" align="center"><strong><u><?php echo strtoupper($d['namap']) ?></u></strong></td>
	</tr>

	<tr>
	<td colspan="4" align="center">&nbsp;&nbsp;&nbsp;</td>
	<td colspan="5" align="center"><strong><?php echo 'NIP. '.$d['nipj']?></strong></td>
	</tr>

	</tbody>
	</table>


</div>
	</div><!-- /.row -->



<!--<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 1.0
    </div>
    <strong>Copyright &copy; 2017 <a href="http://cvmiladiyyah.web.id" target="_blank">Miladiyyah</a>.</strong> All rights reserved.
</footer>-->

    </body>
</html>
