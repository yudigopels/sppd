<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cetak SPT</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link href="<?php echo base_url('assets/AdminLTE-2.0.5/dist/css/AdminLTE.min.css') ?>" rel="stylesheet" type="text/css" />

</head>
<body>

		<div align="center" class="page-header">
		<strong>BADAN PENGELOLAAN KEUANGAN DAN ANGGARAN DAERAH<br>
		<u>PEMERINTAH KOTA PROBOLINGGO</u></strong>
		</div>


<?php $no = 1; ?>
<?php foreach ($data as $d): ?>	
	<div class="row">


  
		<div align="center" class="page-header">
		<u>SURAT PERINTAH TUGAS</u><br>
		Nomor <?php echo $d['no_spt']; ?>
		</div>

<div class="col-md 12">
	<table class="table table-bordered" width="100%" border="0">
	<tbody>
	<tr>
	<td colspan="12"><strong>Diperintahkan Kepada</strong></td>
	</tr>
	</tbody>
	</table>

	<table class="table table-bordered" width="100%" border="1">
	<tbody>
	<tr>
	<!--<td width="2%" align="center"><strong>NO</strong></td>-->
	<td colspan="6" align="center"><strong>Nama/NIP</strong></td>
	<td colspan="4" align="center"><strong>Gol/Ruang</strong></td>
	<td colspan="2" align="center"><strong>Jabatan</strong></td>
	</tr>

	<tr>
	<!--<td width="2%" align="center"><strong>NO</strong></td>-->
	<td colspan="6" align="center"><?php echo $d['nmpeg'].'/'.$d['nip']?></td>
	<td colspan="4" align="center"><?php echo $d['content']?></td>
	<td colspan="2" align="center"><?php echo $d['jaba']?></td>
	</tr>

	</tbody>
	</table>


<?php
$tgspt=$d['tgl_spt'];
$pisah = explode('-',$tgspt);

switch($pisah[1]){       
        case 1 : {
                    $bln='Januari';
                }break;
        case 2 : {
                    $bln='Februari';
                }break;
        case 3 : {
                    $bln='Maret';
                }break;
        case 4 : {
                    $bln='April';
                }break;
        case 5 : {
                    $bln='Mei';
                }break;
        case 6 : {
                    $bln="Juni";
                }break;
        case 7 : {
                    $bln='Juli';
                }break;
        case 8 : {
                    $bln='Agustus';
                }break;
        case 9 : {
                    $bln='September';
                }break;
        case 10 : {
                    $bln='Oktober';
                }break;     
        case 11 : {
                    $bln='November';
                }break;
        case 12 : {
                    $bln='Desember';
                }break;
        default: {
                    $bln='UnKnown';
                }break;
    }


$array = array($pisah[2],$bln,$pisah[0]);
$tglspt = implode(' ',$array);

$tgb=$d['tgl_berangkat'];
$pisah1 = explode('-',$tgb);

switch($pisah1[1]){       
        case 1 : {
                    $bln='Januari';
                }break;
        case 2 : {
                    $bln='Februari';
                }break;
        case 3 : {
                    $bln='Maret';
                }break;
        case 4 : {
                    $bln='April';
                }break;
        case 5 : {
                    $bln='Mei';
                }break;
        case 6 : {
                    $bln="Juni";
                }break;
        case 7 : {
                    $bln='Juli';
                }break;
        case 8 : {
                    $bln='Agustus';
                }break;
        case 9 : {
                    $bln='September';
                }break;
        case 10 : {
                    $bln='Oktober';
                }break;     
        case 11 : {
                    $bln='November';
                }break;
        case 12 : {
                    $bln='Desember';
                }break;
        default: {
                    $bln='UnKnown';
                }break;
    }


$arrayb = array($pisah1[2],$bln,$pisah1[0]);
$brgkt = implode(' ',$arrayb);

$tgk=$d['tgl_kembali'];
$pisah2 = explode('-',$tgk);

switch($pisah2[1]){       
        case 1 : {
                    $bln='Januari';
                }break;
        case 2 : {
                    $bln='Februari';
                }break;
        case 3 : {
                    $bln='Maret';
                }break;
        case 4 : {
                    $bln='April';
                }break;
        case 5 : {
                    $bln='Mei';
                }break;
        case 6 : {
                    $bln="Juni";
                }break;
        case 7 : {
                    $bln='Juli';
                }break;
        case 8 : {
                    $bln='Agustus';
                }break;
        case 9 : {
                    $bln='September';
                }break;
        case 10 : {
                    $bln='Oktober';
                }break;     
        case 11 : {
                    $bln='November';
                }break;
        case 12 : {
                    $bln='Desember';
                }break;
        default: {
                    $bln='UnKnown';
                }break;
    }


$arrayk = array($pisah2[2],$bln,$pisah2[0]);
$kem = implode(' ',$arrayk);
?>

	<table class="table table-bordered" width="100%" border="0">
	<tbody>

	<tr>
	<td colspan="2">Untuk Pergi Ke</td>
	<td width="2%" align="center">:</td>
	<td colspan="3"><?php echo $d['nmlok']?></td>
	</tr>

	<tr>
	<td colspan="2">Keperluan</td>
	<td width="2%" align="center">:</td>
	<td colspan="3"><?php echo $d['nmkeg']?></td>
	</tr>

	<tr>
	<td colspan="2">Angkutan</td>
	<td width="2%" align="center">:</td>
	<td colspan="3"><?php echo $d['angkutan']?></td>
	</tr>

	<tr>
	<td colspan="2">Berangkat Tanggal</td>
	<td width="2%" align="center">:</td>
	<td colspan="3"><?php echo $brgkt?></td>
	</tr>

	<tr>
	<td colspan="2">Kembali Tanggal</td>
	<td width="2%" align="center">:</td>
	<td colspan="3"><?php echo $kem?></td>
	</tr>

	<tr>
	<td colspan="2">Keterangan Lain-lain</td>
	<td width="2%" align="center">:</td>
	<td colspan="3"><?php echo $d['ket']?></td>
	</tr>

	<tr>
	<td colspan="2"><strong>Perintah Selesai</strong></td>
	</tr>

	</tbody>
	</table>
<br/>

	<table class="table table-bordered" width="100%" border="0">
	<tbody>
	<tr>

	<td colspan="3" align="center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<td colspan="4" align="center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<td colspan="2" align="center">Dikeluarkan di</td>
	<td align="center">:</td>
	<td colspan="2">Probolinggo</td>
	</tr>

	<tr>
	<td colspan="3" align="center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<td colspan="4" align="center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<td colspan="2" align="center">Pada Tanggal</td>
	<td align="center">:</td>
	<td colspan="2"><?php echo $tglspt?></td>
	</tr>

	</tbody>
	</table>

<br/>

	<table class="table table-bordered" width="100%" border="0">
	<tbody>
	<tr>
	<td colspan="4" align="center">&nbsp;&nbsp;&nbsp;</td>
	<td colspan="5" align="center"><strong><?php echo strtoupper($d['jbt'])?></strong></td>
	</tr>

	<tr>
	<td colspan="4" align="center">&nbsp;&nbsp;&nbsp;</td>
	<td colspan="5" align="center">&nbsp;&nbsp;&nbsp;</td>
	</tr>

	<tr>
	<td colspan="4" align="center">&nbsp;&nbsp;&nbsp;</td>
	<td colspan="5" align="center">&nbsp;&nbsp;&nbsp;</td>
	</tr>

	<tr>
	<td colspan="4" align="center">&nbsp;&nbsp;&nbsp;</td>
	<td colspan="5" align="center">&nbsp;&nbsp;&nbsp;</td>
	</tr>

	<tr>
	<td colspan="4" align="center">&nbsp;&nbsp;&nbsp;</td>
	<td colspan="5" align="center">&nbsp;&nbsp;&nbsp;</td>
	</tr>



	<tr>
	<td colspan="4" align="center">&nbsp;&nbsp;&nbsp;</td>
	<td colspan="5" align="center"><strong><u><?php echo strtoupper($d['namap']) ?></u></strong></td>
	</tr>

	<tr>
	<td colspan="4" align="center">&nbsp;&nbsp;&nbsp;</td>
	<td colspan="5" align="center"><strong><?php echo 'NIP. '.$d['nipj']?></strong></td>
	</tr>

	</tbody>
	</table>

</div>
	</div><!-- /.row -->
<?php endforeach ?>


<!--<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 1.0
    </div>
    <strong>Copyright &copy; 2017 <a href="http://cvmiladiyyah.web.id" target="_blank">Miladiyyah</a>.</strong> All rights reserved.
</footer>-->

    </body>
</html>
