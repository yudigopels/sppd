			<table class="tbl_lap2" border="1px" style="margin-top:30px;">
				
				
				<tr>
					<td class="cel1">
						<table class="tbl_isi" border="0">
							<tr>
								<td class="cel1">I.</td><td class="cel2">Tiba di</td><td class="cel3">:</td>
							</tr>
							<tr>
								<td></td><td>Pada Tanggal</td><td>:</td>
							</tr>
							
							<tr>
								<td></td><td colspan="2"><br><br><br><br><br><br>&nbsp; <br>&nbsp; </td>
							</tr>
						</table>
					</td>
					<td class="cel2">
						<table class="tbl_isi" border="0">
							<tr>
								<td class="cel1"></td><td class="cel2">Berangkat dari</td><td class="cel3">:</td>
							</tr>
							<tr>
								<td></td><td>Ke</td><td>:</td>
							</tr>
							<tr>
								<td></td><td>Pada Tanggal</td><td>:</td>
							</tr>
							<tr>
								<td></td><td colspan="2"><br><br><br><br><br><br>&nbsp; <br>&nbsp; </td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="cel1">
						<table class="tbl_isi" border="0">
							<tr>
								<td class="cel1">II.</td><td class="cel2">Tiba di</td><td class="cel3">:</td>
							</tr>
							<tr>
								<td></td><td>Pada Tanggal</td><td>:</td>
							</tr>
							
							<tr>
								<td></td><td colspan="2"><br><br><br><br><br><br>&nbsp; <br>&nbsp; </td>
							</tr>
						</table>
					</td>
					<td class="cel2">
						<table class="tbl_isi" border="0">
							<tr>
								<td class="cel1"></td><td class="cel2">Berangkat dari</td><td class="cel3">:</td>
							</tr>
							<tr>
								<td></td><td>Ke</td><td>:</td>
							</tr>
							<tr>
								<td></td><td>Pada Tanggal</td><td>:</td>
							</tr>
							<tr>
								<td></td><td colspan="2"><br><br><br><br><br><br>&nbsp; <br>&nbsp; </td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="cel1">
						<table class="tbl_isi" border="0">
							<tr>
								<td class="cel1">III.</td><td class="cel2">Tiba di</td><td class="cel3">:</td>
							</tr>
							<tr>
								<td></td><td>Pada Tanggal</td><td>:</td>
							</tr>
							
							<tr>
								<td></td><td colspan="2"><br><br><br><br><br><br>&nbsp; <br>&nbsp; </td>
							</tr>
						</table>
					</td>
					<td class="cel2">
						<table class="tbl_isi" border="0">
							<tr>
								<td class="cel1"></td><td class="cel2">Berangkat dari</td><td class="cel3">:</td>
							</tr>
							<tr>
								<td></td><td>Ke</td><td>:</td>
							</tr>
							<tr>
								<td></td><td>Pada Tanggal</td><td>:</td>
							</tr>
							<tr>
								<td></td><td colspan="2"><br><br><br><br><br><br>&nbsp; <br>&nbsp; </td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="cel1">
						<table class="tbl_isi" border="0">
							<tr>
								<td class="cel1">IV.</td><td class="cel2">Tiba di</td><td class="cel3">:</td>
							</tr>
							<tr>
								<td></td><td>Pada Tanggal</td><td>:</td>
							</tr>
							
							<tr>
								<td></td><td colspan="2"><br><br><br><br><br><br>&nbsp; <br>&nbsp; </td>
							</tr>
						</table>
					</td>
					<td class="cel2">
						<table class="tbl_isi" border="0">
							<tr>
								<td class="cel1"></td><td class="cel2">Berangkat dari</td><td class="cel3">:</td>
							</tr>
							<tr>
								<td></td><td>Ke</td><td>:</td>
							</tr>
							<tr>
								<td></td><td>Pada Tanggal</td><td>:</td>
							</tr>
							<tr>
								<td></td><td colspan="2"><br><br><br><br><br><br>&nbsp; <br>&nbsp; </td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="cel1">
						<table class="tbl_isi" border="0">
							<tr>
								<td class="cel1">V.</td><td class="cel2">Tiba di</td><td class="cel3">:</td>
							</tr>
							<tr>
								<td></td><td>Pada Tanggal</td><td>:</td>
							</tr>
							
							<tr>
								<td></td><td colspan="2"><br><br><br><br><br><br>&nbsp; <br>&nbsp; </td>
							</tr>
						</table>
					</td>
					<td class="cel2">
						<table class="tbl_isi" border="0">
							<tr>
								<td class="cel1"></td><td class="cel2" colspan="2">
								Telah diperiksa dengan keterangan bahwa perjalanan<br>
								tersebut atas perintahnya semata-mata untuk<br>
								kepentingan jabatan dalam waktu yang sesingkat-<br>
								singkatnya.</td>
							</tr>
							<tr>
								<td></td><td colspan="2">Pejabat Pembuat Komitmen<br><br><br><br><br><br><br><br>&nbsp; <br>&nbsp; </td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="cel1" colspan="2">
						<table class="tbl_isi" border="0">
							<tr>
								<td class="cel1">VI.</td><td class="cel2" colspan="2"> </td><td class="cel3">:</td>
							</tr>
							
						</table>
					</td>
				
				</tr>
				<tr>
					<td class="cel1" colspan="2">
						<table class="tbl_isi" border="0">
							<tr>
								<td class="cel1">VIII.</td><td class="cel2" colspan="2">PERTHATIAN :<br>
								PPK yang menerbitkan SPD, pegawai yang melakukan perjalanan dinas, para pejabat yang mengesahkan<br>
								tanggal berangkat/tiba, serta bendahara pengeluaran pertanggung jawab berdasarkan peraturan-<br>
								peraturan Keuangan Negara apabila negara menderita rugi akibat kesalahan, kelalaian, dan<br>
								kealpaanya.
								</td><td class="cel3">:</td>
							</tr>
							
						</table>
					</td>
				
				</tr>
				
				
			</table>
