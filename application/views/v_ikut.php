<section class="content-header">
    <h1>
        Dashboard
        <small>Pengikut SPPD</small>
    </h1>

</section>


<div class="col-xs-12">


          <!-- Custom Tabs -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#home" data-toggle="tab" aria-expanded="true">View</a></li>
              <li class=""><a href="#add" data-toggle="tab" aria-expanded="false">Tambah</a></li>

             
            </ul>
           
	<div class="tab-content">
		

	<div class="tab-pane active" id="home">
	<!-- small box -->
	<div class="table-responsive">
	<div class="bg-blue-active">
	<table class="table">
					    <thead>
						    <tr>
						        <th>No</th>
						        <th>Nama Pengikut</th>
						        <th>Ket</th>
						        <th></th>
						    </tr>
					    </thead>

					    <tbody>
					    <?php $no = 1; ?>
					    <?php foreach ($data as $d): ?>					    	
					    	<tr class="bg-teal">
					    		<td><?php echo $no++; ?></td>
					    		<td><?php echo $d['namai'] ?></td>
					    		<td><?php echo $d['ket'] ?></td>
					    		<td>
<a class="btn btn-success btn-sm" href="<?php echo site_url('sppd/getikut_id/').'/'.$d['id'].'/'.$idspd ?>" ><span style="color: #fff;" class="glyphicon glyphicon-pencil"></span> Edit</a>
<a class="btn btn-danger btn-sm" href="<?php echo site_url('sppd/hapus_ikut/').'/'.$d['id'].'/'.$idspd ?>" ><span style="color: #fff;" class="glyphicon glyphicon-trash"></span> Hapus</a>

				    		</td>
					    	</tr>
					    <?php endforeach ?>
					    </tbody>
	</table>
</div>
</div>
</div>

              <div class="tab-pane" id="add">

                <?php $this->load->view('add/add_spdikut'); ?>
              </div>

</div>
</div>
<a class="btn btn-success" href="<?php echo site_url('sppd/v_sppd') ?>" title=""><i class="ion ion-android-arrow-back"></i> Kembali</a>
</div>


