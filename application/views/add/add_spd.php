<div class="box-header with-border">

<?php echo form_open('sppd/tambah'); ?>

<div class="form-group row">
	<div class="col-xs-5">

	<label style="font-size: 14px;">Kode SPPD</label>
	<input type="text" name="kode_spd" id="inputKode" class="form-control input-sm" placeholder="Kode SPD....." required="required">
	</div>

	<div class="col-xs-5">
	<label style="font-size: 14px;">Nomor SPPD</label>
	<input type="text" name="no_spd" id="inputKode" class="form-control input-sm" placeholder="No SPD....." required="required">
	</div>


	<div class="col-xs-6">
	<label style="font-size: 14px;">No SPT </label>
<input type="search" class="autospt form-control input-sm" id="autospt" name="nospt" placeholder="No SPT"/>
<input type="hidden" name="ids" id="ids" class="autospt form-control" readonly="yes">	
	</div>
</div>


</div>
<div class="form-group row">

<div class="col-xs-3">
<label style="font-size: 14px;">Tgl SPT </label>
<input type="text" name="tgspt" id="tgspt" class="autospt form-control" readonly="yes">
</div>

<div class="col-xs-3">
<label style="font-size: 14px;">Tgl Berangkat </label>
<input type="text" name="brgkt" id="brgkt" class="autospt form-control" readonly="yes">
</div>

<div class="col-xs-3">
<label style="font-size: 14px;">Tgl Kembali </label>
<input type="text" name="kmbl" id="kmbl" class="autospt form-control" readonly="yes">
</div>

</div>

<div class="form-group row">
	<div class="col-xs-3">
	<label style="font-size: 14px;">Nama Pegawai </label>
	<input type="text" name="nmpeg" id="nmpeg" class="autospt form-control" readonly="yes">
	<!--<input type="search" class="autop form-control input-sm" id="autop" name="peg" placeholder="Nama Pegawai"/>
	<input type="hidden" name="idp" id="idp" class="autop form-control" readonly="yes">-->
	</div>

	<div class="col-xs-3">
	<label style="font-size: 14px;">NIP Pegawai </label>
	<input type="text" name="nipp" id="nipp" class="autospt form-control" readonly="yes">
	</div>

	<div class="col-xs-3">
	<label style="font-size: 14px;">Jabatan </label>
	<input type="text" name="idjab" id="idjab" class="autospt form-control" readonly="yes">
	</div>

	<div class="col-xs-3">
	<label style="font-size: 14px;">Golongan </label>
	<input type="text" name="gol" id="gol" class="autospt form-control" readonly="yes">
	</div>
</div>

<div class="form-group row">
	<div class="col-xs-3">
	<label style="font-size: 14px;">Tingkat Biaya </label>
	<input type="text" name="tgkt" id="tgkt" class="autospt form-control" readonly="yes" >
	</div>
</div>


<div class="form-group row">
<div class="col-xs-5">
<label style="font-size: 14px;">PPKom </label>
<input type="search" class="autotanda form-control input-sm" id="autotanda" name="pegtanda" placeholder="Nama Penandatangan"/>
<input type="text" name="nip" id="nip" class="autotanda form-control" readonly="yes">
<input type="hidden" name="idj" id="idj" class="autotanda form-control" readonly="yes">
</div>
<div class="col-xs-5">
<label style="font-size: 14px;">Bendahara Pengeluaran </label>
<input type="search" class="autob form-control input-sm" id="autob" name="pegb" placeholder="Nama Bendahara" placeholder="Bendahara"/>
<input type="text" name="nipbe" id="nipbe" class="autob form-control" readonly="yes">
<input type="hidden" name="idjb" id="idjb" class="autob form-control" readonly="yes">
</div>

</div>

<div class="form-group row">
<div class="col-xs-6">
	<label style="font-size: 14px;">Keterangan Lain-lain</label>
	<textarea id="editor1" name="ket" rows="6" class="form-control input-sm"></textarea>
</div>
</div>

<div class="form-group row">

	<div class="col-xs-4">
	<!--<button type="button" class="btn btn-default" data-dismiss="modal"><span style="color: #fff;" class="glyphicon glyphicon-remove-circle"></span> Tutup</button>-->
<a class="btn btn-info" href="<?php echo site_url('sppd/v_sppd') ?>" title=""><i class="ion ion-android-arrow-back"></i> Kembali</a>
	<button type="submit" class="btn btn-primary"><span style="color: #fff;" class="glyphicon glyphicon-floppy-saved"></span>Simpan</button>
	</div>
</div>


<?php echo form_close(); ?>

</div><!--smallbox-->


