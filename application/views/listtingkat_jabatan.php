

<div class="col-lg-10 col-xs-6">
<a class="btn btn-success" data-toggle="modal" href='#modal-id' role="button"><i class="fa fa-plus">Tambah Data Tingkat Biaya</i></a>
                  		<?php $this->load->view('modal/add_tingkat_jabatan'); ?>
</div>

<div class="col-lg-10 col-xs-6">

            <!-- small box -->
           <!-- <div class="table-responsive">-->
			 <div class="modal-info .modal-header">
                <table class="table">
					    <thead>
						    <tr class="bg-orange-active">
						        <th>ID</th>
								<th>Tingkat Biaya</th>
								<th>Lokasi</th>
								<th>Uang Makan</th>
								<th>Uang Saku</th>
						       						        
						        <th></th>
						    </tr>
					    </thead>
					    <tbody>
					    <?php $no = $this->uri->segment('3') + 1;//$no = 1; ?>
					    <?php foreach ($data as $d): ?>					    	
					    	<tr>
					    		<td><?php echo $no++; ?></td>
					    		<td><?php echo $d['tgkt'] ?></td>
							<td><?php echo $d['lok'] ?></td>
							<td><?php echo rupiah($d['um']) ?></td>
							<td><?php echo rupiah($d['us']) ?></td>
					    		
								<td>
					    			<a class="btn btn-primary" href="<?php echo site_url('tingkat_jabatan/get_id/').'/'.$d['id'] ?>" ><span style="color: #fff;" class="glyphicon glyphicon-pencil"></span> Edit</a>
					    			<a class="btn btn-danger" href="<?php echo site_url('tingkat_jabatan/hapus/').'/'.$d['id'] ?>" ><span style="color: #fff;" class="glyphicon glyphicon-trash"></span> Hapus</a>
<!--<a class="btn btn-info" href="<?php echo base_url().'pegawai/cetak/'.$d['id']; ?>" target="_blank" title=""><i class="fa fa-print"></i> Cetak</a>-->
					    		</td>
					    	</tr>
					    <?php endforeach ?>
					    </tbody>
					</table>
            </div>

<div class="halaman">Halaman : <?php echo $this->pagination->create_links(); ?></div>
