<div id="page-wrapper">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <ol class="breadcrumb">
				<li>
					<a href="#"><i class="fa fa-dashboard"></i> Dashboard</a>
				</li>
				<li class="active"><i class="fa fa-wrench fa-fw"></i>Update Data Tingkat Jabatan</li>
			</ol>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="panel panel-default">
                  <div class="panel-body" align="justify">	
				 <?php echo form_open('tingkat_jabatan/update'); ?>
<?php foreach($data as $row){  ?>

		<div class="form-group">					
			<label class="col-sm-2 control-label">Tingkat Jabatan</label>
			<div class="col-sm-10">
<input type="search" class="autot form-control input-sm" id="autot" name="tkt" value="<?php echo $row['tgkt']; ?>"/>
<input type="text" name="idtk" id="idtk" class="autot form-control" readonly="yes" value="<?php echo $row['tingkat_jbt']; ?>">		
<input type="hidden" name="id" id="inputKode" class="form-control" value="<?php echo $row['id']; ?>">	
			</div>
		</div>

<div class="form-group">					
			<label class="col-sm-2 control-label">Lokasi</label>
			<div class="col-sm-10">
			<!--<select name="lokasi" id="lokasi" class="form-control">
			<?php 
			$idlok=$row['idlok'];
			foreach($lok as $rowa){?>
			<?php if(($rowa['id'])==($idlok)){?>
			<option value="<?=$rowa['id']?>" selected="selected"><?=$rowa['nama_provinsi']?></option>
			<?php }else{ ?>
			<option value="<?=$rowa['id']?>"><?=$rowa['nama_provinsi']?></option>
			<?php }?>
			<?php }?>

           		</select>-->
<input type="search" class="autocomplete form-control input-sm" id="autocomplete" name="lokasi" value="<?php echo $row['lok']; ?>"/>
<input type="hidden" name="idkeg" id="idkeg" class="auto form-control" readonly="yes" value="<?php echo $row['idlok']; ?>">

		
			</div>
		</div>

<div class="form-group">					
			<label class="col-sm-2 control-label">Uang Makan</label>
			<div class="col-sm-10">
<input type="text" name="um" id="inputKode" class="form-control" value="<?php echo $row['um']; ?>">		
			</div>
		</div>

<div class="form-group">					
			<label class="col-sm-2 control-label">Uang Saku</label>
			<div class="col-sm-10">
<input type="text" name="us" id="inputKode" class="form-control" value="<?php echo $row['us']; ?>">		
			</div>
		</div>
		
<div class="form-group">
<label class="col-sm-2 control-label"></label>
<div class="col-sm-10">


<br><a href="<?php echo site_url('tingkat_jabatan') ?>" class="btn btn-danger"><span style="color: #fff;" class="glyphicon glyphicon-circle-arrow-left"></span> Kembali</a>
<button type="submit" class="btn btn-success"><span style="color: #fff;" class="glyphicon glyphicon-pencil"></span> Simpan Perubahan</button>
</div>
</div>
				<?php }?>
				<?php echo form_close(); ?>

		</div>
	</div>
</div>
</div>
