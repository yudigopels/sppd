-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 26, 2017 at 07:11 PM
-- Server version: 5.5.54
-- PHP Version: 5.4.45-4+deprecated+dontuse+deb.sury.org~precise+1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sppd`
--

-- --------------------------------------------------------

--
-- Table structure for table `bidang_ruang`
--

CREATE TABLE IF NOT EXISTS `bidang_ruang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bidang_ruang` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `bidang_ruang`
--

INSERT INTO `bidang_ruang` (`id`, `bidang_ruang`) VALUES
(1, 'Administrasi Kepegawaian'),
(2, 'Tata Usaha Pimpinan'),
(3, 'Bagian Rumah Tangga');

-- --------------------------------------------------------

--
-- Table structure for table `data_berkas`
--

CREATE TABLE IF NOT EXISTS `data_berkas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `berkas` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `data_berkas`
--

INSERT INTO `data_berkas` (`id`, `berkas`) VALUES
(1, 'RINCIAN PERJALANAN DINAS'),
(2, 'SPPD'),
(3, 'SPT'),
(5, 'TIKET'),
(6, 'AIRPORT TAX'),
(7, 'BOARDING PASS'),
(8, 'PENGINAPAN'),
(9, 'DAFTAR PENGELUARAN RIIL'),
(10, 'SURAT PERNYATAAN'),
(11, 'KWITANSI / SURAT KETERANGAN SEWA KENDARAAN (BERMATERAI)'),
(12, 'RINCIAN PERJALANAN DINAS');

-- --------------------------------------------------------

--
-- Table structure for table `data_tingkat`
--

CREATE TABLE IF NOT EXISTS `data_tingkat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tingkat_jbt` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `data_tingkat`
--

INSERT INTO `data_tingkat` (`id`, `tingkat_jbt`) VALUES
(1, 'F1'),
(2, 'F2'),
(3, 'F3'),
(5, 'F4'),
(6, 'F5'),
(7, 'A');

-- --------------------------------------------------------

--
-- Table structure for table `giat`
--

CREATE TABLE IF NOT EXISTS `giat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL,
  `tahun` int(4) NOT NULL,
  `akun` varchar(50) NOT NULL,
  `lokasi` int(11) NOT NULL,
  `skpd` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `giat`
--

INSERT INTO `giat` (`id`, `nama`, `tahun`, `akun`, `lokasi`, `skpd`) VALUES
(1, 'PENYELENGGARAAN KEDINASAN PIMPINAN DAN PENDAMPINGAN', 2017, '002.031041.0122541/1501', 15, 'Dinas Maju Terus Pantang Mundur'),
(2, 'PENYUSUNAN LAPORAN KEUANGAN UAPP E-1 PP DAN PL SEMESTER 1', 2017, '01.0225.00215.00222', 16, 'BPKA Provinsi Jatim'),
(3, 'Pembahasan RAPERDA Kota Probolinggo tentang Hak Keuangan dan Administrasi Pimpinan dan Anggota DPRD ', 2017, '2063.006.524219', 16, 'DPRD Kota Probolinggo');

-- --------------------------------------------------------

--
-- Table structure for table `instansi`
--

CREATE TABLE IF NOT EXISTS `instansi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `instansi` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `instansi`
--

INSERT INTO `instansi` (`id`, `instansi`) VALUES
(1, 'Badan Pendapatan Pengelolaan Keuangan dan Aset');

-- --------------------------------------------------------

--
-- Table structure for table `jabatan`
--

CREATE TABLE IF NOT EXISTS `jabatan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `jabatan`
--

INSERT INTO `jabatan` (`id`, `content`) VALUES
(1, 'Kepala Dinas'),
(2, 'Sekretaris Dinas'),
(3, 'Staff Dinas'),
(14, 'Kasubag');

-- --------------------------------------------------------

--
-- Table structure for table `pangkat`
--

CREATE TABLE IF NOT EXISTS `pangkat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `singkat` varchar(200) NOT NULL,
  `content` varchar(200) NOT NULL,
  `idbiaya` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `pangkat`
--

INSERT INTO `pangkat` (`id`, `singkat`, `content`, `idbiaya`) VALUES
(1, 'IV A', 'Golongan IV A', 2),
(2, 'IV B', 'golongan IV B', 2),
(3, 'IV C', 'golongan IV C', 1),
(4, 'IV D', 'golongan IV D', 1),
(5, 'IV E', 'golongan IV E', 1),
(6, 'III A', 'Pranata I', 3),
(7, 'III B', 'golongan III B', 1),
(8, 'III C', 'golongan III C', 1),
(9, 'III D', 'golongan III D', 1),
(10, 'II A', 'golongan II A', 1),
(11, 'II B', 'golongan II B', 1),
(12, 'II C', 'golongan II C', 1),
(13, 'II D', 'golongan II D', 3),
(15, 'I B', 'Staf', 3),
(16, 'I C', 'golongan I C', 1),
(17, 'I D', 'golongan I D', 5);

-- --------------------------------------------------------

--
-- Table structure for table `pejabat`
--

CREATE TABLE IF NOT EXISTS `pejabat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `namap` varchar(50) NOT NULL,
  `jabat` varchar(50) NOT NULL,
  `nip` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `pejabat`
--

INSERT INTO `pejabat` (`id`, `namap`, `jabat`, `nip`) VALUES
(1, 'Yudi Susanto', 'Pejabat Pembuat Komitmen', '196401111989111001'),
(2, 'Sudjiman', 'Bendahara Pengeluaran', '196003031981031008 '),
(3, 'Zulham Wafiq, SE ', 'Pejabat Pelaksana Teknis Kegiatan', '1983100320009121003 ');

-- --------------------------------------------------------

--
-- Table structure for table `sppd`
--

CREATE TABLE IF NOT EXISTS `sppd` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idspt` int(11) NOT NULL,
  `idkom` int(11) NOT NULL,
  `idb` int(11) NOT NULL,
  `kode` varchar(10) NOT NULL,
  `nospd` int(11) NOT NULL,
  `ket` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `sppd`
--

INSERT INTO `sppd` (`id`, `idspt`, `idkom`, `idb`, `kode`, `nospd`, `ket`) VALUES
(1, 2, 1, 3, 'Kode.001', 83, '<p>1. Selesai tugas harap melaporkan hasil kepada pimpinan.</p>\r\n'),
(3, 3, 1, 2, 'Kode.002', 0, '1. Selesai tugas harap melaporkan hasil kepada pimpinan.');

-- --------------------------------------------------------

--
-- Table structure for table `sppd_berkas`
--

CREATE TABLE IF NOT EXISTS `sppd_berkas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idsppd` int(11) NOT NULL,
  `idberkas` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `sppd_berkas`
--

INSERT INTO `sppd_berkas` (`id`, `idsppd`, `idberkas`) VALUES
(1, 1, '1,2,3,10');

-- --------------------------------------------------------

--
-- Table structure for table `sppd_biaya`
--

CREATE TABLE IF NOT EXISTS `sppd_biaya` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idsppd` int(11) NOT NULL,
  `tglbyr` date NOT NULL,
  `lama` int(11) NOT NULL,
  `idprov` int(11) NOT NULL,
  `idtgkt` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `sppd_biaya`
--

INSERT INTO `sppd_biaya` (`id`, `idsppd`, `tglbyr`, `lama`, `idprov`, `idtgkt`) VALUES
(1, 3, '2017-08-17', 1, 16, 7),
(3, 1, '2017-08-23', 2, 15, 6);

-- --------------------------------------------------------

--
-- Table structure for table `sppd_ikut`
--

CREATE TABLE IF NOT EXISTS `sppd_ikut` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idsppd` int(11) NOT NULL,
  `namai` varchar(50) NOT NULL,
  `ket` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `sppd_ikut`
--

INSERT INTO `sppd_ikut` (`id`, `idsppd`, `namai`, `ket`) VALUES
(1, 1, 'Deni', 'Sopir'),
(2, 3, 'Ari', 'Sopir');

-- --------------------------------------------------------

--
-- Table structure for table `sppd_kwt`
--

CREATE TABLE IF NOT EXISTS `sppd_kwt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idsppd` int(11) NOT NULL,
  `tgl` date NOT NULL,
  `nokwt` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `sppd_kwt`
--

INSERT INTO `sppd_kwt` (`id`, `idsppd`, `tgl`, `nokwt`) VALUES
(1, 3, '2017-08-25', '102/KWT_112/3.0.02.02/2017'),
(2, 1, '2017-08-16', '103/KWT_112/3.0.02.02/2017');

-- --------------------------------------------------------

--
-- Table structure for table `spt`
--

CREATE TABLE IF NOT EXISTS `spt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idkeg` int(11) NOT NULL,
  `idpeg` int(11) NOT NULL,
  `no_spt` varchar(100) NOT NULL,
  `tgl_spt` date NOT NULL,
  `tgl_berangkat` date NOT NULL,
  `tempat_brgkt` varchar(100) NOT NULL,
  `tujuan` varchar(100) NOT NULL,
  `tgl_kembali` date NOT NULL,
  `angkutan` varchar(255) NOT NULL,
  `ket` text NOT NULL,
  `idjabat` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `spt`
--

INSERT INTO `spt` (`id`, `idkeg`, `idpeg`, `no_spt`, `tgl_spt`, `tgl_berangkat`, `tempat_brgkt`, `tujuan`, `tgl_kembali`, `angkutan`, `ket`, `idjabat`) VALUES
(1, 1, 1, 'KP.004/DINTECH.0001/VIII/2017', '2017-08-10', '2017-08-14', 'Probolinggo', 'Yogyakarta', '2017-08-16', 'Darat', '<p>1. Selesai tugas harap melaporkan hasil kepada pimpinan.</p>\r\n\r\n<p>2. Biaya Perjalanan Dinas dibebankan APBD Kota Probolinggo</p>\r\n', 3),
(2, 1, 8, 'KP.004/DINTECH.0002/VIII/2017', '2017-08-10', '2017-08-14', 'Probolinggo', 'Yogyakarta', '2017-08-16', 'Darat', '<p>1. Selesai Tugas Harap Melapor Atasan Langsung</p>\r\n', 1),
(3, 2, 11, 'KP.004/MLD.0001/VIII/2017', '2017-08-16', '2017-08-22', 'Probolinggo', 'Surabaya', '2017-08-23', 'Darat', '', 1),
(4, 3, 1, 'SPT/BPPKAD.VIII/2017', '2017-08-14', '2017-08-14', 'Probolinggo', 'Probolinggo', '2017-08-15', 'Darat', '-', 3);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_provinsi`
--

CREATE TABLE IF NOT EXISTS `tbl_provinsi` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `nama_provinsi` varchar(150) NOT NULL,
  `ut` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=35 ;

--
-- Dumping data for table `tbl_provinsi`
--

INSERT INTO `tbl_provinsi` (`id`, `nama_provinsi`, `ut`) VALUES
(1, 'ACEH', 0),
(2, 'SUMATERA UTARA', 0),
(3, 'RIAU', 0),
(4, 'KEPULAUAN RIAU', 0),
(5, 'JAMBI', 0),
(6, 'SUMATERA BARAT', 0),
(7, 'SUMATERA SELATAN', 0),
(8, 'LAMPUNG', 0),
(9, 'BENGKULU', 0),
(10, 'BANGKA BELITUNG', 0),
(11, 'BANTEN', 0),
(12, 'JAWA BARAT', 500000),
(13, 'D.K.I JAKARTA', 0),
(14, 'JAWA TENGAH', 0),
(15, 'YOGYAKARTA', 250000),
(16, 'JAWA TIMUR', 1000000),
(17, 'BALI', 0),
(18, 'NUSA TENGGARA BARAT', 0),
(19, 'NUSA TENGGRA TIMUR', 0),
(20, 'KALIMANTAN BARAT', 0),
(21, 'KALIMANTAN TENGAH', 0),
(22, 'KALIMANTAN SELATAN ', 0),
(23, 'KALIMANTANTIMUR', 0),
(24, 'KALIMANTAN UTARA', 0),
(25, 'SULAWESI UTARA', 0),
(26, 'GORONTALO', 0),
(27, 'SULAWESI BARAT', 0),
(28, 'SULAWESI SELATAN', 0),
(29, 'SULAWESI TENGAH', 0),
(30, 'SULAWESI TENGGARA', 0),
(31, 'MALUKU', 0),
(32, 'MALUKU UTARA', 0),
(33, 'PAPUA', 0),
(34, 'PAPUA BARAT', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_kwitansi`
--

CREATE TABLE IF NOT EXISTS `tb_kwitansi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_kwitansi` varchar(8) NOT NULL,
  `no_sppd` varchar(100) NOT NULL,
  `transportasi` varchar(250) NOT NULL,
  `konsumsi` varchar(250) NOT NULL,
  `penginapan` varchar(250) NOT NULL,
  `kendaraan_lain` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_pegawai`
--

CREATE TABLE IF NOT EXISTS `tb_pegawai` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nip` varchar(12) NOT NULL,
  `nama` varchar(64) NOT NULL,
  `jab` int(11) NOT NULL,
  `gol` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `tb_pegawai`
--

INSERT INTO `tb_pegawai` (`id`, `nip`, `nama`, `jab`, `gol`) VALUES
(1, '196003031981', 'Wawan Soegyantono, SE., MM', 1, 3),
(8, '1111000', 'Marwah', 3, 13),
(11, '768342', 'Arbei', 3, 13),
(12, '3908434244', 'roni33', 3, 3),
(13, '983874', 'novan java', 3, 16);

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE IF NOT EXISTS `tb_user` (
  `id_user` varchar(10) NOT NULL,
  `nama_user` varchar(64) NOT NULL,
  `password` varchar(255) NOT NULL,
  `hak_akses` varchar(16) NOT NULL,
  `aktif` varchar(1) NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id_user`, `nama_user`, `password`, `hak_akses`, `aktif`) VALUES
('user', 'coba', 'user', 'User', 'N'),
('user1', 'fakih', '123', 'User', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `tingkat_jabatan`
--

CREATE TABLE IF NOT EXISTS `tingkat_jabatan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tingkat_jbt` int(11) NOT NULL,
  `idlok` int(11) NOT NULL,
  `um` int(11) NOT NULL,
  `us` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `tingkat_jabatan`
--

INSERT INTO `tingkat_jabatan` (`id`, `tingkat_jbt`, `idlok`, `um`, `us`) VALUES
(1, 6, 1, 600000, 750000),
(2, 2, 2, 700000, 500000),
(3, 5, 3, 550000, 500000),
(5, 4, 4, 0, 0),
(6, 5, 15, 500000, 300000),
(7, 3, 16, 400000, 250000),
(8, 6, 13, 200000, 300000);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
